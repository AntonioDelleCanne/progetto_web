<?php
require_once("bootstrap.php");
$templateParams["nbCart"] = true;
$templateParams["nbMenu"] = true;
$templateParams["nbAlarm"] = true;
$templateParams["nbBack"] = true;
$templateParams["nbSearch"] = true;
if(!isset($_SESSION["logtype"]) || $_SESSION["logtype"] != "utente"){ //Caso che non dovrebbe mai verificarsi
    $templateParams["nbAlarm"] = false;
    $templateParams["nbCart"] = false;
    $templateParams["nbSearch"] = false;
}

//Base template
$templateParams["titolo"] = "Pasuta.it - Acquisti";
$templateParams["nome"] = "eventi_acquistati.php";
$templateParams["evento"] = "evento_miniatura.php";

//Eventi popolari template
if(isset($_SESSION["username"])){ //Caso che dovrebbe sempre verificarsi
    $templateParams["eventiFuturi"] = $dbh->getNEventiAcquistatiFuturi($_GET["n"], $_SESSION["username"]);
    $templateParams["eventiPassati"] = $dbh->getNEventiAcquistatiPassati($_GET["n"], $_SESSION["username"]);
    $templateParams["eventiEliminati"] = $dbh->getNEventiAcquistatiEliminati($_GET["n"], $_SESSION["username"]);
}

require("template/base.php");
?>