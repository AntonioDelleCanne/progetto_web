 <?php
require_once("bootstrap.php");
$templateParams["nbCart"] = true;
$templateParams["nbMenu"] = true;
$templateParams["nbAlarm"] = true;
$templateParams["nbBack"] = true;
$templateParams["nbSearch"] = true;
if(!isset($_SESSION["logtype"]) || $_SESSION["logtype"] != "utente"){ //Caso che non dovrebbe mai verificarsi
    $templateParams["nbAlarm"] = false;
    $templateParams["nbCart"] = false;
    $templateParams["nbSearch"] = false;
}

//Base template
$templateParams["titolo"] = "Pasuta.it - Interessati particolare";
$templateParams["nome"] = "eventi_acquistati_particolare.php";
$templateParams["evento"] = "evento_miniatura.php";
$templateParams["eventi"] = NULL;
$templateParams["stringa_filtro"] = NULL;
$templateParams["tipo"] = $_GET["tipo"];
$templateParams["ordinamento"] = $_GET["ordinamento"];

//Eventi popolari template
if(isset($_SESSION["username"])){ //Caso che dovrebbe sempre verificarsi
    switch($_GET["tipo"]){
        case "futuri":
            $templateParams["eventi"] = $dbh->getEventiAcquistatiFuturi($_SESSION["username"], $_GET["ordinamento"]);
        break;

        case "passati":
            $templateParams["eventi"] = $dbh->getEventiAcquistatiPassati($_SESSION["username"], $_GET["ordinamento"]);
        break;

        case "eliminati":
            $templateParams["eventi"] = $dbh->getEventiAcquistatiEliminati($_SESSION["username"], $_GET["ordinamento"]);
        break;
    }
}

if(isset($_GET["stringa_filtro"]) && strlen($_GET["stringa_filtro"]) > 0) {
    $templateParams["stringa_filtro"] = $_GET["stringa_filtro"];
    $templateParams["eventi"] = array_filter($templateParams["eventi"], function($element){
        return stripos($element["NomeEvento"], $_GET["stringa_filtro"]) !== false ||
        stripos($element["NomeLuogo"], $_GET["stringa_filtro"]) !== false ||
        stripos($element["Username"], $_GET["stringa_filtro"]) !== false;
    });
}

require("template/base.php");
?>