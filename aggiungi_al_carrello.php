<?php
require_once("bootstrap.php");

$idEvento= $_GET["idEvento"];
if($dbh->getDisponibili($idEvento) <= 0){
    return false;
}
$username= $_SESSION["username"];
$quantita=$dbh->getNumeroBigliettiCarrello($username, $idEvento); 
$dbh->setQuantitaEventoCarrello($username, $idEvento, $quantita + 1);

header("Location: evento.php?id=$idEvento");
?>