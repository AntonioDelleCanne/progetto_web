<?php
require_once("bootstrap.php");

$templateParams["nbMenu"] = true;
$templateParams["nbAlarm"] = true;
$templateParams["nbCart"] = true;
$templateParams["nbBack"] = true;
$templateParams["nbSearch"] = true;

$templateParams["evento"] = "evento_carrello.php";
$templateParams["eventicarrello"] = $dbh->getEventiCarrello($_SESSION["username"]);

//Base template
$templateParams["titolo"] = "Pasuta.it - Carrello";
$templateParams["nome"] = "carrello-template.php";
$templateParams["js"] = array("js/ajaxRequests.js","js/carrello.js");

require("template/base.php");
?>