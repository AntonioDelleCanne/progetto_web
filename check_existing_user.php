<?php
    require_once("bootstrap.php");
    
    if(isset($_POST["username"])){
        $username = $_POST["username"];
        echo $dbh->contains("persona", "Username", $username, "s") ? "true" : "false";
    } else if(isset($_POST["mail"])){
        $mail = $_POST["username"];
        echo $dbh->contains("persona", "Mail", $mail, "s") ? "true" : "false";
    } else if(isset($_POST["CF"])){
        $codice = $_POST["CF"];
        echo $dbh->contains("creatore", "CF", $codice, "s") ? "true" : "false";
    } else if(isset($_POST["idcard"])){
        $codice = $_POST["idcard"];
        echo $dbh->contains("creatore", "CartaIdentita", $codice, "s") ? "true" : "false";
    } else echo "false";
?>