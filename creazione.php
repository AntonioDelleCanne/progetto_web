<?php
require_once("bootstrap.php");
$templateParams["nbMenu"] = false;
$templateParams["nbAlarm"] = false;
$templateParams["nbCart"] = false;
$templateParams["nbBack"] = true;
$templateParams["nbSearch"] = false;

$templateParams["titolo"] = "Pasuta.it - Creazione evento";
$templateParams["nome"] = "crea_evento.php";
$templateParams["js"] = array("js/ajaxRequests.js", "js/creazione.js");

$templateParams["evento"] = null;

if(isset($_GET["id"])){
    $templateParams["evento"] = $dbh->getEventoById($_GET['id']);
    if($templateParams["evento"]["Username"] != $_SESSION["username"]){
        echo "Pagina non valida";
        return false;
    }
}

require("template/base.php");
?>