-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 07, 2020 at 06:36 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pasutadb`
--

-- --------------------------------------------------------

--
-- Table structure for table `amministratore`
--

CREATE TABLE `amministratore` (
  `Username` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `amministratore`
--

INSERT INTO `amministratore` (`Username`) VALUES
('Einstein'),
('Sommo');

-- --------------------------------------------------------

--
-- Table structure for table `biglietto_venduto`
--

CREATE TABLE `biglietto_venduto` (
  `idEvento` int(11) NOT NULL,
  `NumeroBiglietto` int(11) NOT NULL,
  `DataVendita` datetime NOT NULL DEFAULT current_timestamp(),
  `PrezzoVendita` float NOT NULL,
  `Username` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `biglietto_venduto`
--

INSERT INTO `biglietto_venduto` (`idEvento`, `NumeroBiglietto`, `DataVendita`, `PrezzoVendita`, `Username`) VALUES
(1, 1, '2020-01-05 16:51:21', 15, 'Cerusso99'),
(1, 2, '2020-01-05 16:56:29', 15, 'Leila123'),
(1, 3, '2020-01-05 16:56:29', 15, 'Leila123'),
(1, 4, '2020-01-05 16:56:29', 15, 'Leila123'),
(1, 5, '2020-01-05 16:56:29', 15, 'Leila123'),
(1, 6, '2020-01-05 16:56:29', 15, 'Leila123'),
(1, 7, '2020-01-05 16:56:29', 15, 'Leila123'),
(1, 8, '2020-01-05 16:56:29', 15, 'Leila123'),
(1, 9, '2020-01-05 16:56:29', 15, 'Leila123'),
(1, 10, '2020-01-05 16:56:41', 15, 'Leila123'),
(1, 11, '2020-01-05 16:56:41', 15, 'Leila123'),
(1, 12, '2020-01-05 16:56:53', 15, 'Leila123'),
(1, 13, '2020-01-07 03:38:44', 15, 'luca99'),
(1, 14, '2020-01-07 03:38:44', 15, 'luca99'),
(1, 15, '2020-01-07 03:38:44', 15, 'luca99'),
(1, 16, '2020-01-07 03:38:44', 15, 'luca99'),
(4, 1, '2020-01-05 16:58:54', 15, 'CremoC80'),
(4, 2, '2020-01-07 06:30:14', 15, 'maurizio73'),
(4, 3, '2020-01-07 06:30:14', 15, 'maurizio73'),
(6, 1, '2020-01-05 16:51:21', 10, 'Cerusso99'),
(6, 2, '2020-01-05 16:53:03', 10, 'Cerusso99'),
(6, 3, '2020-01-05 16:58:54', 10, 'CremoC80'),
(6, 4, '2020-01-05 16:58:54', 10, 'CremoC80'),
(6, 5, '2020-01-05 16:58:54', 10, 'CremoC80'),
(6, 6, '2020-01-05 16:59:47', 10, 'Ricatald80'),
(6, 7, '2020-01-05 16:59:47', 10, 'Ricatald80'),
(7, 1, '2020-01-05 16:51:21', 20, 'Cerusso99'),
(7, 2, '2020-01-05 16:51:21', 20, 'Cerusso99'),
(7, 3, '2020-01-05 16:59:47', 20, 'Ricatald80'),
(7, 4, '2020-01-07 03:38:44', 20, 'luca99'),
(7, 5, '2020-01-07 05:13:27', 20, 'maurizio73'),
(7, 6, '2020-01-07 05:13:27', 20, 'maurizio73'),
(7, 7, '2020-01-07 06:29:44', 20, 'maurizio73'),
(7, 8, '2020-01-07 06:29:44', 20, 'maurizio73'),
(7, 9, '2020-01-07 06:29:44', 20, 'maurizio73'),
(7, 10, '2020-01-07 06:29:44', 20, 'maurizio73'),
(8, 1, '2020-01-07 05:13:12', 10, 'maurizio73'),
(8, 2, '2020-01-07 05:13:12', 10, 'maurizio73'),
(8, 3, '2020-01-07 05:53:52', 10, 'maurizio73'),
(8, 4, '2020-01-07 06:32:56', 10, 'filo93'),
(8, 5, '2020-01-07 06:32:56', 10, 'filo93'),
(8, 6, '2020-01-07 06:32:56', 10, 'filo93'),
(9, 1, '2020-01-07 05:09:58', 10, 'mario98'),
(9, 2, '2020-01-07 05:09:58', 10, 'mario98'),
(9, 3, '2020-01-07 05:09:58', 10, 'mario98'),
(9, 4, '2020-01-07 05:09:58', 10, 'mario98'),
(9, 5, '2020-01-07 05:09:58', 10, 'mario98'),
(10, 1, '2020-01-07 05:08:09', 10, 'luca99'),
(10, 2, '2020-01-07 05:08:09', 10, 'luca99'),
(10, 3, '2020-01-07 05:08:09', 10, 'luca99'),
(10, 4, '2020-01-07 05:13:38', 10, 'maurizio73'),
(10, 5, '2020-01-07 06:33:07', 10, 'filo93'),
(10, 6, '2020-01-07 06:33:07', 10, 'filo93');

-- --------------------------------------------------------

--
-- Table structure for table `carrello`
--

CREATE TABLE `carrello` (
  `idEvento` int(11) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `Quantita` int(11) NOT NULL,
  `TempoAggiunta` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categoria`
--

CREATE TABLE `categoria` (
  `IdCategoria` int(11) NOT NULL,
  `idEvento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categorie`
--

CREATE TABLE `categorie` (
  `IdCategoria` int(11) NOT NULL,
  `Nome` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categorie`
--

INSERT INTO `categorie` (`IdCategoria`, `Nome`) VALUES
(1, 'Musica'),
(2, 'Festa'),
(3, 'Ristorante'),
(4, 'Bambini'),
(5, 'Teatro');

-- --------------------------------------------------------

--
-- Table structure for table `creatore`
--

CREATE TABLE `creatore` (
  `CF` char(16) NOT NULL,
  `CartaIdentita` varchar(20) NOT NULL,
  `IBAN` varchar(50) NOT NULL,
  `Accettato` tinyint(1) NOT NULL,
  `Ban` tinyint(1) NOT NULL,
  `Username` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `creatore`
--

INSERT INTO `creatore` (`CF`, `CartaIdentita`, `IBAN`, `Accettato`, `Ban`, `Username`) VALUES
('CSNFBA65B21D488F', 'CX78514EE', 'IT65E4422203567897976154799', 1, 0, 'Fabio65'),
('NDRFRT98D21D488K', 'CX78224FF', 'IT44E5123403567897976154799', 0, 0, 'Ferro98'),
('MNFGRL80H12G944V', 'CW12683PE', 'IT91H0300203280558247894399', 1, 0, 'GabriM80'),
('CSTGTT77D55A592W', 'CE15904ER', 'IT96E0300203280597976154799', 1, 0, 'GiudiCa14'),
('BRGCLD95R20I514F', 'CD38162LO', 'IT79F0300203280743798452227', 0, 0, 'Imest1934'),
('MGNMRC70T01D488G', 'CE15514EL', 'IT70E0399203283197976154799', 1, 0, 'MarcheEventi01'),
('MLNLCU80A01L500A', 'CZ33134ER', 'IT44E4422204444897933354799', 0, 0, 'Molo80'),
('BGRMRO90B05H501Y', 'CT25128DE', 'IT12H0300203280889354554982', 0, 0, 'Obagari5');

-- --------------------------------------------------------

--
-- Table structure for table `destinatario_msg`
--

CREATE TABLE `destinatario_msg` (
  `UsernameDestinatario` varchar(50) NOT NULL,
  `IdMessaggio` int(11) NOT NULL,
  `Visto` tinyint(4) NOT NULL DEFAULT 0,
  `Eliminato` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `evento`
--

CREATE TABLE `evento` (
  `idEvento` int(11) NOT NULL,
  `Capienza` int(11) DEFAULT NULL,
  `BigliettiDisponibili` int(11) DEFAULT NULL,
  `NomeEvento` varchar(50) NOT NULL,
  `Immagine` varchar(400) NOT NULL,
  `DataInizio` date NOT NULL,
  `OraInizio` time NOT NULL,
  `DataFine` date NOT NULL,
  `OraFine` time NOT NULL,
  `NomeLuogo` varchar(50) NOT NULL,
  `Prezzo` float DEFAULT NULL,
  `Descrizione` varchar(1000) NOT NULL,
  `Via` varchar(50) NOT NULL,
  `N` int(11) NOT NULL,
  `CAP` int(11) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `Eliminato` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `evento`
--

INSERT INTO `evento` (`idEvento`, `Capienza`, `BigliettiDisponibili`, `NomeEvento`, `Immagine`, `DataInizio`, `OraInizio`, `DataFine`, `OraFine`, `NomeLuogo`, `Prezzo`, `Descrizione`, `Via`, `N`, `CAP`, `Username`, `Eliminato`) VALUES
(1, 18, 18, 'Sabato da Leoni', 'sabato_da_leoni.jpg', '2020-02-01', '23:00:00', '2020-02-02', '05:00:00', 'Mamamia', 15, 'Musica: pescheremo A CASO tra i piÃ¹ grandi successi di tutti i tempi, spaziando A CASO da un genere allâ€™altro! \r\n\r\nMain Room > Trap music\r\nOpen Air > Indie\r\nEx Negozio > NOTTE ITALIANA - LA REGINA\r\nFreestyler > Rock\r\n\r\nCosto drink 7â‚¬\r\nCosto shot 3â‚¬', 'Via Giambattista Fiorini', 23, 60019, 'GabriM80', 0),
(2, NULL, NULL, 'Festa delle Streghe', 'festa_delle_streghe.jpg', '2020-01-06', '20:00:00', '2020-01-07', '02:00:00', 'Centro Storico', NULL, 'Stand con cibo e bancarelle in strada, per tutto il centro storico.\r\n\r\nMusica e spettacolo fino a tarda notte.\r\n\r\nFesta adatta a tutte le etÃ .\r\n\r\nPer le 22.00 Ã¨ prevista la distribuzione di dolciumi da parte della befana!\r\n\r\nDa non perdere!', 'Via Barberini', 45, 61040, 'GabriM80', 0),
(3, NULL, NULL, 'Apertura pista di pattinaggio', 'apertura_pattinaggio_in_piazza.jpg', '2020-01-06', '16:00:00', '2020-02-06', '23:00:00', 'Piazza Salotto', NULL, 'Per il mese di gennaio verra\' aperta al pubblico la pista di pattinaggio in piazza salotto.\r\n\r\nSara\' possibile pattinare con un costo di 10â‚¬ l\'ora.\r\n\r\nLa pista sara\' aperta tutti i giorni dalle 16.00 alle 23.00.', 'Piazza salotto', 20, 65100, 'GiudiCa14', 0),
(4, 10, 10, 'Random Party', 'random.jpg', '2020-02-15', '23:00:00', '2020-02-16', '05:00:00', 'Mamamia', 15, 'RandomÂ® #unafestaacaso\r\nha ð—¨ð—¡ð—” sola regola fondamentale:\r\n\r\nðŸ‘  ðŸ‘” ðŸ‘™ ðŸ‘¢ ðŸŽ½ ðŸ‘“ ðŸ’„ ðŸŽ€ ðŸ‘»\r\nðŸ‘º ð—©ð—˜ð—¦ð—§ð—œð—§ð—˜ð—©ð—œ ð—” ð—–ð—”ð—¦ð—¢ ðŸ‘½ \r\nðŸ‘¡ ðŸ‘‘ ðŸŽ© ðŸ‘Ÿ ðŸ’… ðŸ’¼ ðŸ‘• ðŸ‘’ ðŸŽŽ\r\n\r\nðŸŽ ð—¦ð—¢ð—Ÿð—¢ vestendovi ð—” ð—–ð—”ð—¦ð—¢ potrete avere in REGALO\r\ni gadget UFFICIALI di Random, una festa a caso:\r\nt-shirt, occhiali, lecca lecca e tanto altro...\r\n\r\nâ€¢â€¢â€¢â€¢â€¢â€¢â€¢â€¢â€¢â€¢â€¢â€¢â€¢â€¢â€¢â€¢â€¢â€¢â€¢â€¢â€¢â€¢â€¢\r\n\r\nðŸŽµ ð— ð—¨ð—¦ð—œð—–ð—”: pescheremo A CASO tra i piÃ¹ grandi successi di tutti i tempi, spaziando A CASO da un genere allâ€™altro! Ascolta la nostra Playlist Ufficiale: bit.ly/Playlistacaso\r\n\r\nMain Room > RANDOM - Una festa a caso\r\nOpen Air > Afro Afroremember DJs TIUM & JACO \r\nEx Negozio > NOTTE ITALIANA - LA REGINA\r\nFreestyler > Techno KLEN', 'Via Giambattista Fiorini', 23, 60019, 'GiudiCa14', 0),
(5, 8, 8, 'Cena della Vigilia', 'natale_in_rosso.jpg', '2019-12-24', '20:00:00', '2019-01-24', '23:00:00', 'Ristorante Scottadito', 30, 'In occasione della vigilia il ristorante Scottadito offre una cena a tema speciale.\r\n\r\nMenu fisso per tutti i partecipanti.\r\n\r\nVi aspettiamo per festeggiare insieme questa serata speciale.\r\n\r\n', 'Via Mario Angeloni', 335, 47521, 'GiudiCa14', 0),
(6, 15, 15, 'Aperitivo al Bombardino Apres Ski', 'apres_ski.jpg', '2020-01-11', '17:00:00', '2020-01-12', '02:00:00', 'Jumper Apres Ski', 10, 'Il jumper apres ski da un evento con dj set dalla sera fino a tarda notte.\r\n\r\nPer tutti i partecipanti un bombardino omaggio e una consumazione offerta.', 'Via Cima Tosa', 25, 38086, 'GiudiCa14', 1),
(7, 20, 20, 'Post Malone In Concerto', 'concerto_di_fine_anno.jpg', '2020-01-18', '21:00:00', '2020-01-19', '00:30:00', 'Ippodromo Roma Capannelle', 20, 'Si esibisce alle Capannelle il famoso artista Post Malone.\r\n\r\nAd aprire la serata ci sono, alle 21.00, i nostrani Dark Polo Gang con una mezzâ€™ora di set sfacciatamente esplicito, e subito dopo il feroce SaintJhn dal flow incendiario, tra drop secchi e una capacitÃ  di tenere il palco assolutamente animalesca. Due spalle diversissime tra loro, tra divertimento senza prendersi sul serio e lâ€™anima densa, potentissima, del rapper di Brooklyn.\r\n\r\nPer le 22.30 e\' prevista l\'entrata di Post Malone.\r\n\r\n', 'Via Appia Nuova', 1245, 178, 'GiudiCa14', 0),
(8, 10, 10, 'Carnevale di Fano - 1°giornata', 'carnevale01.jpg', '2020-02-09', '14:00:00', '2020-02-09', '20:00:00', 'Centro Storico', 10, 'Il Carnevale di Fano è uno dei più antichi d’Italia. La sua nascita si fa comunemente risalire al 1347, data del primo documento noto nel quale vengono descritte le spese sostenute dal Comune per i festeggiamenti carnevaleschi.\r\nVieni anche tu al migliore carnevale d\'Italia, ti aspetta tanto divertimento e tanti dolciumi.\r\nNon mancare!', 'Via Cavour', 1, 61032, 'MarcheEventi01', 0),
(9, 10, 10, 'Carnevale di Fano - 2°giornata', 'carnevale02.jpg', '2020-02-16', '14:00:00', '2020-02-16', '20:00:00', 'Centro Storico', 10, 'Il Carnevale di Fano è uno dei più antichi d’Italia. La sua nascita si fa comunemente risalire al 1347, data del primo documento noto nel quale vengono descritte le spese sostenute dal Comune per i festeggiamenti carnevaleschi.\r\nVieni anche tu al migliore carnevale d\'Italia, ti aspetta tanto divertimento e tanti dolciumi.\r\nNon mancare!', 'Via Cavour', 1, 61032, 'MarcheEventi01', 0),
(10, 10, 10, 'Carnevale di Fano - 3°giornata', 'carnevale03.jpg', '2020-02-23', '14:00:00', '2020-02-23', '20:00:00', 'Centro Storico', 10, 'Il Carnevale di Fano è uno dei più antichi d’Italia. La sua nascita si fa comunemente risalire al 1347, data del primo documento noto nel quale vengono descritte le spese sostenute dal Comune per i festeggiamenti carnevaleschi.\r\nVieni anche tu al migliore carnevale d\'Italia, ti aspetta tanto divertimento e tanti dolciumi.\r\nNon mancare!', 'Via Cavour', 1, 61032, 'MarcheEventi01', 0),
(11, NULL, NULL, 'Festa del Brodetto', 'brodetto.jpg', '2019-03-21', '19:00:00', '2019-03-21', '23:00:00', 'Lungomare', NULL, 'A Fano la cucina tipica di pesce torna, ogni anno, a farsi spettacolo con il festival più gustoso della riviera adriatica. Palcoscenico della cultura gastronomica con chef di altissimo livello e ambasciatore della tradizione locale, il Festival Internazionale del Brodetto e delle Zuppe di Pesce promette ogni anno di stupire il pubblico con i profumi e i sapori che riempiono, da 17 anni, il lungomare della città.', 'Via Cesare Simonetti', 2, 61032, 'MarcheEventi01', 1),
(12, NULL, NULL, 'Fano dei Cesari - 2019', 'fanodeicesari.jpg', '2019-08-03', '12:00:00', '2019-08-08', '23:00:00', 'Centro Storico', NULL, 'Dopo il grande successo della passata edizione, anche quest’anno è stata confermata la Fano dei Cesari 2019. Si tratta di un evento che la città di Fano, nota anche per il suo storico Carnevale, organizza per celebrare l’antica Fanum Fortunae. L’origine della città, infatti, è legata alla presenza di un tempio dedicato al culto della dea Fortuna. Successivamente i fasti di Fano aumentarono grazie all’imperatore Cesare Ottaviano Augusto e alle opere dell’architetto Vitruvio. Augusto elevò Fano al rango di Colonia Julia, abbellendola con parecchi edifici pubblici e dotandola di una cinta muraria con porta monumentale. Ancora oggi possiamo vedere le vestigia dell’antica storia romana di Fano.', 'Via Cavour', 1, 61032, 'MarcheEventi01', 0),
(13, NULL, NULL, 'Intervista a Jago', 'jago.jpg', '2020-03-14', '16:00:00', '2020-03-14', '18:00:00', 'Piazza del Popolo', NULL, 'In piazza a Pesaro si terrà un\'intervista all\'artista Jago in occasione dell\'inaugurazione della settimana della cultura.\r\nJago è un artista – imprenditore italiano che lavora principalmente nella scultura e nella produzione video.\r\nNasce a Frosinone (Italia) nel 1987, dove ha frequentato il liceo artistico e poi l’Accademia di Belle Arti.\r\n\r\nIn giovane età ha lavorato in Grecia a Naxos, e in Italia fra Roma e Verona.\r\nOggi Jago vive a New York, lavorando tra USA, Cina e Italia.', 'Piazza del Popolo', 1, 61121, 'Fabio65', 0),
(14, NULL, NULL, 'Will Martyr - esposizione temporanea', 'willmartyr.jpg', '2019-07-12', '12:00:00', '2019-07-28', '20:00:00', 'Atelier del Centro', NULL, 'L\'Atelier del Centro, Ancona, annuncia felicemente un esposizione temporanea di 25 opere del pittore Will Martyr.\r\nLe opere che arrivano direttamente dagli Stati Uniti, verranno esposte dal 12 al 28 luglio, per poi essere spostate a Milano.', 'Via Garibaldi', 27, 60121, 'Fabio65', 0),
(15, NULL, NULL, 'Edoardo Tresoldi - Installazione temporanea', 'tresoldi.jpg', '2020-01-08', '14:00:00', '2020-01-20', '18:00:00', 'Palazzo Ducale', NULL, 'Edoardo Tresoldi è uno scultore italiano. Specializzato nella creazione di installazioni ambientali in rete metallica, ha raggiunto notorietá internazionale grazie alla sua opera di ricostruzione della Basilica paleocristiana di Siponto. Al Palazzo Ducale di Urbino presenterà in anteprima una sua nuova opera, ad oggi ancora sconosciuta. Evento immancabili per gli amanti dell\'arte e per tutti i curiosi.', ' Piazza Rinascimento', 13, 61029, 'Fabio65', 0);

-- --------------------------------------------------------

--
-- Table structure for table `indirizzo`
--

CREATE TABLE `indirizzo` (
  `Via` varchar(50) NOT NULL,
  `N` int(11) NOT NULL,
  `Provincia` varchar(50) NOT NULL,
  `CAP` int(11) NOT NULL,
  `Stato` varchar(50) NOT NULL,
  `Citta` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `indirizzo`
--

INSERT INTO `indirizzo` (`Via`, `N`, `Provincia`, `CAP`, `Stato`, `Citta`) VALUES
(' Piazza Rinascimento', 13, 'PU', 61029, 'Italia', 'Urbino'),
('Piazza del Popolo', 1, 'PU', 61121, 'Italia', 'Pesaro'),
('Piazza salotto', 20, 'PE', 65100, 'Italia', 'Pescara'),
('Via Appia Nuova', 1245, 'RM', 178, 'Italia', 'Roma'),
('Via Barberini', 45, 'PU', 61040, 'Italia', 'Castelvecchio'),
('Via Cavour', 1, 'PU', 61032, 'Italia', 'Fano'),
('Via Cesare Simonetti', 2, 'PU', 61032, 'Italia', 'Fano'),
('Via Cima Tosa', 25, 'TN', 38086, 'Italia', 'Madonna di Campiglio'),
('Via Garibaldi', 27, 'PU', 60121, 'Italia', 'Ancona'),
('Via Giambattista Fiorini', 23, 'AN', 60019, 'Italia', 'Senigallia'),
('Via Mario Angeloni', 335, 'FC', 47521, 'Italia', 'Cesena');

-- --------------------------------------------------------

--
-- Table structure for table `messaggio`
--

CREATE TABLE `messaggio` (
  `IdMessaggio` int(11) NOT NULL,
  `Oggetto` varchar(50) NOT NULL,
  `Testo` varchar(1000) NOT NULL,
  `UsernameMittente` varchar(50) NOT NULL,
  `Eliminato` tinyint(4) NOT NULL DEFAULT 0,
  `DataInvio` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messaggio`
--

INSERT INTO `messaggio` (`IdMessaggio`, `Oggetto`, `Testo`, `UsernameMittente`, `Eliminato`, `DataInvio`) VALUES
(1, 'Orario di apertura', ' Salve, volevo sapere se anche la domenica mattina la pista rimane aperta. Grazie.', 'luca99', 0, '2020-01-07 03:39:48'),
(2, 'Parcheggio', 'Salve, volevo sapere se il locale dispone di un parcheggio custodito, e se si quanto costa. Grazie e buona serata.\r\n ', 'luca99', 0, '2020-01-07 03:40:51'),
(3, 'Concerto', 'Salve, è possibile avere la scaletta dei brani anticipatamente? Grazie. ', 'luca99', 0, '2020-01-07 03:57:32'),
(4, 'Augurio di inizio anno', 'La redazione di Pasuta.it coglie l\'occasione per augurare un buon anno a tutti i suoi iscritti! ', 'Sommo', 0, '2020-01-07 04:10:37'),
(5, 'Domanda', 'Salve, verso che ora si terrà il lancio dei dolciumi? ', 'luca99', 0, '2020-01-07 04:37:10');

-- --------------------------------------------------------

--
-- Table structure for table `osservazione`
--

CREATE TABLE `osservazione` (
  `idEvento` int(11) NOT NULL,
  `Username` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `osservazione`
--

INSERT INTO `osservazione` (`idEvento`, `Username`) VALUES
(1, 'Leila123'),
(1, 'luca99'),
(1, 'mario98'),
(2, 'Cerusso99'),
(2, 'Leila123'),
(2, 'luca99'),
(2, 'Ricatald80'),
(3, 'Cerusso99'),
(3, 'CremoC80'),
(3, 'filo93'),
(3, 'luca99'),
(3, 'maurizio73'),
(3, 'Ricatald80'),
(4, 'Cerusso99'),
(4, 'Leila123'),
(4, 'luca99'),
(4, 'maurizio73'),
(7, 'luca99'),
(7, 'maurizio73'),
(8, 'filo93'),
(8, 'luca99'),
(8, 'maurizio73'),
(9, 'luca99'),
(9, 'maurizio73'),
(11, 'CremoC80'),
(11, 'mario98'),
(13, 'filo93'),
(13, 'maurizio73'),
(15, 'filo93'),
(15, 'maurizio73');

-- --------------------------------------------------------

--
-- Table structure for table `persona`
--

CREATE TABLE `persona` (
  `Nome` varchar(50) NOT NULL,
  `Cognome` varchar(50) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Mail` varchar(50) NOT NULL,
  `DataNascita` date NOT NULL,
  `DataRegistrazione` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `persona`
--

INSERT INTO `persona` (`Nome`, `Cognome`, `Username`, `Password`, `Mail`, `DataNascita`, `DataRegistrazione`) VALUES
('Celeste', 'Russo', 'Cerusso99', 'cielo123', 'celeste.russo@hotmail.com', '1999-11-12', '2020-01-05 16:05:34'),
('Corrado', 'Cremonesi', 'CremoC80', 'crema001', 'CCremo@gmail.com', '1997-12-15', '2020-01-05 16:09:28'),
('Antonio', 'Delle Canne', 'Einstein', 'admin', 'antonio.dellecanne@studio.unibo.it', '1999-03-06', '2020-01-05 13:00:40'),
('Fabio', 'Casanova', 'Fabio65', 'fabio1965', 'casanovafb@hotmail.com', '1965-02-21', '2020-01-07 04:08:26'),
('Andrea', 'Ferretti', 'Ferro98', 'ferro1998', 'andreaferretti@gmail.com', '1998-04-21', '2020-01-07 05:26:19'),
('Filippo', 'Pensi', 'filo93', 'filo1993', 'filo.pensi@libero.it', '1993-06-12', '2020-01-07 06:31:58'),
('Gabriele', 'Manfrin', 'GabriM80', 'cane1234', 'GbManfri@libero.it', '1980-06-12', '2020-01-05 13:35:27'),
('Giuditta', 'Castiglione', 'GiudiCa14', 'gatto1234', 'Gcastiglione@gmail.com', '1997-04-14', '2020-01-05 13:37:56'),
('Cataldo', 'Bergamaschi', 'Imest1934', 'tredici13', 'CataldoBergamaschi@dayrep.com', '1995-10-20', '2020-01-05 13:31:07'),
('Leila', 'Rossi', 'Leila123', 'chewbacca1', 'lerossi@gmail.com', '2000-01-01', '2020-01-05 16:07:06'),
('Luca', 'Verdi', 'luca99', 'luca1999', 'luca.verdi@yahoo.it', '1999-09-21', '2020-01-07 03:02:09'),
('Marco', 'Magnani', 'MarcheEventi01', 'marco1970', 'marcomagnani@gmail.com', '1970-12-01', '2020-01-07 04:05:10'),
('Mario', 'Rossi', 'mario98', 'mario1998', 'mariorossi@gmail.com', '1998-01-11', '2020-01-07 02:58:24'),
('Maurizio', 'Falcioni', 'maurizio73', 'maurizio1973', 'maurizio73@gmail.com', '1973-03-12', '2020-01-07 05:12:37'),
('Luca', 'Molini', 'Molo80', 'molo1980', 'molo.luca@gmail.com', '1980-01-01', '2020-01-07 05:24:21'),
('Omar', 'Bagari', 'Obagari5', '1234abcd', 'omar.bagari@gmail.com', '1990-02-05', '2020-01-05 13:26:44'),
('Cataldo', 'Ricci', 'Ricatald80', 'zebra1234', 'cataricci@libero.it', '1994-07-21', '2020-01-05 16:08:12'),
('Keivan', 'Ameri', 'Sommo', 'admin', 'keivan.ameri@studio.unibo.it', '1998-02-10', '2020-01-05 13:00:40');

-- --------------------------------------------------------

--
-- Table structure for table `promemoria`
--

CREATE TABLE `promemoria` (
  `IdPromemoria` int(11) NOT NULL,
  `Testo` varchar(500) NOT NULL,
  `idEvento` int(11) NOT NULL,
  `DataInvio` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `promemoria`
--

INSERT INTO `promemoria` (`IdPromemoria`, `Testo`, `idEvento`, `DataInvio`) VALUES
(1, 'L\'evento Festa delle Streghe inizia oggi', 2, '2020-01-05 16:02:47'),
(2, 'L\'evento Apertura pista di pattinaggio inizia oggi', 3, '2020-01-05 16:17:58'),
(3, 'L\'evento Aperitivo al Bombardino Apres Ski inizia tra pochi giorni', 6, '2020-01-05 16:34:18'),
(4, 'Il tuo evento \'Aperitivo al Bombardino Apres Ski\' e\' stato rimosso da un amministratore!', 6, '2020-01-05 17:01:09'),
(5, 'L\'evento Aperitivo al Bombardino Apres Ski a cui avresti partecipato e\' stato cancellato, il tuo acquisto sarÃ  rimborsato a breve', 6, '2020-01-05 17:01:30'),
(6, 'Il tuo evento \'Festa del Brodetto\' e\' stato rimosso da un amministratore!', 11, '2020-01-07 05:01:51'),
(7, 'L\'evento Festa del Brodetto che stavi osservando e\' stato cancellato', 11, '2020-01-07 05:10:52'),
(8, 'L\'evento Edoardo Tresoldi - Installazione temporanea inizia oggi', 15, '2020-01-07 06:22:47');

-- --------------------------------------------------------

--
-- Table structure for table `promemoria_creatore`
--

CREATE TABLE `promemoria_creatore` (
  `IdPromemoria` int(11) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `Eliminato` tinyint(4) NOT NULL DEFAULT 0,
  `Visto` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `promemoria_creatore`
--

INSERT INTO `promemoria_creatore` (`IdPromemoria`, `Username`, `Eliminato`, `Visto`) VALUES
(1, 'GabriM80', 0, 0),
(2, 'GiudiCa14', 0, 0),
(3, 'GiudiCa14', 0, 0),
(4, 'GiudiCa14', 0, 0),
(6, 'MarcheEventi01', 0, 0),
(8, 'Fabio65', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `promemoria_utente`
--

CREATE TABLE `promemoria_utente` (
  `IdPromemoria` int(11) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `Eliminato` tinyint(4) NOT NULL DEFAULT 0,
  `Visto` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `promemoria_utente`
--

INSERT INTO `promemoria_utente` (`IdPromemoria`, `Username`, `Eliminato`, `Visto`) VALUES
(1, 'Cerusso99', 0, 0),
(2, 'Cerusso99', 0, 0),
(3, 'Cerusso99', 0, 0),
(5, 'CremoC80', 0, 0),
(7, 'mario98', 0, 0),
(8, 'maurizio73', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `utente`
--

CREATE TABLE `utente` (
  `Ban` tinyint(1) NOT NULL,
  `Username` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `utente`
--

INSERT INTO `utente` (`Ban`, `Username`) VALUES
(0, 'Cerusso99'),
(0, 'CremoC80'),
(0, 'filo93'),
(0, 'Leila123'),
(0, 'luca99'),
(0, 'mario98'),
(0, 'maurizio73'),
(0, 'Ricatald80');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `amministratore`
--
ALTER TABLE `amministratore`
  ADD PRIMARY KEY (`Username`);

--
-- Indexes for table `biglietto_venduto`
--
ALTER TABLE `biglietto_venduto`
  ADD PRIMARY KEY (`idEvento`,`NumeroBiglietto`),
  ADD KEY `FKR_BU` (`Username`);

--
-- Indexes for table `carrello`
--
ALTER TABLE `carrello`
  ADD PRIMARY KEY (`idEvento`,`Username`),
  ADD KEY `FKR_aggiunta` (`Username`);

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`IdCategoria`,`idEvento`);

--
-- Indexes for table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`IdCategoria`);

--
-- Indexes for table `creatore`
--
ALTER TABLE `creatore`
  ADD PRIMARY KEY (`Username`),
  ADD UNIQUE KEY `SID_CREATORE_1` (`CartaIdentita`),
  ADD UNIQUE KEY `SID_CREATORE` (`CF`);

--
-- Indexes for table `destinatario_msg`
--
ALTER TABLE `destinatario_msg`
  ADD PRIMARY KEY (`UsernameDestinatario`,`IdMessaggio`),
  ADD KEY `FKR_DEST_MESS` (`IdMessaggio`);

--
-- Indexes for table `evento`
--
ALTER TABLE `evento`
  ADD PRIMARY KEY (`idEvento`),
  ADD KEY `FKR_EC` (`Username`),
  ADD KEY `FKR_EI` (`Via`,`N`,`CAP`);

--
-- Indexes for table `indirizzo`
--
ALTER TABLE `indirizzo`
  ADD PRIMARY KEY (`Via`,`N`,`CAP`);

--
-- Indexes for table `messaggio`
--
ALTER TABLE `messaggio`
  ADD PRIMARY KEY (`IdMessaggio`),
  ADD KEY `FKMittente` (`UsernameMittente`);

--
-- Indexes for table `osservazione`
--
ALTER TABLE `osservazione`
  ADD PRIMARY KEY (`idEvento`,`Username`),
  ADD KEY `FKR_OU` (`Username`);

--
-- Indexes for table `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`Username`),
  ADD UNIQUE KEY `SID_PERSONA` (`Mail`);

--
-- Indexes for table `promemoria`
--
ALTER TABLE `promemoria`
  ADD PRIMARY KEY (`IdPromemoria`),
  ADD KEY `FKricordo` (`idEvento`);

--
-- Indexes for table `promemoria_creatore`
--
ALTER TABLE `promemoria_creatore`
  ADD PRIMARY KEY (`IdPromemoria`),
  ADD KEY `FKR_PROC_CRE` (`Username`);

--
-- Indexes for table `promemoria_utente`
--
ALTER TABLE `promemoria_utente`
  ADD PRIMARY KEY (`IdPromemoria`),
  ADD KEY `FKR_PRO_U` (`Username`);

--
-- Indexes for table `utente`
--
ALTER TABLE `utente`
  ADD PRIMARY KEY (`Username`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `amministratore`
--
ALTER TABLE `amministratore`
  ADD CONSTRAINT `FKR_AP` FOREIGN KEY (`Username`) REFERENCES `persona` (`Username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `biglietto_venduto`
--
ALTER TABLE `biglietto_venduto`
  ADD CONSTRAINT `FKR_BE` FOREIGN KEY (`idEvento`) REFERENCES `evento` (`idEvento`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FKR_BU` FOREIGN KEY (`Username`) REFERENCES `utente` (`Username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `carrello`
--
ALTER TABLE `carrello`
  ADD CONSTRAINT `FKR_aggiunta` FOREIGN KEY (`Username`) REFERENCES `utente` (`Username`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FKcar_EVE` FOREIGN KEY (`idEvento`) REFERENCES `evento` (`idEvento`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `categoria`
--
ALTER TABLE `categoria`
  ADD CONSTRAINT `FKcat_CAT` FOREIGN KEY (`IdCategoria`) REFERENCES `categorie` (`IdCategoria`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `creatore`
--
ALTER TABLE `creatore`
  ADD CONSTRAINT `FKR_CP` FOREIGN KEY (`Username`) REFERENCES `persona` (`Username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `destinatario_msg`
--
ALTER TABLE `destinatario_msg`
  ADD CONSTRAINT `FKDes_PER` FOREIGN KEY (`UsernameDestinatario`) REFERENCES `persona` (`Username`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FKR_DEST_MESS` FOREIGN KEY (`IdMessaggio`) REFERENCES `messaggio` (`IdMessaggio`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `evento`
--
ALTER TABLE `evento`
  ADD CONSTRAINT `FKR_EC` FOREIGN KEY (`Username`) REFERENCES `creatore` (`Username`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FKR_EI` FOREIGN KEY (`Via`,`N`,`CAP`) REFERENCES `indirizzo` (`Via`, `N`, `CAP`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `messaggio`
--
ALTER TABLE `messaggio`
  ADD CONSTRAINT `FKMittente` FOREIGN KEY (`UsernameMittente`) REFERENCES `persona` (`Username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `osservazione`
--
ALTER TABLE `osservazione`
  ADD CONSTRAINT `FKR_OU` FOREIGN KEY (`Username`) REFERENCES `utente` (`Username`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FKoss_EVE` FOREIGN KEY (`idEvento`) REFERENCES `evento` (`idEvento`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `promemoria`
--
ALTER TABLE `promemoria`
  ADD CONSTRAINT `FKricordo` FOREIGN KEY (`idEvento`) REFERENCES `evento` (`idEvento`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `promemoria_creatore`
--
ALTER TABLE `promemoria_creatore`
  ADD CONSTRAINT `FKR_PROC_CRE` FOREIGN KEY (`Username`) REFERENCES `creatore` (`Username`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FKR_PROC_PRO` FOREIGN KEY (`IdPromemoria`) REFERENCES `promemoria` (`IdPromemoria`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `promemoria_utente`
--
ALTER TABLE `promemoria_utente`
  ADD CONSTRAINT `FKR_PROU_PRO` FOREIGN KEY (`IdPromemoria`) REFERENCES `promemoria` (`IdPromemoria`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FKR_PRO_U` FOREIGN KEY (`Username`) REFERENCES `utente` (`Username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `utente`
--
ALTER TABLE `utente`
  ADD CONSTRAINT `FKR_UP` FOREIGN KEY (`Username`) REFERENCES `persona` (`Username`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
