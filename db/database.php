<?php

class DatabaseHelper{
    private $db;
    
    public function __construct($servername, $username, $password, $dbname){
        $this->db = new mysqli($servername, $username, $password, $dbname);
        if($this->db->connect_error){
            die("Connesione fallita al db");
        }
    }
    
    public function getRandomPosts($n=2){
        $stmt = $this->db->prepare("SELECT idarticolo, titoloarticolo, imgarticolo FROM articolo ORDER BY RAND() LIMIT ?");
        $stmt->bind_param("i", $n);
        $stmt->execute();
        $result = $stmt->get_result();
        
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getCategories(){
        $stmt = $this->db->prepare("SELECT idcategoria, nomecategoria FROM categoria");
        $stmt->execute();
        $result = $stmt->get_result();
        
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getArticoliRecenti($n=2){
        $stmt = $this->db->prepare("SELECT idarticolo, titoloarticolo, imgarticolo, testoarticolo, anteprimaarticolo, dataarticolo, idautore, nome FROM articolo ar INNER JOIN autore au ON ar.autore = au.idautore ORDER BY dataarticolo LIMIT ?");
        $stmt->bind_param("i", $n);
        $stmt->execute();
        $result = $stmt->get_result();
        
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getEventiPopolari($n=10){
        $query = "SELECT *
        FROM
        (SELECT evo.idEvento, evo.Immagine, evo.DataInizio, evo.OraInizio, evo.NomeEvento, evo.DataFine, evo.OraFine, evo.NomeLuogo, evo.Prezzo, evo.Eliminato
        FROM evento evo INNER JOIN osservazione oss ON evo.idEvento = oss.idEvento
        UNION ALL
        SELECT ev.idEvento, ev.Immagine, ev.DataInizio, ev.OraInizio, ev.NomeEvento, ev.DataFine, ev.OraFine, ev.NomeLuogo, ev.Prezzo, ev.Eliminato
        FROM evento ev
        UNION ALL
        SELECT evv.idEvento, evv.Immagine, evv.DataInizio, evv.OraInizio, evv.NomeEvento, evv.DataFine, evv.OraFine, evv.NomeLuogo, evv.Prezzo, evv.Eliminato
        FROM evento evv INNER JOIN biglietto_venduto ven ON evv.idEvento = ven.idEvento) as res 
        WHERE DataFine > CURDATE() AND Eliminato = 0
        GROUP BY res.idEvento
        ORDER BY COUNT(*) DESC, res.DataInizio ASC
        LIMIT ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $n);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getPartecipanti($idEvento){
        $stmt = $this->db->prepare("SELECT * FROM evento evv INNER JOIN biglietto_venduto ven ON evv.idEvento = ven.idEvento WHERE evv.idEvento = ?");
        $stmt->bind_param("i", $idEvento);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->num_rows;
    }
    
    public function getInteressati($idEvento){
        $stmt = $this->db->prepare("SELECT * FROM evento evv INNER JOIN osservazione ven ON evv.idEvento = ven.idEvento WHERE evv.idEvento = ?");
        $stmt->bind_param("i", $idEvento);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->num_rows;
    }
    
    public function getIndirizzo($idEvento){
        $stmt = $this->db->prepare("SELECT * FROM evento evv INNER JOIN indirizzo ind ON evv.Via = ind.Via AND evv.CAP = ind.CAP AND evv.N = ind.N WHERE evv.idEvento = ?");
        $stmt->bind_param("i", $idEvento);
        $stmt->execute();
        $result = $stmt->get_result();
        $res = $result->fetch_array(MYSQLI_ASSOC);
        return $res["Via"].", ".$res["N"].", ".$res["Citta"].", ".$res["CAP"].", ".$res["Provincia"].", ".$res["Stato"];
    }
    
    public function getEventoById($idEvento){
        $stmt = $this->db->prepare("SELECT * FROM evento evv INNER JOIN indirizzo ind ON evv.Via = ind.Via AND evv.CAP = ind.CAP AND evv.N = ind.N WHERE evv.idEvento = ?");
        $stmt->bind_param("i", $idEvento);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_array(MYSQLI_ASSOC);
    }
    
    public function deleteEventoById($idEvento){
        $stmt = $this->db->prepare("UPDATE evento SET Eliminato = '1' WHERE idEvento = ?");
        $stmt->bind_param("i", $idEvento);
        $stmt->execute();
    }
    
    public function aggiungiUtente($nome, $cognome, $username, $password, $mail, $dataNascita){
        $stmt = $this->db->prepare("INSERT INTO persona (Nome, Cognome, Username, Password, Mail, DataNascita) VALUES (?, ?, ?, ?, ?, ?)");
        $stmt->bind_param("ssssss", $nome, $cognome, $username, $password, $mail, $dataNascita);
        $stmt->execute();
        $value = 0;
        $stmt = $this->db->prepare("INSERT INTO utente (Ban, Username) VALUES (?, ?)");
        $stmt->bind_param("is", $value, $username);
        $stmt->execute();
    }
    
    public function aggiungiCreatore($nome, $cognome, $username, $password, $mail, $dataNascita, $cf, $cartaId, $iban){
        $stmt = $this->db->prepare("INSERT INTO persona (Nome, Cognome, Username, Password, Mail, DataNascita) VALUES (?, ?, ?, ?, ?, ?)");
        $stmt->bind_param("ssssss", $nome, $cognome, $username, $password, $mail, $dataNascita);
        $stmt->execute();
        $value = 0;
        $stmt = $this->db->prepare("INSERT INTO creatore (Ban, Username, CF, CartaIdentita, IBAN, Accettato) VALUES (?, ?, ?, ?, ?, ?)");
        $stmt->bind_param("issssi", $value, $username, $cf, $cartaId, $iban, $value);
        $stmt->execute();
    }
    
    public function contains($table, $field, $value, $type){
        $stmt = $this->db->prepare("SELECT * FROM $table WHERE $field = ?");
        $stmt->bind_param($type, $value);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->num_rows > 0;
    }
    
    public function checkCredentials($username, $password){
        $stmt = $this->db->prepare("SELECT * FROM persona WHERE Username = ? AND Password = ?");
        $stmt->bind_param("ss", $username, $password);
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows == 0){
            return null;
        }
        $stmt = $this->db->prepare("SELECT * FROM persona p
        INNER JOIN amministratore a ON p.Username = a.Username WHERE p.Username = ?");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows > 0){
            return "admin";
        }
        $stmt = $this->db->prepare("SELECT Ban FROM persona p
        INNER JOIN utente u ON p.Username = u.Username WHERE p.Username = ?");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows > 0){
            $row = $result->fetch_array(MYSQLI_ASSOC);
            if($row["Ban"]) return null;
            return "utente";
        }
        $stmt = $this->db->prepare("SELECT Ban, Accettato FROM persona p
        INNER JOIN creatore c ON p.Username = c.Username WHERE p.Username = ?");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows > 0){
            $row = $result->fetch_array(MYSQLI_ASSOC);
            if($row["Ban"]) return null;
            if(!$row["Accettato"]) return null;
            return "creatore";
        }
        return null;
    }
    
    private function getNextId($table, $idName){
        $stmt = $this->db->prepare("SELECT MAX($idName) FROM $table");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_array(MYSQLI_NUM)[0] + 1;
    }
    
    /*----------------------------------------------------*/
    /* --------------- EVENTI CREATORE ------------------ */
    /*----------------------------------------------------*/
    
    
    public function getEventiCretore($username, $type){
        switch($type){
            case "dataAsc":
                $stmt = $this->db->prepare("SELECT * FROM evento 
                WHERE username = ?
                ORDER BY DataInizio ASC");
            break;
            case "dataDesc":
                $stmt = $this->db->prepare("SELECT * FROM evento 
                WHERE username = ?
                ORDER BY DataInizio DESC");
            break;
            case "nomeAsc":
                $stmt = $this->db->prepare("SELECT * FROM evento 
                WHERE username = ?
                ORDER BY NomeEvento ASC");
            break;
            case "nomeDesc":
                $stmt = $this->db->prepare("SELECT * FROM evento 
                WHERE username = ?
                ORDER BY NomeEvento DESC");
            break;
        }
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getNEventiCreatoreFuturi($n, $username){
        $stmt = $this->db->prepare("SELECT * FROM evento 
        WHERE username = ? AND DataInizio >= CURDATE() AND Eliminato = 0
        ORDER BY DataInizio ASC
        LIMIT ?");
        $stmt->bind_param("si", $username, $n);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getEventiCretoreFuturi($username, $type){
        switch($type){
            case "dataAsc":
                $stmt = $this->db->prepare("SELECT * FROM evento 
                WHERE username = ? AND DataInizio >= CURDATE() AND Eliminato = 0
                ORDER BY DataInizio ASC");
            break;
            case "dataDesc":
                $stmt = $this->db->prepare("SELECT * FROM evento 
                WHERE username = ? AND DataInizio >= CURDATE() AND Eliminato = 0
                ORDER BY DataInizio DESC");
            break;
            case "nomeAsc":
                $stmt = $this->db->prepare("SELECT * FROM evento 
                WHERE username = ? AND DataInizio >= CURDATE() AND Eliminato = 0
                ORDER BY NomeEvento ASC");
            break;
            case "nomeDesc":
                $stmt = $this->db->prepare("SELECT * FROM evento 
                WHERE username = ? AND DataInizio >= CURDATE() AND Eliminato = 0
                ORDER BY NomeEvento DESC");
            break;
        }
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getNEventiCreatorePassati($n, $username){
        $stmt = $this->db->prepare("SELECT * FROM evento 
        WHERE username = ? AND DataInizio < CURDATE() AND Eliminato = 0
        ORDER BY DataInizio DESC
        LIMIT ?");
        $stmt->bind_param("si", $username, $n);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getEventiCreatorePassati($username, $type){
        switch($type){
            case "dataAsc":
                $stmt = $this->db->prepare("SELECT * FROM evento 
                WHERE username = ? AND DataInizio < CURDATE() AND Eliminato = 0
                ORDER BY DataInizio ASC");
            break;
            case "dataDesc":
                $stmt = $this->db->prepare("SELECT * FROM evento 
                WHERE username = ? AND DataInizio < CURDATE() AND Eliminato = 0
                ORDER BY DataInizio DESC");
            break;
            case "nomeAsc":
                $stmt = $this->db->prepare("SELECT * FROM evento 
                WHERE username = ? AND DataInizio < CURDATE() AND Eliminato = 0
                ORDER BY NomeEvento ASC");
            break;
            case "nomeDesc":
                $stmt = $this->db->prepare("SELECT * FROM evento 
                WHERE username = ? AND DataInizio < CURDATE() AND Eliminato = 0
                ORDER BY NomeEvento DESC");
            break;
        }
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getNEventiCreatoreEliminati($n, $username){
        $stmt = $this->db->prepare("SELECT * FROM evento 
        WHERE username = ? AND Eliminato = 1                
        ORDER BY DataInizio DESC
        LIMIT ?");
        $stmt->bind_param("si", $username, $n);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getEventiCreatoreEliminati($username, $type){
        switch($type){
            case "dataAsc":
                $stmt = $this->db->prepare("SELECT * FROM evento 
                WHERE username = ? AND Eliminato = 1
                ORDER BY DataInizio ASC");
            break;
            case "dataDesc":
                $stmt = $this->db->prepare("SELECT * FROM evento 
                WHERE username = ? AND Eliminato = 1
                ORDER BY DataInizio DESC");
            break;
            case "nomeAsc":
                $stmt = $this->db->prepare("SELECT * FROM evento 
                WHERE username = ? AND Eliminato = 1
                ORDER BY NomeEvento ASC");
            break;
            case "nomeDesc":
                $stmt = $this->db->prepare("SELECT * FROM evento 
                WHERE username = ? AND Eliminato = 1
                ORDER BY NomeEvento DESC");
            break;
        }
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    
    
    /*----------------------------------------------------*/
    /* ---------------- ACQUISTATI ---------------------- */
    /*----------------------------------------------------*/
    
    
    public function getNEventiAcquistatiFuturi($n, $username){
        $stmt = $this->db->prepare("SELECT evv.idEvento, NomeEvento, Immagine, DataInizio, OraInizio, DataFine, OraFine, NomeLuogo, Via, N, CAP, Eliminato, DataVendita, evv.Username, COUNT(*) AS NumeroBiglietti, (COUNT(*) * Prezzo) AS Totale
        FROM evento evv
        INNER JOIN biglietto_venduto acq ON evv.idEvento = acq.idEvento
        WHERE acq.Username = ?
        AND evv.DataFine >= CURDATE() AND evv.Eliminato = 0
        GROUP BY DataVendita, acq.idEvento
        ORDER BY DataInizio ASC
        LIMIT ?");
        $stmt->bind_param("si", $username, $n);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getEventiAcquistatiFuturi($username, $type){
        switch($type){
            case "dataAsc":
                $stmt = $this->db->prepare("SELECT evv.idEvento, NomeEvento, Immagine, DataInizio, OraInizio, DataFine, OraFine, NomeLuogo, Via, N, CAP, Eliminato, DataVendita, evv.Username, COUNT(*) AS NumeroBiglietti, (COUNT(*) * Prezzo) AS Totale
                FROM evento evv
                INNER JOIN biglietto_venduto acq ON evv.idEvento = acq.idEvento
                WHERE acq.Username = ?
                AND evv.DataFine >= CURDATE() AND evv.Eliminato = 0
                GROUP BY DataVendita, acq.idEvento
                ORDER BY DataInizio ASC");
            break;
            case "dataDesc":
                $stmt = $this->db->prepare("SELECT evv.idEvento, NomeEvento, Immagine, DataInizio, OraInizio, DataFine, OraFine, NomeLuogo, Via, N, CAP, Eliminato, DataVendita, evv.Username, COUNT(*) AS NumeroBiglietti, (COUNT(*) * Prezzo) AS Totale
                FROM evento evv
                INNER JOIN biglietto_venduto acq ON evv.idEvento = acq.idEvento
                WHERE acq.Username = ?
                AND evv.DataFine >= CURDATE() AND evv.Eliminato = 0
                GROUP BY DataVendita, acq.idEvento
                ORDER BY DataInizio DESC");
            break;
            case "nomeAsc":
                $stmt = $this->db->prepare("SELECT evv.idEvento, NomeEvento, Immagine, DataInizio, OraInizio, DataFine, OraFine, NomeLuogo, Via, N, CAP, Eliminato, DataVendita, evv.Username, COUNT(*) AS NumeroBiglietti, (COUNT(*) * Prezzo) AS Totale
                FROM evento evv
                INNER JOIN biglietto_venduto acq ON evv.idEvento = acq.idEvento
                WHERE acq.Username = ?
                AND evv.DataFine >= CURDATE() AND evv.Eliminato = 0
                GROUP BY DataVendita, acq.idEvento
                ORDER BY NomeEvento ASC");
            break;
            case "nomeDesc":
                $stmt = $this->db->prepare("SELECT evv.idEvento, NomeEvento, Immagine, DataInizio, OraInizio, DataFine, OraFine, NomeLuogo, Via, N, CAP, Eliminato, DataVendita, evv.Username, COUNT(*) AS NumeroBiglietti, (COUNT(*) * Prezzo) AS Totale
                FROM evento evv
                INNER JOIN biglietto_venduto acq ON evv.idEvento = acq.idEvento
                WHERE acq.Username = ?
                AND evv.DataFine >= CURDATE() AND evv.Eliminato = 0
                GROUP BY DataVendita, acq.idEvento
                ORDER BY NomeEvento DESC");
            break;
            case "acqAsc":
                $stmt = $this->db->prepare("SELECT evv.idEvento, NomeEvento, Immagine, DataInizio, OraInizio, DataFine, OraFine, NomeLuogo, Via, N, CAP, Eliminato, DataVendita, evv.Username, COUNT(*) AS NumeroBiglietti, (COUNT(*) * Prezzo) AS Totale
                FROM evento evv
                INNER JOIN biglietto_venduto acq ON evv.idEvento = acq.idEvento
                WHERE acq.Username = ?
                AND evv.DataFine >= CURDATE() AND evv.Eliminato = 0
                GROUP BY DataVendita, acq.idEvento
                ORDER BY DataVendita ASC");
            break;
            case "acqDesc":
                $stmt = $this->db->prepare("SELECT evv.idEvento, NomeEvento, Immagine, DataInizio, OraInizio, DataFine, OraFine, NomeLuogo, Via, N, CAP, Eliminato, DataVendita, evv.Username, COUNT(*) AS NumeroBiglietti, (COUNT(*) * Prezzo) AS Totale
                FROM evento evv
                INNER JOIN biglietto_venduto acq ON evv.idEvento = acq.idEvento
                WHERE acq.Username = ?
                AND evv.DataFine >= CURDATE() AND evv.Eliminato = 0
                GROUP BY DataVendita, acq.idEvento
                ORDER BY DataVendita DESC");
            break;
        }
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getNEventiAcquistatiPassati($n, $username){
        $stmt = $this->db->prepare("SELECT evv.idEvento, NomeEvento, Immagine, DataInizio, OraInizio, DataFine, OraFine, NomeLuogo, Via, N, CAP, Eliminato, DataVendita, evv.Username, COUNT(*) AS NumeroBiglietti, (COUNT(*) * Prezzo) AS Totale
        FROM evento evv
        INNER JOIN biglietto_venduto acq ON evv.idEvento = acq.idEvento
        WHERE acq.Username = ?
        AND evv.DataFine < CURDATE() AND evv.Eliminato = 0
        GROUP BY DataVendita, acq.idEvento
        ORDER BY DataInizio DESC
        LIMIT ?");
        $stmt->bind_param("si", $username, $n);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getEventiAcquistatiPassati($username, $type){
        switch($type){
            case "dataAsc":
                $stmt = $this->db->prepare("SELECT evv.idEvento, NomeEvento, Immagine, DataInizio, OraInizio, DataFine, OraFine, NomeLuogo, Via, N, CAP, Eliminato, DataVendita, evv.Username, COUNT(*) AS NumeroBiglietti, (COUNT(*) * Prezzo) AS Totale
                FROM evento evv
                INNER JOIN biglietto_venduto acq ON evv.idEvento = acq.idEvento
                WHERE acq.Username = ?
                AND evv.DataFine < CURDATE() AND evv.Eliminato = 0
                GROUP BY DataVendita, acq.idEvento
                ORDER BY DataInizio ASC");
            break;
            case "dataDesc":
                $stmt = $this->db->prepare("SELECT evv.idEvento, NomeEvento, Immagine, DataInizio, OraInizio, DataFine, OraFine, NomeLuogo, Via, N, CAP, Eliminato, DataVendita, evv.Username, COUNT(*) AS NumeroBiglietti, (COUNT(*) * Prezzo) AS Totale
                FROM evento evv
                INNER JOIN biglietto_venduto acq ON evv.idEvento = acq.idEvento
                WHERE acq.Username = ?
                AND evv.DataFine < CURDATE() AND evv.Eliminato = 0
                GROUP BY DataVendita, acq.idEvento
                ORDER BY DataInizio DESC");
            break;
            case "nomeAsc":
                $stmt = $this->db->prepare("SELECT evv.idEvento, NomeEvento, Immagine, DataInizio, OraInizio, DataFine, OraFine, NomeLuogo, Via, N, CAP, Eliminato, DataVendita, evv.Username, COUNT(*) AS NumeroBiglietti, (COUNT(*) * Prezzo) AS Totale
                FROM evento evv
                INNER JOIN biglietto_venduto acq ON evv.idEvento = acq.idEvento
                WHERE acq.Username = ?
                AND evv.DataFine < CURDATE() AND evv.Eliminato = 0
                GROUP BY DataVendita, acq.idEvento
                ORDER BY NomeEvento ASC");
            break;
            case "nomeDesc":
                $stmt = $this->db->prepare("SELECT evv.idEvento, NomeEvento, Immagine, DataInizio, OraInizio, DataFine, OraFine, NomeLuogo, Via, N, CAP, Eliminato, DataVendita, evv.Username, COUNT(*) AS NumeroBiglietti, (COUNT(*) * Prezzo) AS Totale
                FROM evento evv
                INNER JOIN biglietto_venduto acq ON evv.idEvento = acq.idEvento
                WHERE acq.Username = ?
                AND evv.DataFine < CURDATE() AND evv.Eliminato = 0
                GROUP BY DataVendita, acq.idEvento
                ORDER BY NomeEvento DESC");
            break;
            case "acqAsc":
                $stmt = $this->db->prepare("SELECT evv.idEvento, NomeEvento, Immagine, DataInizio, OraInizio, DataFine, OraFine, NomeLuogo, Via, N, CAP, Eliminato, DataVendita, evv.Username, COUNT(*) AS NumeroBiglietti, (COUNT(*) * Prezzo) AS Totale
                FROM evento evv
                INNER JOIN biglietto_venduto acq ON evv.idEvento = acq.idEvento
                WHERE acq.Username = ?
                AND evv.DataFine < CURDATE() AND evv.Eliminato = 0
                GROUP BY DataVendita, acq.idEvento
                ORDER BY DataVendita ASC");
            break;
            case "acqDesc":
                $stmt = $this->db->prepare("SELECT evv.idEvento, NomeEvento, Immagine, DataInizio, OraInizio, DataFine, OraFine, NomeLuogo, Via, N, CAP, Eliminato, DataVendita, evv.Username, COUNT(*) AS NumeroBiglietti, (COUNT(*) * Prezzo) AS Totale
                FROM evento evv
                INNER JOIN biglietto_venduto acq ON evv.idEvento = acq.idEvento
                WHERE acq.Username = ?
                AND evv.DataFine < CURDATE() AND evv.Eliminato = 0
                GROUP BY DataVendita, acq.idEvento
                ORDER BY DataVendita DESC");
            break;
        }
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getNEventiAcquistatiEliminati($n, $username){
        $stmt = $this->db->prepare("SELECT evv.idEvento, NomeEvento, Immagine, DataInizio, OraInizio, DataFine, OraFine, NomeLuogo, Via, N, CAP, Eliminato, DataVendita, evv.Username, COUNT(*) AS NumeroBiglietti, (COUNT(*) * Prezzo) AS Totale
        FROM evento evv
        INNER JOIN biglietto_venduto acq ON evv.idEvento = acq.idEvento
        WHERE acq.Username = ?
        AND evv.Eliminato = 1
        GROUP BY DataVendita, acq.idEvento
        ORDER BY DataInizio ASC
        LIMIT ?");
        $stmt->bind_param("si", $username, $n);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getEventiAcquistatiEliminati($username, $type){
        switch($type){
            case "dataAsc":
                $stmt = $this->db->prepare("SELECT evv.idEvento, NomeEvento, Immagine, DataInizio, OraInizio, DataFine, OraFine, NomeLuogo, Via, N, CAP, Eliminato, DataVendita, evv.Username, COUNT(*) AS NumeroBiglietti, (COUNT(*) * Prezzo) AS Totale
                FROM evento evv
                INNER JOIN biglietto_venduto acq ON evv.idEvento = acq.idEvento
                WHERE acq.Username = ?
                AND evv.Eliminato = 1
                GROUP BY DataVendita, acq.idEvento
                ORDER BY DataInizio ASC");
            break;
            case "dataDesc":
                $stmt = $this->db->prepare("SELECT evv.idEvento, NomeEvento, Immagine, DataInizio, OraInizio, DataFine, OraFine, NomeLuogo, Via, N, CAP, Eliminato, DataVendita, evv.Username, COUNT(*) AS NumeroBiglietti, (COUNT(*) * Prezzo) AS Totale
                FROM evento evv
                INNER JOIN biglietto_venduto acq ON evv.idEvento = acq.idEvento
                WHERE acq.Username = ?
                AND evv.Eliminato = 1
                GROUP BY DataVendita, acq.idEvento
                ORDER BY DataInizio DESC");
            break;
            case "nomeAsc":
                $stmt = $this->db->prepare("SELECT evv.idEvento, NomeEvento, Immagine, DataInizio, OraInizio, DataFine, OraFine, NomeLuogo, Via, N, CAP, Eliminato, DataVendita, evv.Username, COUNT(*) AS NumeroBiglietti, (COUNT(*) * Prezzo) AS Totale
                FROM evento evv
                INNER JOIN biglietto_venduto acq ON evv.idEvento = acq.idEvento
                WHERE acq.Username = ?
                AND evv.Eliminato = 1
                GROUP BY DataVendita, acq.idEvento
                ORDER BY NomeEvento ASC");
            break;
            case "nomeDesc":
                $stmt = $this->db->prepare("SELECT evv.idEvento, NomeEvento, Immagine, DataInizio, OraInizio, DataFine, OraFine, NomeLuogo, Via, N, CAP, Eliminato, DataVendita, evv.Username, COUNT(*) AS NumeroBiglietti, (COUNT(*) * Prezzo) AS Totale
                FROM evento evv
                INNER JOIN biglietto_venduto acq ON evv.idEvento = acq.idEvento
                WHERE acq.Username = ?
                AND evv.Eliminato = 1
                GROUP BY DataVendita, acq.idEvento
                ORDER BY NomeEvento DESC");
            break;
            case "acqAsc":
                $stmt = $this->db->prepare("SELECT evv.idEvento, NomeEvento, Immagine, DataInizio, OraInizio, DataFine, OraFine, NomeLuogo, Via, N, CAP, Eliminato, DataVendita, evv.Username, COUNT(*) AS NumeroBiglietti, (COUNT(*) * Prezzo) AS Totale
                FROM evento evv
                INNER JOIN biglietto_venduto acq ON evv.idEvento = acq.idEvento
                WHERE acq.Username = ?
                AND evv.Eliminato = 1
                GROUP BY DataVendita, acq.idEvento
                ORDER BY DataVendita ASC");
            break;
            case "acqDesc":
                $stmt = $this->db->prepare("SELECT evv.idEvento, NomeEvento, Immagine, DataInizio, OraInizio, DataFine, OraFine, NomeLuogo, Via, N, CAP, Eliminato, DataVendita, evv.Username, COUNT(*) AS NumeroBiglietti, (COUNT(*) * Prezzo) AS Totale
                FROM evento evv
                INNER JOIN biglietto_venduto acq ON evv.idEvento = acq.idEvento
                WHERE acq.Username = ?
                AND evv.Eliminato = 1
                GROUP BY DataVendita, acq.idEvento
                ORDER BY DataVendita DESC");
            break;
        }
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    /*----------------------------------------------------*/
    /* ---------------- OSSERVATI ----------------------- */
    /*----------------------------------------------------*/
    
    public function isOsservato($idEvento, $username) {
        $stmt = $this->db->prepare("SELECT * FROM osservazione WHERE idEvento = ? AND Username = ?");
        $stmt->bind_param("is", $idEvento, $username);
        $stmt->execute();
        return empty($stmt->get_result()->fetch_all(MYSQLI_ASSOC)) ? false : true;
    }
    
    public function aggiungiOsservati($idEvento, $username) {
        $stmt = $this->db->prepare("INSERT INTO osservazione(idEvento, Username) VALUES (?,?)");
        $stmt->bind_param("ss", $idEvento, $username);
        $stmt->execute();
    }
    
    public function eliminaDaOsservati($idEvento, $username) {
        $stmt = $this->db->prepare("DELETE FROM osservazione WHERE idEvento = ? AND Username = ?");
        $stmt->bind_param("ss", $idEvento, $username);
        $stmt->execute();
    }
    
    public function getNEventiInteressatiFuturi($n, $username){
        $stmt = $this->db->prepare("SELECT * FROM evento evv
        INNER JOIN osservazione oss ON evv.idEvento = oss.idEvento
        WHERE oss.Username = ?
        AND evv.DataFine >= CURDATE() AND evv.Eliminato = 0
        ORDER BY DataInizio ASC
        LIMIT ?");
        $stmt->bind_param("si", $username, $n);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getEventiInteressatiFuturi($username, $type){
        $stmt = null;
        switch($type){
            case "dataAsc":
                $stmt = $this->db->prepare("SELECT * FROM evento evv
                INNER JOIN osservazione oss ON evv.idEvento = oss.idEvento
                WHERE oss.Username = ?
                AND evv.DataFine >= CURDATE() AND evv.Eliminato = 0
                ORDER BY DataInizio ASC");
            break;
            case "dataDesc":
                $stmt = $this->db->prepare("SELECT * FROM evento evv
                INNER JOIN osservazione oss ON evv.idEvento = oss.idEvento
                WHERE oss.Username = ?
                AND evv.DataFine >= CURDATE() AND evv.Eliminato = 0
                ORDER BY DataInizio DESC");
            break;
            case "nomeAsc":
                $stmt = $this->db->prepare("SELECT * FROM evento evv
                INNER JOIN osservazione oss ON evv.idEvento = oss.idEvento
                WHERE oss.Username = ?
                AND evv.DataFine >= CURDATE() AND evv.Eliminato = 0
                ORDER BY NomeEvento ASC");
            break;
            case "nomeDesc":
                $stmt = $this->db->prepare("SELECT * FROM evento evv
                INNER JOIN osservazione oss ON evv.idEvento = oss.idEvento
                WHERE oss.Username = ?
                AND evv.DataFine >= CURDATE() AND evv.Eliminato = 0
                ORDER BY NomeEvento DESC");
            break;
        }
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getNEventiInteressatiPassati($n, $username){
        $stmt = $this->db->prepare("SELECT * FROM evento evv
        INNER JOIN osservazione oss ON evv.idEvento = oss.idEvento
        WHERE oss.Username = ?
        AND evv.DataFine < CURDATE() AND evv.Eliminato = 0
        ORDER BY DataInizio DESC
        LIMIT ?");
        $stmt->bind_param("si", $username, $n);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getEventiInteressatiPassati($username, $type){
        switch($type){
            case "dataAsc":
                $stmt = $this->db->prepare("SELECT * FROM evento evv
                INNER JOIN osservazione oss ON evv.idEvento = oss.idEvento
                WHERE oss.Username = ?
                AND evv.DataFine < CURDATE() AND evv.Eliminato = 0
                ORDER BY DataInizio ASC");
            break;
            case "dataDesc":
                $stmt = $this->db->prepare("SELECT * FROM evento evv
                INNER JOIN osservazione oss ON evv.idEvento = oss.idEvento
                WHERE oss.Username = ?
                AND evv.DataFine < CURDATE() AND evv.Eliminato = 0
                ORDER BY DataInizio DESC");
            break;
            case "nomeAsc":
                $stmt = $this->db->prepare("SELECT * FROM evento evv
                INNER JOIN osservazione oss ON evv.idEvento = oss.idEvento
                WHERE oss.Username = ?
                AND evv.DataFine < CURDATE() AND evv.Eliminato = 0
                ORDER BY NomeEvento ASC");
            break;
            case "nomeDesc":
                $stmt = $this->db->prepare("SELECT * FROM evento evv
                INNER JOIN osservazione oss ON evv.idEvento = oss.idEvento
                WHERE oss.Username = ?
                AND evv.DataFine < CURDATE() AND evv.Eliminato = 0
                ORDER BY NomeEvento DESC");
            break;
        }
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    
    public function getNEventiInteressatiEliminati($n, $username){
        $stmt = $this->db->prepare("SELECT * FROM evento evv
        INNER JOIN osservazione oss ON evv.idEvento = oss.idEvento
        WHERE oss.Username = ?
        AND evv.Eliminato = 1
        ORDER BY DataInizio DESC
        LIMIT ?");
        $stmt->bind_param("si", $username, $n);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getEventiInteressatiEliminati($username, $type){
        $stmt = null;
        switch($type){
            case "dataAsc":
                $stmt = $this->db->prepare("SELECT * FROM evento evv
                INNER JOIN osservazione oss ON evv.idEvento = oss.idEvento
                WHERE oss.Username = ?
                AND evv.Eliminato = 1
                ORDER BY DataInizio ASC");
            break;
            case "dataDesc":
                $stmt = $this->db->prepare("SELECT * FROM evento evv
                INNER JOIN osservazione oss ON evv.idEvento = oss.idEvento
                WHERE oss.Username = ?
                AND evv.Eliminato = 1
                ORDER BY DataInizio DESC");
            break;
            case "nomeAsc":
                $stmt = $this->db->prepare("SELECT * FROM evento evv
                INNER JOIN osservazione oss ON evv.idEvento = oss.idEvento
                WHERE oss.Username = ?
                AND evv.Eliminato = 1
                ORDER BY NomeEvento ASC");
            break;
            case "nomeDesc":
                $stmt = $this->db->prepare("SELECT * FROM evento evv
                INNER JOIN osservazione oss ON evv.idEvento = oss.idEvento
                WHERE oss.Username = ?
                AND evv.Eliminato = 1
                ORDER BY NomeEvento DESC");
            break;
        }
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    
    
    
    
    /*----------------------------------------------------*/
    /* ---------------- ADMIN QUERY --------------------- */
    /*----------------------------------------------------*/
    
    
    public function getRichiesteCreatori($ordinamento){
        switch($ordinamento){
            case "dataAsc":
                $stmt = $this->db->prepare("SELECT * FROM creatore c 
                INNER JOIN persona p
                ON c.Username = p.Username
                WHERE c.accettato = 0
                ORDER BY p.DataRegistrazione DESC");
            break;
            case "dataDesc":
                $stmt = $this->db->prepare("SELECT * FROM creatore c 
                INNER JOIN persona p
                ON c.Username = p.Username
                WHERE c.accettato = 0
                ORDER BY p.DataRegistrazione ASC");
            break;
            case "nomeAsc":
                $stmt = $this->db->prepare("SELECT * FROM creatore c 
                INNER JOIN persona p
                ON c.Username = p.Username
                WHERE c.accettato = 0
                ORDER BY p.Username ASC");
            break;
            case "nomeDesc":
                $stmt = $this->db->prepare("SELECT * FROM creatore c 
                INNER JOIN persona p
                ON c.Username = p.Username
                WHERE c.accettato = 0
                ORDER BY p.Username DESC");
            break;
        }
        $stmt->execute();
        return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getRegistrati($tipo, $visualizza, $ordinamento) {
        $base = "SELECT Nome, Cognome, Username, Mail, DataNascita, DataRegistrazione, Ban FROM"; 
        $tipoTutti = "(SELECT p.Nome, p.Cognome, p.Username, p.Mail, p.DataNascita, p.DataRegistrazione, c.Ban FROM persona p 
        INNER JOIN creatore c ON p.Username = c.Username WHERE c.Accettato = 1 
        UNION ALL SELECT pp.Nome, pp.Cognome, pp.Username, pp.Mail, pp.DataNascita, pp.DataRegistrazione, u.Ban FROM persona pp 
        INNER JOIN utente u ON pp.Username = u.Username) ";
        $tipoCreatore = "(SELECT p.Nome, p.Cognome, p.Username, p.Mail, p.DataNascita, p.DataRegistrazione, c.Ban FROM persona p INNER JOIN creatore c ON p.Username = c.Username WHERE c.Accettato = 1) ";
        $tipoUtente = "(SELECT p.Nome, p.Cognome, p.Username, p.Mail, p.DataNascita, p.DataRegistrazione, u.Ban FROM persona p INNER JOIN utente u ON p.Username = u.Username) ";
        $middle = "reg ";
        $visualizzaTutti = "WHERE ";
        $visualizzaBan = "WHERE reg.Ban = 1 AND";
        $visualizzaUnBan = "WHERE reg.Ban = 0 AND";
        $end = " reg.Username NOT IN (SELECT a.Username FROM amministratore a) ORDER BY ";
        $ordinamentoUsAsc = "reg.Username ASC";
        $ordinamentoUsDesc = "reg.Username DESC";
        
        /* $query = $base;
        
        if($tipo == "tutti"){
            $query = $query.$tipoTutti;
        } elseif($tipo == "creatori"){
            $query = $query.$tipoCreatore;
        } elseif($tipo == "utenti"){
            $query = $query.$tipoUtente;
        }
        
        $query = $middle;
        
        if($visualizza == "tutti"){
            $query = $query.$visualizzaTutti;
        } elseif($visualizza == "ban"){
            $query = $query.$visualizzaBan;
        } elseif($visualizza == "unban"){
            $query = $query.$visualizzaUnBan;
        }
        
        $query = $end;
        
        if($ordinamento == "usrAsc"){
            $query = $query.$ordinamentoUsAsc;
        } elseif($ordinamento == "usrDesc"){
            $query = $query.$ordinamentoUsDesc;
        }*/
        
        
        if($tipo == "tutti"){
            if($visualizza == "tutti"){
                if($ordinamento == "usrAsc"){
                    $query = "".$base.$tipoTutti.$middle.$visualizzaTutti.$end.$ordinamentoUsAsc;
                } elseif($ordinamento == "usrDesc"){
                    $query = "".$base.$tipoTutti.$middle.$visualizzaTutti.$end.$ordinamentoUsDesc;
                }
            } elseif($visualizza == "bannati"){
                if($ordinamento == "usrAsc"){
                    $query = "".$base.$tipoTutti.$middle.$visualizzaBan.$end.$ordinamentoUsAsc;
                } elseif($ordinamento == "usrDesc"){
                    $query = "".$base.$tipoTutti.$middle.$visualizzaBan.$end.$ordinamentoUsDesc;
                }
            } elseif($visualizza == "unbannati"){
                if($ordinamento == "usrAsc"){
                    $query = "".$base.$tipoTutti.$middle.$visualizzaUnBan.$end.$ordinamentoUsAsc;
                } elseif($ordinamento == "usrDesc"){
                    $query = "".$base.$tipoTutti.$middle.$visualizzaUnBan.$end.$ordinamentoUsDesc;
                }
            }
        } elseif($tipo == "creatori"){
            if($visualizza == "tutti"){
                if($ordinamento == "usrAsc"){
                    $query = "".$base.$tipoCreatore.$middle.$visualizzaTutti.$end.$ordinamentoUsAsc;
                } elseif($ordinamento == "usrDesc"){
                    $query = "".$base.$tipoCreatore.$middle.$visualizzaTutti.$end.$ordinamentoUsDesc;
                }
            } elseif($visualizza == "bannati"){
                if($ordinamento == "usrAsc"){
                    $query = "".$base.$tipoCreatore.$middle.$visualizzaBan.$end.$ordinamentoUsAsc;
                } elseif($ordinamento == "usrDesc"){
                    $query = "".$base.$tipoCreatore.$middle.$visualizzaBan.$end.$ordinamentoUsDesc;
                }
            } elseif($visualizza == "unbannati"){
                if($ordinamento == "usrAsc"){
                    $query = "".$base.$tipoCreatore.$middle.$visualizzaUnBan.$end.$ordinamentoUsAsc;
                } elseif($ordinamento == "usrDesc"){
                    $query = "".$base.$tipoCreatore.$middle.$visualizzaUnBan.$end.$ordinamentoUsDesc;
                }
            }
        } elseif($tipo == "utenti"){
            if($visualizza == "tutti"){
                if($ordinamento == "usrAsc"){
                    $query = "".$base.$tipoUtente.$middle.$visualizzaTutti.$end.$ordinamentoUsAsc;
                } elseif($ordinamento == "usrDesc"){
                    $query = "".$base.$tipoUtente.$middle.$visualizzaTutti.$end.$ordinamentoUsDesc;
                }
            } elseif($visualizza == "bannati"){
                if($ordinamento == "usrAsc"){
                    $query = "".$base.$tipoUtente.$middle.$visualizzaBan.$end.$ordinamentoUsAsc;
                } elseif($ordinamento == "usrDesc"){
                    $query = "".$base.$tipoUtente.$middle.$visualizzaBan.$end.$ordinamentoUsDesc;
                }
            } elseif($visualizza == "unbannati"){
                if($ordinamento == "usrAsc"){
                    $query = "".$base.$tipoUtente.$middle.$visualizzaUnBan.$end.$ordinamentoUsAsc;
                } elseif($ordinamento == "usrDesc"){
                    $query = "".$base.$tipoUtente.$middle.$visualizzaUnBan.$end.$ordinamentoUsDesc;
                }
            }
        }
        
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getUtenteFromPersona($persona) {
        $stmt = $this->db->prepare("SELECT * FROM utente u
        INNER JOIN persona p ON u.Username = p.Username
        WHERE u.Username = ?");
        $stmt->bind_param("s", $persona);
        $stmt->execute();
        return $stmt->get_result()->fetch_array(MYSQLI_ASSOC);
    }
    
    public function getCreatoreFromPersona($persona) {
        $stmt = $this->db->prepare("SELECT * FROM creatore c
        INNER JOIN persona p ON c.Username = p.Username
        WHERE c.Username = ?");
        $stmt->bind_param("s", $persona);
        $stmt->execute();
        return $stmt->get_result()->fetch_array(MYSQLI_ASSOC);
    }
    
    public function accettazioneRichiesta($username) {
        $stmt = $this->db->prepare("UPDATE creatore SET accettato = 1 WHERE Username = ?");
        $stmt->bind_param("s", $username);
        $stmt->execute();
    }
    
    public function rifiutoRichiesta($username) {
        $stmt = $this->db->prepare("DELETE FROM creatore WHERE Username = ?");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $stmt = $this->db->prepare("DELETE FROM persona WHERE Username = ?");
        $stmt->bind_param("s", $username);
        $stmt->execute();
    }
    
    public function isBannato($username) {
        $stmt = $this->db->prepare("SELECT p.Username, u.Ban FROM persona p INNER JOIN utente u ON p.Username = u.Username WHERE p.Username = ? AND u.Ban = 1 
        UNION ALL SELECT p1.Username, c.Ban FROM persona p1 INNER JOIN creatore c ON p1.Username = c.Username WHERE p1.Username = ? AND c.Ban = 1");
        $stmt->bind_param("ss", $username, $username);
        $stmt->execute();
        return (empty($stmt->get_result()->fetch_all(MYSQLI_ASSOC)) ? false : true);
    }
    
    public function banRegistrato($username) {
        $stmt = null;
        if($this->contains('creatore', 'Username', $username, 's')) $stmt = $this->db->prepare("UPDATE creatore SET Ban = 1 WHERE Username = ?");
        else if ($this->contains('utente', 'Username', $username, 's')) $stmt = $this->db->prepare("UPDATE utente SET Ban = 1 WHERE Username = ?");
        else return false;
        $stmt->bind_param("s", $username);
        $stmt->execute();
        return true;
    }
    
    public function unbanRegistrato($username) {
        $stmt = $this->db->prepare("UPDATE creatore SET Ban = 0 WHERE Username = ?");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $stmt = $this->db->prepare("UPDATE utente SET Ban = 0 WHERE Username = ?");
        $stmt->bind_param("s", $username);
        $stmt->execute();
    }
    
    public function getTuttiGliEventi($tipo, $ordinamento, $type, $username) {
        $base = "SELECT * FROM evento "; 
        $tipoTutti = "";
        $tipoFuturi = "WHERE DataFine > CURDATE() AND Eliminato = 0 ";
        $tipoInCorso = "WHERE DataInizio >= CURDATE() AND DataFine <= CURDATE() AND Eliminato = 0 ";
        $tipoPassati = "WHERE DataInizio < CURDATE() AND Eliminato = 0 ";
        $tipoCancellati = "WHERE Eliminato = 1 ";
        $specificaTutti = "WHERE Username = ? ";
        $specifica = "AND Username = ? ";
        
        $ordinamentoDataAsc = "ORDER BY DataInizio ASC";
        $ordinamentoDataDesc = "ORDER BY DataInizio DESC";
        $ordinamentoNomeAsc = "ORDER BY NomeEvento ASC";
        $ordinamentoNomeDesc = "ORDER BY NomeEvento DESC";
        
        if($type == 0){
            if($tipo == "tutti"){
                if($ordinamento == "dataAsc"){
                    $query = $base.$tipoTutti.$ordinamentoDataAsc;
                } elseif($ordinamento == "dataDesc"){
                    $query = $base.$tipoTutti.$ordinamentoDataDesc;
                } elseif($ordinamento == "nomeAsc"){
                    $query = $base.$tipoTutti.$ordinamentoNomeAsc;
                } elseif($ordinamento == "nomeDesc"){
                    $query = $base.$tipoTutti.$ordinamentoNomeDesc;
                }
            } elseif($tipo == "futuri"){
                if($ordinamento == "dataAsc"){
                    $query = $base.$tipoFuturi.$ordinamentoDataAsc;
                } elseif($ordinamento == "dataDesc"){
                    $query = $base.$tipoFuturi.$ordinamentoDataDesc;
                } elseif($ordinamento == "nomeAsc"){
                    $query = $base.$tipoFuturi.$ordinamentoNomeAsc;
                } elseif($ordinamento == "nomeDesc"){
                    $query = $base.$tipoFuturi.$ordinamentoNomeDesc;
                }
            } elseif($tipo == "corso"){
                if($ordinamento == "dataAsc"){
                    $query = $base.$tipoInCorso.$ordinamentoDataAsc;
                } elseif($ordinamento == "dataDesc"){
                    $query = $base.$tipoInCorso.$ordinamentoDataDesc;
                } elseif($ordinamento == "nomeAsc"){
                    $query = $base.$tipoInCorso.$ordinamentoNomeAsc;
                } elseif($ordinamento == "nomeDesc"){
                    $query = $base.$tipoInCorso.$ordinamentoNomeDesc;
                }
            } elseif($tipo == "passati"){
                if($ordinamento == "dataAsc"){
                    $query = $base.$tipoPassati.$ordinamentoDataAsc;
                } elseif($ordinamento == "dataDesc"){
                    $query = $base.$tipoPassati.$ordinamentoDataDesc;
                } elseif($ordinamento == "nomeAsc"){
                    $query = $base.$tipoPassati.$ordinamentoNomeAsc;
                } elseif($ordinamento == "nomeDesc"){
                    $query = $base.$tipoPassati.$ordinamentoNomeDesc;
                }
            } elseif($tipo == "eliminati"){
                if($ordinamento == "dataAsc"){
                    $query = $base.$tipoCancellati.$ordinamentoDataAsc;
                } elseif($ordinamento == "dataDesc"){
                    $query = $base.$tipoCancellati.$ordinamentoDataDesc;
                } elseif($ordinamento == "nomeAsc"){
                    $query = $base.$tipoCancellati.$ordinamentoNomeAsc;
                } elseif($ordinamento == "nomeDesc"){
                    $query = $base.$tipoCancellati.$ordinamentoNomeDesc; 
                }
            }
            $stmt = $this->db->prepare($query);    
        } else {
            if($tipo == "tutti"){
                if($ordinamento == "dataAsc"){
                    $query = $base.$tipoTutti.$specificaTutti.$ordinamentoDataAsc;
                } elseif($ordinamento == "dataDesc"){
                    $query = $base.$tipoTutti.$specificaTutti.$ordinamentoDataDesc;
                } elseif($ordinamento == "nomeAsc"){
                    $query = $base.$tipoTutti.$specificaTutti.$ordinamentoNomeAsc;
                } elseif($ordinamento == "nomeDesc"){
                    $query = $base.$tipoTutti.$specificaTutti.$ordinamentoNomeDesc;
                }
            } elseif($tipo == "futuri"){
                if($ordinamento == "dataAsc"){
                    $query = $base.$tipoFuturi.$specifica.$ordinamentoDataAsc;
                } elseif($ordinamento == "dataDesc"){
                    $query = $base.$tipoFuturi.$specifica.$ordinamentoDataDesc;
                } elseif($ordinamento == "nomeAsc"){
                    $query = $base.$tipoFuturi.$specifica.$ordinamentoNomeAsc;
                } elseif($ordinamento == "nomeDesc"){
                    $query = $base.$tipoFuturi.$specifica.$ordinamentoNomeDesc;
                }
            } elseif($tipo == "corso"){
                if($ordinamento == "dataAsc"){
                    $query = $base.$tipoInCorso.$specifica.$ordinamentoDataAsc;
                } elseif($ordinamento == "dataDesc"){
                    $query = $base.$tipoInCorso.$specifica.$ordinamentoDataDesc;
                } elseif($ordinamento == "nomeAsc"){
                    $query = $base.$tipoInCorso.$specifica.$ordinamentoNomeAsc;
                } elseif($ordinamento == "nomeDesc"){
                    $query = $base.$tipoInCorso.$specifica.$ordinamentoNomeDesc;
                }
            } elseif($tipo == "passati"){
                if($ordinamento == "dataAsc"){
                    $query = $base.$tipoPassati.$specifica.$ordinamentoDataAsc;
                } elseif($ordinamento == "dataDesc"){
                    $query = $base.$tipoPassati.$specifica.$ordinamentoDataDesc;
                } elseif($ordinamento == "nomeAsc"){
                    $query = $base.$tipoPassati.$specifica.$ordinamentoNomeAsc;
                } elseif($ordinamento == "nomeDesc"){
                    $query = $base.$tipoPassati.$specifica.$ordinamentoNomeDesc;
                }
            } elseif($tipo == "eliminati"){
                if($ordinamento == "dataAsc"){
                    $query = $base.$tipoCancellati.$specifica.$ordinamentoDataAsc;
                } elseif($ordinamento == "dataDesc"){
                    $query = $base.$tipoCancellati.$specifica.$ordinamentoDataDesc;
                } elseif($ordinamento == "nomeAsc"){
                    $query = $base.$tipoCancellati.$specifica.$ordinamentoNomeAsc;
                } elseif($ordinamento == "nomeDesc"){
                    $query = $base.$tipoCancellati.$specifica.$ordinamentoNomeDesc; 
                }
            }    
            $stmt = $this->db->prepare($query);
            $stmt->bind_param("s", $username);
        }
        
        $stmt->execute();
        return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    }
    
    
    
    
    
    
    /*----------------------------------------------------*/
    /* -------------------------------------------------- */
    /*----------------------------------------------------*/
    
    
    
    
    
    
    public function getEventiCarrello($username){
        $query = "SELECT *
        FROM  evento ev INNER JOIN carrello car ON ev.idEvento = car.idEvento
        WHERE car.Username = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getNumeroBigliettiCarrello($username, $idevento){
        $query = "SELECT *
        FROM carrello
        WHERE Username = ? AND idEvento = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("si", $username, $idevento);
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows <= 0) return 0;
        $row = $result->fetch_array(MYSQLI_ASSOC);
        return $row["Quantita"];
    }

    public function getDisponibili($idevento){
        $result = $this->getEventoById($idevento);
        $disponibili = $result["BigliettiDisponibili"];
        return $disponibili;
    }
    
    public function setQuantitaEventoCarrello($username, $idevento, $quantita){
        $query = "SELECT *
        FROM carrello
        WHERE Username = ? AND idEvento = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("si", $username, $idevento);
        $stmt->execute();
        $result = $stmt->get_result();
        $quantitaAttuale = 0;
        $disponibili = $this->getDisponibili($idevento);
        if($result->num_rows <= 0){ //aggiungi al carrello
            if($quantita <= 0) return false;
            if($quantita > $disponibili) return false;
            $stmt = $this->db->prepare("INSERT INTO carrello (idEvento, Username, Quantita) VALUES (?, ?, ?)");
            $stmt->bind_param("isi", $idevento, $username, $quantita);
            $stmt->execute();
        }else {
            $row = $result->fetch_array(MYSQLI_ASSOC);
            $quantitaAttuale = $row["Quantita"];
            $disponibili = $disponibili + $quantitaAttuale;
            if($quantita > $disponibili) return false;
            if($quantita <= 0){ //elimina dal carrello
                $query = "DELETE FROM carrello WHERE Username = ? AND idEvento = ?";
                $stmt = $this->db->prepare($query);
                $stmt->bind_param("si", $username, $idevento);
                $stmt->execute();
            } else { //aggiorna valore attuale
                $query = "UPDATE carrello SET Quantita = ? WHERE Username = ? AND idEvento = ?";
                $stmt = $this->db->prepare($query);
                $stmt->bind_param("isi", $quantita, $username, $idevento);
                $stmt->execute();
            }
        }
        $disponibiliNuovo = $disponibili - $quantita;
        $query = "UPDATE evento SET BigliettiDisponibili = ? WHERE idEvento = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("ii", $disponibiliNuovo, $idevento);
        $stmt->execute();
        return true;
    }
    
    public function eliminaEventoCarrello($username, $idevento){
        return $this->setQuantitaEventoCarrello($username, $idevento, 0);
    }
    
    public function aggiungiEventoCarrello($username, $idevento){
        return $this->setQuantitaEventoCarrello($username, $idevento, 1);
    }
    
    public function getEventiAttiviByKeyword($keyword){
        $stmt = $this->db->prepare("SELECT * FROM evento ev WHERE Eliminato = 0 AND DataInizio >= CURDATE() AND ( INSTR(NomeEvento, ?) > 0 OR INSTR(Username, ?) > 0 OR INSTR(NomeLuogo, ?) > 0) ORDER BY DataInizio ASC");
        $stmt->bind_param("sss", $keyword, $keyword, $keyword);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    
    public function getEventiCreatoreByKeyword($username, $keyword){
        $stmt = $this->db->prepare("SELECT * FROM evento ev WHERE Username = ? AND ( INSTR(NomeEvento, ?) > 0 OR INSTR(Username, ?) > 0 OR INSTR(NomeLuogo, ?) > 0) ORDER BY DataInizio ASC");
        $stmt->bind_param("ssss", $username, $keyword, $keyword, $keyword);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function spostaDaCarrelloInAcquistati($username){
        $carrelloUtente = $this->getEventiCarrello($username);
        foreach ($carrelloUtente as $key => $value) {
            $stmt = $this->db->prepare("SELECT MAX(NumeroBiglietto) FROM biglietto_venduto WHERE idEvento = ?");
            $stmt->bind_param("i", $value["idEvento"]);
            $stmt->execute();
            $result = $stmt->get_result();
            $nextId =  $result->fetch_array(MYSQLI_NUM)[0] + 1;
            //insert
            foreach (range(0, $value["Quantita"] - 1) as $number) {
                $id = $nextId + $number;
                $stmt = $this->db->prepare("INSERT INTO biglietto_venduto (idEvento, NumeroBiglietto, PrezzoVendita, Username) VALUES (?, ?, ?, ?)");
                $stmt->bind_param("iiis", $value["idEvento"], $id, $value["Prezzo"], $username);
                $stmt->execute();
            }
            //remove
            $stmt = $this->db->prepare("DELETE FROM carrello WHERE Username = ? AND idEvento = ?");
            $stmt->bind_param("si", $username, $value["idEvento"]);
            $stmt->execute();
        }
    }
    
    public function aggiungiEvento($usernameCreatore, $capienza, $nomeEvento, $immagine, $dataInizio, $oraInizio, $dataFine, $oraFine, $nomeLuogo, $prezzo, $descrizione, $via, $n, $cap, $provincia, $stato, $citta){
        //check se indirizzo esiste, se no creare
        $stmt = $this->db->prepare("SELECT * FROM indirizzo WHERE Via = ? AND N = ? AND CAP = ?");
        $stmt->bind_param("sii", $via, $n, $cap);
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows <= 0){
            $stmt = $this->db->prepare("INSERT INTO indirizzo (Via, N, Provincia, CAP, Stato, Citta) VALUES (?, ?, ?, ?, ?, ?)");
            $stmt->bind_param("sisiss", $via, $n, $provincia, $cap, $stato, $citta);
            $stmt->execute();
        }
        //inserisci evento in tabella evento
        $id = $this->getNextId("evento", "idEvento");
        $stmt = $this->db->prepare("INSERT INTO evento (idEvento, Username, Capienza, BigliettiDisponibili, NomeEvento, Immagine, DataInizio, OraInizio, DataFine, OraFine, NomeLuogo, Prezzo, Descrizione, Via, N, CAP) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param("isiisssssssissis", $id, $usernameCreatore, $capienza, $capienza, $nomeEvento, $immagine, $dataInizio, $oraInizio, $dataFine, $oraFine, $nomeLuogo, $prezzo, $descrizione, $via, $n, $cap);
        $stmt->execute();
    }

    public function modificaEvento($id, $usernameCreatore, $capienza, $nomeEvento, $immagine, $dataInizio, $oraInizio, $dataFine, $oraFine, $nomeLuogo, $prezzo, $descrizione, $via, $n, $cap, $provincia, $stato, $citta){
        //check se indirizzo esiste, se no creare
        $stmt = $this->db->prepare("SELECT * FROM indirizzo WHERE Via = ? AND N = ? AND CAP = ?");
        $stmt->bind_param("sii", $via, $n, $cap);
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows <= 0){
            $stmt = $this->db->prepare("INSERT INTO indirizzo (Via, N, Provincia, CAP, Stato, Citta) VALUES (?, ?, ?, ?, ?, ?)");
            $stmt->bind_param("sisiss", $via, $n, $provincia, $cap, $stato, $citta);
            $stmt->execute();
        }
        //update evento in tabella evento
        $stmt = $this->db->prepare("UPDATE evento SET Username = ?, Capienza = ?, BigliettiDisponibili = ?, NomeEvento = ?, Immagine = ?, DataInizio = ?, OraInizio = ?, DataFine = ?, OraFine = ?, NomeLuogo = ?, Prezzo = ?, Descrizione = ?, Via = ?, N = ?, CAP = ? WHERE idEvento = ?");
        $stmt->bind_param("siisssssssissisi", $usernameCreatore, $capienza, $capienza, $nomeEvento, $immagine, $dataInizio, $oraInizio, $dataFine, $oraFine, $nomeLuogo, $prezzo, $descrizione, $via, $n, $cap, $id);
        $stmt->execute();
    }
    
    public function inviaMessaggio($mittente, $destinatari, $oggetto, $testo){
        //inserire il messaggio in tabella messaggio con nome mittente
        $id = $this->getNextId("messaggio", "idMessaggio");
        $stmt = $this->db->prepare("INSERT INTO messaggio (idMessaggio, Oggetto, Testo, UsernameMittente) VALUES (?, ?, ?, ?)");
        $stmt->bind_param("isss", $id, $oggetto, $testo, $mittente);
        $stmt->execute();
        
        //per ogni destinatario inserire un campo in tabella destinatario_msg
        foreach ($destinatari as $key => $value) {
            $stmt = $this->db->prepare("INSERT INTO destinatario_msg (UsernameDestinatario, idMessaggio) VALUES (?, ?)");
            $stmt->bind_param("si", $value, $id);
            $stmt->execute();
        }
    }
    
    public function inviaMessaggioAPartecipanti($idevento, $mittente, $oggetto , $testo){
        $stmt = $this->db->prepare("SELECT Username FROM biglietto_venduto WHERE idEvento = ?");
        $stmt->bind_param("i", $idevento);
        $stmt->execute();
        $partecipanti = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        //inserisco il messaggio
        $idMsg = $this->getNextId("messaggio", "idMessaggio");
        $stmt = $this->db->prepare("INSERT INTO messaggio (IdMessaggio, Oggetto, Testo, UsernameMittente) VALUES (?, ?, ?, ?)");
        $stmt->bind_param("isss", $idMsg, $oggetto, $testo, $mittente);
        $stmt->execute();
        
        //lo assegno ai destinatari
        foreach ($partecipanti as $key => $value) {
            $stmt = $this->db->prepare("INSERT INTO destinatario_msg (IdMessaggio, UsernameDestinatario) VALUES (?, ?)");
            $stmt->bind_param("is", $idMsg, $value["Username"]);
            $stmt->execute();
        }
    }
    
    public function inviaMessaggioATutti($mittente, $oggetto , $testo){
        $stmt = $this->db->prepare("SELECT Username FROM persona WHERE Username != ?");
        $stmt->bind_param("s", $mittente);
        $stmt->execute();
        $destinatari = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        //inserisco il messaggio
        $idMsg = $this->getNextId("messaggio", "idMessaggio");
        $stmt = $this->db->prepare("INSERT INTO messaggio (IdMessaggio, Oggetto, Testo, UsernameMittente) VALUES (?, ?, ?, ?)");
        $stmt->bind_param("isss", $idMsg, $oggetto, $testo, $mittente);
        $stmt->execute();
        
        //lo assegno ai destinatari
        foreach ($destinatari as $key => $value) {
            $stmt = $this->db->prepare("INSERT INTO destinatario_msg (IdMessaggio, UsernameDestinatario) VALUES (?, ?)");
            $stmt->bind_param("is", $idMsg, $value["Username"]);
            $stmt->execute();
        }
    }
    
    public function inviaMessaggioAUtenti($mittente, $oggetto , $testo){
        $stmt = $this->db->prepare("SELECT * FROM utente WHERE Username != ?");
        $stmt->bind_param("s", $mittente);
        $stmt->execute();
        $res = $stmt->get_result();
        $destinatari = $res->fetch_all(MYSQLI_ASSOC);
        //inserisco il messaggio
        $idMsg = $this->getNextId("messaggio", "idMessaggio");
        $stmt = $this->db->prepare("INSERT INTO messaggio (IdMessaggio, Oggetto, Testo, UsernameMittente) VALUES (?, ?, ?, ?)");
        $stmt->bind_param("isss", $idMsg, $oggetto, $testo, $mittente);
        $stmt->execute();
        
        //lo assegno ai destinatari
        foreach ($destinatari as $key => $value) {
            $stmt = $this->db->prepare("INSERT INTO destinatario_msg (IdMessaggio, UsernameDestinatario) VALUES (?, ?)");
            $stmt->bind_param("is", $idMsg, $value["Username"]);
            $stmt->execute();
        }
    }
    
    public function inviaMessaggioACreatori($mittente, $oggetto , $testo){
        $stmt = $this->db->prepare("SELECT Username FROM creatore");
        $stmt->execute();
        $destinatari = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        //inserisco il messaggio
        $idMsg = $this->getNextId("messaggio", "idMessaggio");
        $stmt = $this->db->prepare("INSERT INTO messaggio (IdMessaggio, Oggetto, Testo, UsernameMittente) VALUES (?, ?, ?, ?)");
        $stmt->bind_param("isss", $idMsg, $oggetto, $testo, $mittente);
        $stmt->execute();
        
        //lo assegno ai destinatari
        foreach ($destinatari as $key => $value) {
            $stmt = $this->db->prepare("INSERT INTO destinatario_msg (IdMessaggio, UsernameDestinatario) VALUES (?, ?)");
            $stmt->bind_param("is", $idMsg, $value["Username"]);
            $stmt->execute();
        }
    }
    
    public function getMessaggiRicevuti($username){
        $stmt = $this->db->prepare("SELECT * FROM destinatario_msg dest INNER JOIN messaggio msg ON dest.IdMEssaggio = msg.IdMessaggio WHERE dest.UsernameDestinatario = ? AND dest.Eliminato = 0 ORDER BY dest.Visto ASC, msg.DataInvio DESC");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getNMessaggiRicevutiNonLetti($username){
        $stmt = $this->db->prepare("SELECT * FROM destinatario_msg dest INNER JOIN messaggio msg ON dest.IdMEssaggio = msg.IdMessaggio WHERE dest.UsernameDestinatario = ? AND dest.Eliminato = 0 AND dest.Visto = 0");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->num_rows;
    }
    
    public function getNPromemoriaNonLetti($username){
        $table = null;
        //vedere se username è creatore o utente
        if($this->contains('creatore', 'Username', $username, 's')) $table = 'promemoria_creatore';
        else $table = 'promemoria_utente';
        $stmt = $this->db->prepare("SELECT * FROM $table dest INNER JOIN promemoria msg ON dest.IdPromemoria = msg.IdPromemoria WHERE dest.Username = ? AND dest.Eliminato = 0 AND dest.Visto = 0");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->num_rows;
    }
    
    public function getMessaggiInviati($username){
        $stmt = $this->db->prepare("SELECT * FROM messaggio WHERE UsernameMittente = ? AND Eliminato = 0 ORDER BY DataInvio DESC");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getDestinatari($idMessaggio){
        $stmt = $this->db->prepare("SELECT UsernameDestinatario FROM destinatario_msg WHERE IdMessaggio = ?");
        $stmt->bind_param("i", $idMessaggio);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_NUM);
    }
    
    public function getMessaggio($idMessaggio){
        $stmt = $this->db->prepare("SELECT * FROM messaggio WHERE IdMessaggio = ? AND Eliminato = 0");
        $stmt->bind_param("i", $idMessaggio);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_array(MYSQLI_ASSOC);
    }
    
    public function getPromemoria($username){
        $table = null;
        //vedere se username è creatore o utente
        if($this->contains('creatore', 'Username', $username, 's')) $table = 'promemoria_creatore';
        else $table = 'promemoria_utente';
        $stmt = $this->db->prepare("SELECT * FROM $table dest INNER JOIN promemoria msg ON dest.IdPromemoria = msg.IdPromemoria WHERE dest.Username = ? AND dest.Eliminato = 0 ORDER BY dest.Visto ASC, msg.DataInvio DESC");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function setVisto($username, $idMessaggio){
        $stmt = $this->db->prepare("UPDATE destinatario_msg SET Visto = 1 WHERE UsernameDestinatario = ? AND IdMessaggio = ?");
        $stmt->bind_param("si", $username, $idMessaggio);
        $stmt->execute();
    }
    
    public function setNonVisto($username, $idMessaggio){
        $query = "UPDATE destinatario_msg SET Visto = 0 WHERE UsernameDestinatario = ? AND IdMessaggio = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("si", $username, $idMessaggio);
        $stmt->execute();
    }
    
    public function eliminaMessaggioDest($username, $idMessaggio){
        $query = "UPDATE destinatario_msg SET Eliminato = 1 WHERE UsernameDestinatario = ? AND IdMessaggio = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("si", $username, $idMessaggio);
        $stmt->execute();
    }
    
    public function eliminaMessaggioMitt($username, $idMessaggio){
        $query = "UPDATE messaggio SET Eliminato = 1 WHERE UsernameMittente = ? AND IdMessaggio = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("si", $username, $idMessaggio);
        $stmt->execute();
    }
    
    public function eliminaPromemoria($username, $msgId){
        $table = null;
        //vedere se username è creatore o utente
        if($this->contains('creatore', 'Username', $username, 's')) $table = 'promemoria_creatore';
        else $table = 'promemoria_utente';
        $query = "UPDATE $table SET Eliminato = 1 WHERE Username = ? AND IdPromemoria = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("si", $username, $msgId);
        $stmt->execute();
    }
    
    public function setVistoProm($username, $msgId){
        $table = null;
        //vedere se username è creatore o utente
        if($this->contains('creatore', 'Username', $username, 's')) $table = 'promemoria_creatore';
        else $table = 'promemoria_utente';
        $query = "UPDATE $table SET Visto = 1 WHERE Username = ? AND IdPromemoria = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("si", $username, $msgId);
        $stmt->execute();
    }
    
    public function setNonVistoProm($username, $msgId){
        $table = null;
        //vedere se username è creatore o utente
        if($this->contains('creatore', 'Username', $username, 's')) $table = 'promemoria_creatore';
        else $table = 'promemoria_utente';
        $query = "UPDATE $table SET Visto = 0 WHERE Username = ? AND IdPromemoria = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("si", $username, $msgId);
        $stmt->execute();
    }
    
    public function generaPromemoriaTempo($idEvento){
        $evento = $this->getEventoById($idEvento);
        $testo = null;
        $event_date = new DateTime($evento["DataInizio"]);
        $now = new DateTime(date("Y-m-d"));
        $tempo = date_diff($now, $event_date)->format('%a');
        $nomeEvento = $evento["NomeEvento"];
        if($tempo > 7) return -1;
        if($tempo > 1) $testo = "L'evento $nomeEvento inizierà prossimamente";
        else $testo = "L'evento $nomeEvento inizia tra meno di 24 ore";
        //vedere se promemoria temporale esiste già: stesso idEvento, testo Hai un evento tra una settimana, giorno, oggi
        $stmt = $this->db->prepare("SELECT * FROM promemoria WHERE IdEvento = ? AND Testo = ?");
        $stmt->bind_param("is", $idEvento, $testo);
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows > 0){
            $res = $result->fetch_array(MYSQLI_ASSOC);
            return $res["IdPromemoria"];
        }
        //crea il rpomemoria
        $id=$this->getNextId('promemoria', 'IdPromemoria');
        $stmt = $this->db->prepare("INSERT INTO promemoria (IdPromemoria, idEvento, Testo) VALUES (?, ?, ?)");
        $stmt->bind_param("iis", $id, $idEvento, $testo);
        $stmt->execute();
        return $id;
    }
    
    public function generaPromemoriaPostiRimanenti($idEvento){
        $evento = $this->getEventoById($idEvento);
        if(!isset($evento["Capienza"]) || !isset($evento["PostiDisponibili"])) return -1;
        $postiTotali=$evento["Capienza"];
        $postiRimanenti=$evento["PostiDisponibili"];
        $nomeEvento = $evento["NomeEvento"];
        $testo = "I posti per l'evento $nomeEvento stanno finendo! Affrettati a comprare i biglietti!";
        if($postiRimanenti > $postiTotali / 4) return -1;
        //vedere se promemoria temporale esiste già: stesso idEvento, testo Hai un evento tra una settimana, giorno, oggi
        $stmt = $this->db->prepare("SELECT * FROM promemoria WHERE IdEvento = ? AND Testo = ?");
        $stmt->bind_param("is", $idEvento, $testo);
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows > 0){
            $res = $result->fetch_array(MYSQLI_ASSOC);
            return $res["IdPromemoria"];
        }
        //crea il rpomemoria
        $id=$this->getNextId('promemoria', 'IdPromemoria');
        $stmt = $this->db->prepare("INSERT INTO promemoria (IdPromemoria, idEvento, Testo) VALUES (?, ?, ?)");
        $stmt->bind_param("iis", $id, $idEvento, $testo);
        $stmt->execute();
        return $id;
    }
    
    public function generaPromemoriaEliminato($idEvento, $comprato){
        $evento = $this->getEventoById($idEvento);
        $testo = null;
        $nomeEvento = $evento["NomeEvento"];
        if(!$evento["Eliminato"]) return -1;
        if($comprato) $testo = "L'evento $nomeEvento a cui avresti partecipato e' stato cancellato, il tuo acquisto sarà rimborsato a breve";
        else $testo = "L'evento $nomeEvento che stavi osservando e' stato cancellato";
        //vedere se promemoria esiste già
        $stmt = $this->db->prepare("SELECT * FROM promemoria WHERE IdEvento = ? AND Testo = ?");
        $stmt->bind_param("is", $idEvento, $testo);
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows > 0){
            $res = $result->fetch_array(MYSQLI_ASSOC);
            return $res["IdPromemoria"];
        }
        //crea il rpomemoria
        $id=$this->getNextId('promemoria', 'IdPromemoria');
        $stmt = $this->db->prepare("INSERT INTO promemoria (IdPromemoria, idEvento, Testo) VALUES (?, ?, ?)");
        $stmt->bind_param("iis", $id, $idEvento, $testo);
        $stmt->execute();
        return $id;
    }
    
    public function generaPromemoriaEliminatoCreatore($idEvento){
        $evento = $this->getEventoById($idEvento);
        $testo = null;
        $nomeEvento = $evento["NomeEvento"];
        if(!$evento["Eliminato"]) return -1;
        $testo = "Il tuo evento '$nomeEvento' e' stato rimosso da un amministratore!";
        //vedere se promemoria esiste già
        $stmt = $this->db->prepare("SELECT * FROM promemoria WHERE IdEvento = ? AND Testo = ?");
        $stmt->bind_param("is", $idEvento, $testo);
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows > 0){
            $res = $result->fetch_array(MYSQLI_ASSOC);
            return $res["IdPromemoria"];
        }
        //crea il rpomemoria
        $id=$this->getNextId('promemoria', 'IdPromemoria');
        $stmt = $this->db->prepare("INSERT INTO promemoria (IdPromemoria, idEvento, Testo) VALUES (?, ?, ?)");
        $stmt->bind_param("iis", $id, $idEvento, $testo);
        $stmt->execute();
        return $id;
    }
    
    public function updatePromemoriaUtente($username){
        $table = null;
        $eventiUtente = null;
        $osservati = null;
        //vedere se username è creatore o utente
        if($this->contains('creatore', 'Username', $username, 's')){ // parte cretore
            $table = 'promemoria_creatore';   
            $eventiUtente = $this->getEventiCretore($username, "dataDesc");
        } else { //parte utente
            $table = 'promemoria_utente';
            $osservati = $this->getEventiInteressatiFuturi($username, "dataDesc");
            $acquistati = $this->getEventiAcquistatiFuturi($username, "dataDesc");
            $eventiUtente = array_merge($osservati, $acquistati);
        }
        //genera promemoria e aggiungi a tabella utente/creatore proemmoria mancanti
        
        foreach ($eventiUtente as $key => $value) { //comuni
            $idEvento = $value["idEvento"];
            $idProm=$this->generaPromemoriaTempo($idEvento);
            if($idProm >= 0){
                //aggiungi a tabella utente se non c'è
                $stmt = $this->db->prepare("SELECT * FROM $table WHERE Username = ? AND IdPromemoria = ?");
                $stmt->bind_param("si", $username, $idProm);
                $stmt->execute();
                $result = $stmt->get_result();
                if($result->num_rows <= 0){
                    $stmt = $this->db->prepare("INSERT INTO $table (IdPromemoria, Username) VALUES (?, ?)");
                    $stmt->bind_param("is", $idProm, $username);
                    $stmt->execute();
                }
            }
            if(!$this->contains('creatore', 'Username', $username, 's')){ //utente
                $comprato= $key >= count($osservati);
                if(!$comprato){
                    $idProm=$this->generaPromemoriaPostiRimanenti($idEvento);
                    if($idProm >= 0){
                        //aggiungi a tabella utente se non c'è
                        $stmt = $this->db->prepare("SELECT * FROM $table WHERE Username = ? AND IdPromemoria = ?");
                        $stmt->bind_param("si", $username, $idProm);
                        $stmt->execute();
                        $result = $stmt->get_result();
                        if($result->num_rows <= 0){
                            $stmt = $this->db->prepare("INSERT INTO $table (IdPromemoria, Username) VALUES (?, ?)");
                            $stmt->bind_param("is", $idProm, $username);
                            $stmt->execute();
                        }
                    }
                }
                $osservati = $this->getEventiInteressatiEliminati($username, "dataDesc");
                $acquistati = $this->getEventiAcquistatiEliminati($username, "dataDesc");
                $eventiUtente = array_merge($osservati, $acquistati);
                foreach ($eventiUtente as $key => $value) {
                    $comprato= $key >= count($osservati);
                    $idEvento = $value["idEvento"];
                    $idProm=$this->generaPromemoriaEliminato($idEvento, $comprato);
                    if($idProm >= 0){
                        //aggiungi a tabella utente se non c'è
                        $stmt = $this->db->prepare("SELECT * FROM $table WHERE Username = ? AND IdPromemoria = ?");
                        $stmt->bind_param("si", $username, $idProm);
                        $stmt->execute();
                        $result = $stmt->get_result();
                        if($result->num_rows <= 0){
                            $stmt = $this->db->prepare("INSERT INTO $table (IdPromemoria, Username) VALUES (?, ?)");
                            $stmt->bind_param("is", $idProm, $username);
                            $stmt->execute();
                        }
                    }
                }
            } else { //creatore
                foreach ($eventiUtente as $key => $value) {
                    $idEvento = $value["idEvento"];
                    $idProm=$this->generaPromemoriaEliminatoCreatore($idEvento);
                    if($idProm >= 0){
                        //aggiungi a tabella utente se non c'è
                        $stmt = $this->db->prepare("SELECT * FROM $table WHERE Username = ? AND IdPromemoria = ?");
                        $stmt->bind_param("si", $username, $idProm);
                        $stmt->execute();
                        $result = $stmt->get_result();
                        if($result->num_rows <= 0){
                            $stmt = $this->db->prepare("INSERT INTO $table (IdPromemoria, Username) VALUES (?, ?)");
                            $stmt->bind_param("is", $idProm, $username);
                            $stmt->execute();
                        }
                    }
                }
            }
            
        }
        
    }
}
?>