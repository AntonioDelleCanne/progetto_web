<?php
    require_once("bootstrap.php");
    
    $msgId = $_POST["idMessaggio"];
    $username = $_SESSION["username"];
    $tipo = $_POST["tipo"];

    if($tipo == "ricevuto") $dbh->eliminaMessaggioDest($username, $msgId);
    else if($tipo == "inviato") $dbh->eliminaMessaggioMitt($username, $msgId);
    else $dbh->eliminaPromemoria($username, $msgId);
    echo "msgId:$msgId user: $username tipo: $tipo";
?>
