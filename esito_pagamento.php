<?php
require_once("bootstrap.php");

//active buttons
$templateParams["nbMenu"] = true;
$templateParams["nbAlarm"] = true;
$templateParams["nbCart"] = true;
$templateParams["nbBack"] = true;
$templateParams["nbSearch"] = true;

$templateParams["titolo"] = "Pasuta.it - Esito Pagamento";
$templateParams["nome"] = "schermata_esito_pagamento.php";
$templateParams["errore"] = false;

$dbh->spostaDaCarrelloInAcquistati($_SESSION["username"]);

require("template/base.php");
?>