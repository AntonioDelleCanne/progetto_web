<?php
require_once("bootstrap.php");
$templateParams["nbCart"] = false;
$templateParams["nbMenu"] = true;
$templateParams["nbAlarm"] = true;
$templateParams["nbBack"] = true;
$templateParams["nbSearch"] = true;
if(isset($_SESSION["logtype"])){
    switch ($_SESSION["logtype"]) {

        case 'utente':
            $templateParams["nbCart"] = true;
        break;

        case 'creatore':

        break;

        case 'admin':

        break;
    }
} else { //Utente non loggato
    $templateParams["nbAlarm"] = false;
}

$templateParams["titolo"] = "Pasuta.it - Evento";
$templateParams["nome"] = "evento_aperto.php";
$templateParams["evento"] = $dbh->getEventoById($_GET['id']);
$templateParams["js"] = array("js/ajaxRequests.js","js/interessati.js","js/eliminazioneEvento.js");


require("template/base.php");

?>