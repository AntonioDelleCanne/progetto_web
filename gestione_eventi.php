<?php
require_once("bootstrap.php");
$templateParams["nbCart"] = false;
$templateParams["nbMenu"] = true;
$templateParams["nbAlarm"] = true;
$templateParams["nbBack"] = true;
$templateParams["nbSearch"] = false;

$templateParams["titolo"] = "Pasuta.it - Gestione eventi";
$templateParams["nome"] = "eventi_admin.php";
$templateParams["evento"] = "evento_miniatura.php";
$templateParams["eventi"] = NULL;
$templateParams["stringa_filtro"] = NULL;
$templateParams["tipo"] = $_GET["tipo"];
$templateParams["ordinamento"] = $_GET["ordinamento"];

if(isset($_GET["type"]) && $_GET["type"] != 0 ){
    $templateParams["eventi"] = $dbh->getTuttiGliEventi($_GET["tipo"], $_GET["ordinamento"], $_GET["type"], $_GET["username"]);
} else {
    $templateParams["eventi"] = $dbh->getTuttiGliEventi($_GET["tipo"], $_GET["ordinamento"], 0, "");
}

if(isset($_GET["stringa_filtro"]) && strlen($_GET["stringa_filtro"]) > 0) {
    $templateParams["stringa_filtro"] = $_GET["stringa_filtro"];
    $templateParams["eventi"] = array_filter($templateParams["eventi"], function($element){
        return stripos($element["NomeEvento"], $_GET["stringa_filtro"]) !== false ||
        stripos($element["NomeLuogo"], $_GET["stringa_filtro"]) !== false ||
        stripos($element["Username"], $_GET["stringa_filtro"]) !== false;
    });
}

require("template/base.php");
?>