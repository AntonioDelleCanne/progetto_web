<?php
require_once("bootstrap.php");

$templateParams["nbMenu"] = true;
$templateParams["nbAlarm"] = true;
$templateParams["nbCart"] = true;
$templateParams["nbBack"] = true;
$templateParams["nbSearch"] = true;

$templateParams["paginaAttiva"] = $_GET["pagina"];
$templateParams["destinatari"] = null;
$templateParams["messaggi"] = null;
$templateParams["miniatura"] = "messaggio_miniatura.php";
switch ($templateParams["paginaAttiva"]) {
    case 'ricevuti':
        $templateParams["messaggi"] = $dbh->getMessaggiRicevuti($_SESSION["username"]);
        $templateParams["js"] = array("js/ajaxRequests.js","js/gestioneMessaggiRicevuti.js");
    break;
    
    case 'inviati':
        $templateParams["messaggi"] = $dbh->getMessaggiInviati($_SESSION["username"]);
        $templateParams["js"] = array("js/ajaxRequests.js","js/gestioneMessaggiInviati.js");
    break;
    
    case 'promemoria':
        $templateParams["messaggi"] = $dbh->getPromemoria($_SESSION["username"]);
        $templateParams["js"] = array("js/ajaxRequests.js","js/gestionePromemoria.js");
    break;
    
    default:
        echo "Invalid page!";
        return;
    break;
}


//Base template
$templateParams["titolo"] = "Pasuta.it - Gestione Messaggi";
$templateParams["nome"] = "schermata_gestione_messaggi.php";

require("template/base.php");
?>