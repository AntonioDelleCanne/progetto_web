<?php
require_once("bootstrap.php");
$templateParams["nbCart"] = false;
$templateParams["nbMenu"] = true;
$templateParams["nbAlarm"] = true;
$templateParams["nbBack"] = true;
$templateParams["nbSearch"] = false;

$templateParams["titolo"] = "Pasuta.it - Gestione registrati";
$templateParams["nome"] = "registrati.php";
$templateParams["registrato"] = "registrato_miniatura.php";
$templateParams["stringa_filtro"] = NULL;
$templateParams["tipo"] = $_GET["tipo"];
$templateParams["ordinamento"] = $_GET["ordinamento"];

$templateParams["registrati"] = $dbh->getRegistrati($_GET["tipo"], $_GET["visualizza"], $_GET["ordinamento"]);

$templateParams["js"] = array("js/ajaxRequests.js","js/banUnban.js");

if(isset($_GET["stringa_filtro"]) && isset($templateParams["registrati"]) && strlen($_GET["stringa_filtro"]) > 0) {
    $templateParams["stringa_filtro"] = $_GET["stringa_filtro"];
    $templateParams["registrati"] = array_filter($templateParams["registrati"], function($element){
        return stripos($element["Nome"], $_GET["stringa_filtro"]) !== false ||
        stripos($element["Cognome"], $_GET["stringa_filtro"]) !== false ||
        stripos($element["Username"], $_GET["stringa_filtro"]) !== false;
    });
}


require("template/base.php");
?>