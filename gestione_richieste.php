<?php
require_once("bootstrap.php");
$templateParams["nbCart"] = false;
$templateParams["nbMenu"] = true;
$templateParams["nbAlarm"] = true;
$templateParams["nbBack"] = true;
$templateParams["nbSearch"] = false;

//Base template
$templateParams["titolo"] = "Pasuta.it - Gestione richieste";
$templateParams["nome"] = "richieste_creatori.php";
$templateParams["evento"] = "richiesta_miniatura.php";
$templateParams["stringa_filtro"] = NULL;
$templateParams["ordinamento"] = $_GET["ord"];

//Eventi popolari template
$templateParams["richieste"] = $dbh->getRichiesteCreatori($_GET["ord"]);
    
$templateParams["js"] = array("js/ajaxRequests.js","js/accetazioneRifiutoRichiesta.js","js/banUnban.js");

if(isset($_GET["stringa_filtro"]) && strlen($_GET["stringa_filtro"]) > 0) {
    $templateParams["stringa_filtro"] = $_GET["stringa_filtro"];
    $templateParams["richieste"] = array_filter($templateParams["richieste"], function($element){
        return stripos($element["Nome"], $_GET["stringa_filtro"]) !== false ||
        stripos($element["Cognome"], $_GET["stringa_filtro"]) !== false ||
        stripos($element["Username"], $_GET["stringa_filtro"]) !== false;
    });
}

require("template/base.php");
?>