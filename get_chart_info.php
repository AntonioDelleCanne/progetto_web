<?php
    require_once("bootstrap.php");
    
    $username = $_SESSION["username"];

    $articoli = $dbh->getEventiCarrello($username);
    $nArticoli = 0;
    $sum = 0;
    foreach ($articoli as $key => $value) {
        if(isset($value["Prezzo"]))$sum += $value["Prezzo"] * $value["Quantita"];
        $nArticoli += $value["Quantita"];
    }
    echo json_encode(array("narticoli" => $nArticoli, "totale" => $sum == 0 ? "gratuito" : number_format((float)$sum, 2, '.', '')));
?>
