<?php
require_once("bootstrap.php");

//Login
if(isset($_POST["username"])){
    $_SESSION["username"] = $_POST["username"];
    $_SESSION["logtype"] = $dbh->checkCredentials($_POST["username"], $_POST["password"]);
}

//Base template
$templateParams["titolo"] = "Pasuta.it - Home";
$templateParams["evento"] = "evento_miniatura.php";

$templateParams["nbBack"] = false;
$templateParams["nbCart"] = false;
$templateParams["nbAlarm"] = true;
$templateParams["nbMenu"] = true;
$templateParams["nbSearch"] = true;
$templateParams["n"] = 2;

//Particular template
if(isset($_SESSION["logtype"])){
    switch ($_SESSION["logtype"]) {

        case 'utente':
            $templateParams["nome"] = "eventi_popolari.php";
            $templateParams["eventipagina"] = $dbh->getEventiPopolari();
            $templateParams["nbCart"] = true;
        break;

        case 'creatore':
            $templateParams["nome"] = "home_creatore.php";   
            $templateParams["eventiFuturi"] = $dbh->getNEventiCreatoreFuturi($templateParams["n"], $_SESSION["username"]);
            $templateParams["eventiPassati"] = $dbh->getNEventiCreatorePassati(2, $_SESSION["username"]);
            $templateParams["eventiEliminati"] = $dbh->getNEventiCreatoreEliminati(2, $_SESSION["username"]);
        break;

        case 'admin':
            $templateParams["nome"] = "home_admin.php";   
            $templateParams["nbSearch"] = false;
        break;
    }
} else { //Utente non loggato
    $templateParams["nome"] = "eventi_popolari.php";
    $templateParams["eventipagina"] = $dbh->getEventiPopolari();
    $templateParams["nbAlarm"] = false;
}

require("template/base.php");
?>