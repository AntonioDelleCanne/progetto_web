<?php
require_once("bootstrap.php");
$templateParams["nbCart"] = true;
$templateParams["nbMenu"] = true;
$templateParams["nbAlarm"] = true;
$templateParams["nbBack"] = true;
$templateParams["nbSearch"] = true;
if(!isset($_SESSION["logtype"]) || $_SESSION["logtype"] != "utente"){ //Caso che non dovrebbe mai verificarsi
    $templateParams["nbAlarm"] = false;
    $templateParams["nbCart"] = false;
    $templateParams["nbSearch"] = false;
}

//Base template
$templateParams["titolo"] = "Pasuta.it - Interessati";
$templateParams["nome"] = "eventi_interessati.php";
$templateParams["evento"] = "evento_miniatura.php";

//Eventi popolari template
if(isset($_SESSION["username"])){ //Caso che dovrebbe sempre verificarsi
    $templateParams["eventiFuturi"] = $dbh->getNEventiInteressatiFuturi($_GET["n"], $_SESSION["username"]);
    $templateParams["eventiPassati"] = $dbh->getNEventiInteressatiPassati($_GET["n"], $_SESSION["username"]);
    $templateParams["eventiEliminati"] = $dbh->getNEventiInteressatiEliminati($_GET["n"], $_SESSION["username"]);
}

require("template/base.php");
?>