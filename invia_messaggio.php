<?php
require_once("bootstrap.php");

$stringaDestinatari = $_POST["inputNome"];
if($stringaDestinatari == "Partecipanti" && isset($_POST["evento"]) && $dbh->contains('creatore', 'Username', $_SESSION["username"], 's')){
    $dbh->inviaMessaggioAPartecipanti($_POST["evento"], $_SESSION["username"], $_POST["oggetto"], $_POST["testo"]);
} else if($stringaDestinatari == "Creatori" && $dbh->contains('amministratore', 'Username', $_SESSION["username"], 's')){
    $dbh->inviaMessaggioACreatori($_SESSION["username"], $_POST["oggetto"], $_POST["testo"]);
} else if($stringaDestinatari == "Utenti" && $dbh->contains('amministratore', 'Username', $_SESSION["username"], 's')){
    $dbh->inviaMessaggioAUtenti($_SESSION["username"], $_POST["oggetto"], $_POST["testo"]);
} else if($stringaDestinatari == "Tutti" && $dbh->contains('amministratore', 'Username', $_SESSION["username"], 's')){
    $dbh->inviaMessaggioATutti($_SESSION["username"], $_POST["oggetto"], $_POST["testo"]);
} else {
    $destinatari = explode(";", preg_replace('/\s/', '', $stringaDestinatari));
    $dbh->inviaMessaggio($_SESSION["username"], $destinatari, $_POST["oggetto"], $_POST["testo"]);
}

header("Location: index.php");
?>