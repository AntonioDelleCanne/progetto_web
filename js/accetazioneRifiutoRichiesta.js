$(document).ready(function () {
    $(".rifiuto-richiesta").click(function(){
        let parent = $(this).parents(".event").first();
        let username= parent.attr("data-username");
        gestisciRichiesta(username, "rifiuto");
        location.reload();
    });
    
    $(".accettazione-richiesta").click(function(){
        let parent = $(this).parents(".event").first();
        let username= parent.attr("data-username");
        gestisciRichiesta(username, "accettazione");
        location.reload();
    });

});

function gestisciRichiesta(username, tipo){
    const params = "&username=" + username + "&tipo=" + tipo;
    fun = function(xhttp) {
        console.log(xhttp);
        if(xhttp === "error"){
            return false;
        }
        return true;
    }
    return loadDocPostSync("accettazione_rifiuto_richieste.php", fun, params);
}