function loadDocGetAsync(url, cFunction, params) {
  var xhttp;
  xhttp=new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      cFunction(this.responseText);
    }
 };
  xhttp.open("GET", url + "?" + params, true);
  xhttp.send();
}

function loadDocPostAsync(url, cFunction, params) {
  var xhttp;
  xhttp=new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      cFunction(this.responseText);
    }
 };
  xhttp.open("POST", url, true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(params);
}

function loadDocGetSync(url, cFunction, params) {
  var xhttp;
  xhttp=new XMLHttpRequest();
  xhttp.open("GET", url + "?" + params, false);
  xhttp.send();
  return cFunction(xhttp.responseText);
}

function loadDocPostSync(url, cFunction, params) {
  var xhttp;
  xhttp=new XMLHttpRequest();
  xhttp.open("POST", url, false);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(params);
  return cFunction(xhttp.responseText);
}