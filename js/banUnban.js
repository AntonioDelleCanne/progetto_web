const plus = '<rm class="fas fa-plus-square" aria-hidden="true" title="Mostra più informazioni"></rm>';
const minus = '<rm class="fas fa-minus-square" aria-hidden="true" title="Mostra meno informazioni"></rm>';

$(document).ready(function () {
    $(".ban").click(function(){
        let parent = $(this).parents(".event").first();
        let username= parent.attr("data-username");
        console.log(username);
        gestisciBanUnban(username, "true"); 
        location.reload();   
    });
    
    $(".unban").click(function(){
        let parent = $(this).parents(".event").first();
        let username= parent.attr("data-username");
        console.log(username);
        gestisciBanUnban(username, "false"); 
        location.reload();   
    });
    
    $(".hiding-zone").on('hide.bs.collapse', function () {
        let caller = $("button[aria-controls="+ $(this).attr("id") +"]");
        caller.html(plus);
        caller.attr("data-btn", "plus");
      });

    $(".hiding-zone").on('show.bs.collapse', function () {
        let caller = $("button[aria-controls="+ $(this).attr("id") +"]");
        caller.html(minus);
        caller.attr("data-btn", "minus");
      });
    
});

function gestisciBanUnban(username, ban){
    const params = "registrato=" + username + "&ban=" + ban;
    funz = function(xhttp) {
        console.log(xhttp);
        if(xhttp === "error"){
            return false;
        }
        return true;
    }
    return loadDocPostAsync("./ban_unban_registrato.php", funz, params);
}