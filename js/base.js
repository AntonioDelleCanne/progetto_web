$(document).ready(function () {
    updatePromemoria();
    //check if notifications need to be generated
    //check if chart needs to be cleaned
    setInterval(function(){
        updatePromemoria();
    }, 500);
});

function updatePromemoria(){
    const params = "";
    fun = function(xhttp) {
        console.log(xhttp);
        if(xhttp === "error"){
            return false;
        }
        let res = JSON.parse(xhttp);
        let nNotifiche = res.npromemoria + res.nmessaggi;
        let nArticoli = res.narticoli;
        if(nArticoli != -1) $("#chartNumber").text(nArticoli > 0 ? nArticoli : "");
        if(nArticoli > 0) $("#btnchart").addClass("notified"); else $("#btnchart").removeClass("notified");
        if(nNotifiche > 0) $("#menuNotificheCollapse").addClass("notified"); else $("#menuNotificheCollapse").removeClass("notified");
        $("#notificationNumber").text(nNotifiche > 0 ? nNotifiche : "");
        if(res.nmessaggi > 1) $("#nmessaggi").html('<a href="gestione_messaggi.php?pagina=ricevuti" ><rm class="fas fa-comments"></rm> Hai ' + res.nmessaggi + ' messaggi non letti!</a>');
        else if (res.nmessaggi == 1) $("#nmessaggi").html('<a href="gestione_messaggi.php?pagina=ricevuti" ><rm class="fas fa-comments"></rm> Hai un messaggio non letto!</a>');
        else $("#nmessaggi").html("");
        if(res.npromemoria > 1) $("#npromemoria").html('<a href="gestione_messaggi.php?pagina=promemoria" ><rm class="fas fa-exclamation"></rm> Hai ' + res.npromemoria + ' promemoria non letti!</a>');
        else if (res.npromemoria == 1) $("#npromemoria").html('<a href="gestione_messaggi.php?pagina=promemoria" ><rm class="fas fa-exclamation"></rm> Hai un promemoria non letto!</a>');
        else $("#npromemoria").html("");
        return true;
    }
    return loadDocPostAsync("updatePromemoria.php", fun, params);
}