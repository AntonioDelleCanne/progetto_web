$(document).ready(function () {
    updateChartInfo();
    $(".chart-trash").click(function(){
        removeFromChart($(this).parents(".event").first());
        updateChartInfo();
    });
    $(".chart-plus, .chart-minus").click(function(){
        let parent = $(this).parents(".event").first();
        let eventId= parent.attr("data-idevento");
        let delta = 1;
        let pageValue = parent.find(".n-biglietti").first();
        if($(document).width() < 768){
            pageValue = parent.find(".n-biglietti").eq(1);
        }
        let actual = pageValue.attr("data-nbiglietti");

        if($(this).attr("class").includes("chart-minus")){
            delta = -1;
        } else {
            if(getDisponibili(eventId) <= 0){
                parent.find(".chart-plus").prop("disabled", true);
                return;
            }
        }
        actual = +actual + +delta;
        
        if(actual <= 0){
            removeFromChart($(this).parents(".event").first());
        }else {
            if(updateDatabaseNumber(eventId, actual)){
                pageValue.text("Quantità: " + actual);
                pageValue.attr("data-nbiglietti", actual);
            } else {
                alert("An error occured! Couldn't update the value!");
            }
        }
        updateChartInfo();
        if(getDisponibili(eventId) <= 0){
            parent.find(".chart-plus").prop("disabled", true);
        } else {
            parent.find(".chart-plus").prop("disabled", false);
        }
    });
    //get time added to chart from the document, then update it each second
    let maxTimeSeconds = 900;
    setInterval(function(){
        $("div[data-tempoaggiunta]").each(function (index, element) {
            let t = $(this).attr("data-tempoaggiunta").split(/[- :]/);
            let tempoAggiunta = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
            let timeDiffSeconds = (new Date() - tempoAggiunta) / 1000;
            let displayTimer = $(this).find(".timer").first();
            if($(document).width() < 768){
                displayTimer = $(this).find(".timer").eq(1);
            }
            let updatedTime = maxTimeSeconds-timeDiffSeconds;
            if(updatedTime < 0){
                $(this).remove();
            }
            displayTimer.text("Tempo: " + String(Math.floor(updatedTime/60)).padStart(2, '0') + ":" + String(Math.round(updatedTime)%60).padStart(2, '0'));
            updateChartInfo();
        });
    }, 1000);
});

function removeFromChart(event){
    let eventId= event.attr("data-idevento");
    if(removeFromDatabaseChart(eventId)){
        event.remove();
    } else {
        alert("An error occured! Couldn't remove the event from the chart!");
    }
}

function removeFromDatabaseChart(eventId){
    return updateDatabaseNumber(eventId, 0);
}

function updateDatabaseNumber(eventId, newValue){
    const params = "value=" + newValue + "&eventid=" + eventId;
    fun = function(xhttp) {
        console.log(xhttp);
        if(xhttp === "error"){
            return false;
        }
        return true;
    }
    return loadDocPostSync("modifica_quantita_carrello.php", fun, params);
}

function getDisponibili(idevento){
    const params = "id=" + idevento;
    fun = function(xhttp) {
        //console.log(xhttp);
        if(xhttp === "error"){
            return -2;
        }
        let res = JSON.parse(xhttp);
        return res.disponibili;
    }
    return loadDocPostSync("get_disponibili.php", fun, params);
}

function updateChartInfo(){
    const params = "";
    fun = function(xhttp) {
        //console.log(xhttp);
        if(xhttp === "error"){
            return false;
        }
        let res = JSON.parse(xhttp);
        if(res.totale == "gratuito"){
            $("#totale").html('Totale: ' + res.totale);
        } else {
            $("#totale").html('Totale: ' + res.totale + '<em class="fas fa-euro-sign" aria-hidden="true"></em>');
        }
        $("#narticoli").html('Articoli: ' + res.narticoli);
        return true;
    }
    return loadDocPostSync("get_chart_info.php", fun, params);
}

function updateTimer(){
    return false;
}