function deactivateField(checkbox){
    console.log(checkbox);
    
    if(checkbox.checked){
        //disattiva
        document.getElementById(checkbox.getAttribute("data-rif")).disabled  = true;
        document.getElementById(checkbox.getAttribute("data-rif")).value  = null;
    } else {
        //attiva
        document.getElementById(checkbox.getAttribute("data-rif")).disabled  = false;
        document.getElementById(checkbox.getAttribute("data-rif")).value  = "";
    }
}
