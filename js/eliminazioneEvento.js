$(document).ready(function () {
    $("#eliminaEvento").click(function(){
        let parent = $(this).parents(".event").first();
        let eventId= parent.attr("data-idevento");
        eliminaEvento(eventId);
        location.reload();
    });
});

function eliminaEvento(eventId){
    const params = "&eventid=" + eventId;
    fun = function(xhttp) {
        console.log(xhttp);
        if(xhttp === "error"){
            return false;
        }
        return true;
    }
    return loadDocPostSync("eliminaEvento.php", fun, params);
}