$(document).ready(function () {
    //a lancio pagina nascondere tutti tranne #messaggi_ricevuti, a presisne pulsanti cambiare, forse richiedere di volta in volta con ajax
    $(".chart-trash").click(function(){
        removeFromMessaggi($(this).parents(".msg").first());
    });
    $(".chart-visto").click(function(){
        let msg = $(this).parents(".msg").first();
        let visto = msg.attr("data-visto") == 1 ? 0 : 1;
        let testoContrassegna = msg.find(".contrassegna-testo").first();
        let pallino = msg.find(".pallino-visto").first();
        console.log(visto);
        
        if(!setVisto(msg.attr("data-messaggio"), visto)){
            alert("An error occured! Couldn't update the message!");
            return;
        }
        //toggle visto
        msg.attr("data-visto", visto);

        //aggiornamento icone
        if(visto){
            //cambia occhio
            $(this).html('<em class="far fa-eye-slash" aria-hidden="true" title="Contrassegna come non letto"></em>');
            //cambia contrassegna come
            testoContrassegna.text("Non letto");
            //cambia pallino
            pallino.html('<em class="far fa-circle" aria-hidden="true" title="Messaggio letto"></em>');
        } else {
            //cambia occhio
            $(this).html('<em class="far fa-eye" aria-hidden="true" title="Contrassegna come letto"></em>');
            //cambia contrassegna come
            testoContrassegna.text("Letto");
            //cambia pallino
            pallino.html('<em class="fas fa-circle" aria-hidden="true" title="Messaggio non letto"></em>');
        }
        
    });
});

function removeFromMessaggi(messaggio){
    let idMessaggio= messaggio.attr("data-messaggio");
    if(removeFromDatabaseMessaggio(idMessaggio)){
        messaggio.remove();
    } else {
        alert("An error occured! Couldn't remove the message!");
    }
}

function removeFromDatabaseMessaggio(idMesaggio){
    const params = "idMessaggio=" + idMesaggio + "&tipo=inviato";
    fun = function(xhttp) {
        console.log(xhttp);
        if(xhttp === "error"){
            return false;
        }
        return true;
    }
    return loadDocPostSync("elimina_messaggio.php", fun, params);
}

