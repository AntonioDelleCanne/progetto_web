const textint = '<em class="fas fa-eye" aria-hidden="true"></em> Interessati: ';

$(document).ready(function () {

    $(".star").each(function(){
        let starIcon = $(this).find(".fa-star").first();
        let stato = starIcon.attr("data-stato");
        if(stato == "acceso"){
            this.style.color = "rgb(255,148,41)";
            starIcon.attr("data-stato", "spento");
        }else{
            this.style.color = "rgb(38,38,38)";
            starIcon.attr("data-stato", "acceso");
        }
    });

    $(".star").click(function(){
        let parent = $(this).parents(".event").first();
        let eventId= parent.attr("data-idevento");
        let starIcon = $(this).find(".fa-star").first();
        let stato = starIcon.attr("data-stato");
        let ninteressati = parseInt($("#ninteressati").attr("data-ninteressati"));
        if(stato == "acceso"){
            updateOsservato("spento", eventId);
            ninteressati = ninteressati - 1;
            this.style.color = "rgb(38,38,38)";
            starIcon.attr("data-stato", "spento");
        }else{
            updateOsservato("acceso", eventId);
            ninteressati = ninteressati + 1;
            this.style.color = "rgb(255,148,41)";
            starIcon.attr("data-stato", "acceso");
        }
        $("#ninteressati").attr("data-ninteressati", ninteressati);
        $("#ninteressati").html(textint + ninteressati);
    });
});

function updateOsservato(stato, eventId){
    const params = "stato=" + stato + "&eventid=" + eventId;
    fun = function(xhttp) {
        console.log(xhttp);
        if(xhttp === "error"){
            return false;
        }
        return true;
    }
    return loadDocPostSync("aggiungi_rimuovi_osservati.php", fun, params);
}