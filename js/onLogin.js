$(document).ready(function () {
    $("form").submit(function(e){
        e.preventDefault();
        let isFormOk = true;
        const nome_utente = $("input#username");
        const password = $("input#password");
        password.siblings(".error-prompt").remove();
        isFormOk = check_nome_utnete(nome_utente, password);
        if(isFormOk){
            e.currentTarget.submit();
        }
    });
});

function check_nome_utnete(nome_utente, password) {
    const params = "username=" + nome_utente.val() + "&password=" + password.val();
    fun = function(xhttp) {
        console.log(xhttp);
        if(xhttp === "invalid"){
            password.parent().append('<p class ="error-prompt">Credenziali errate</p>');
            return false;
        }
        return true;
    }
    return loadDocPostSync("check_credentials.php", fun, params);
}
