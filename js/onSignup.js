
var urlParams = new URLSearchParams(window.location.search);
$(document).ready(function () {
    $("form").submit(function(e){
        e.preventDefault();
        let isFormOk = true;
        const nome_utente = $("input#nome-utente");
        const password = $("input#password");
        const conferma_password = $("input#conferma-password");
        const mail = $("input#mail");
        password.siblings(".error-prompt").remove();
        nome_utente.siblings(".error-prompt").remove();
        conferma_password.siblings(".error-prompt").remove();
        mail.siblings(".error-prompt").remove();
        isFormOk = check_nome_utente(nome_utente) & check_password(password) & check_conferma_password(conferma_password, password) & check_mail(mail);
        if (urlParams.get("tipo_signup") === "creatore"){
            console.log("creatore");
            const CF = $("input#cf");
            const cartaID = $("input#n-id");
            CF.siblings(".error-prompt").remove();
            cartaID.siblings(".error-prompt").remove();
            isFormOk = isFormOk & check_idcard(cartaID) & check_cf(CF);
        }
        if(isFormOk){
            e.currentTarget.submit();
        }
    });
});


function check_mail(mail) {
    const pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;
    if(mail.val()==undefined || !pattern.test(mail.val())){
        mail.parent().append('<p class ="error-prompt">Formato E-mail non valido</p>')
        return false;
    }
    const params = "mail=" + mail.val();
    fun = function(xhttp) {
        if(xhttp === "true"){
            mail.parent().append('<p class ="error-prompt">E-mail già in uso</p>');
            return false;
        } 
        return true;
    }
    return loadDocPostSync("check_existing_user.php", fun, params);
}

function check_nome_utente(nome_utente) {
    const pattern = /^(?:[0-9]+[a-z]|[a-z]+[0-9])[a-z0-9]*$/i;
    if(nome_utente.val()==undefined || nome_utente.val().length<5 || !pattern.test(nome_utente.val())){
        nome_utente.parent().append('<p class ="error-prompt">Nome Utente deve essere lungo almeno 5 caratteri e deve contenere almeno un numero</p>')
        return false;
    }
    const params = "username=" + nome_utente.val();
    fun = function(xhttp) {
        if(xhttp === "true"){
            nome_utente.parent().append('<p class ="error-prompt">Nome utente già in uso</p>');
            return false;
        } 
        return true;
    }
    return loadDocPostSync("check_existing_user.php", fun, params);
}

function check_password(password) {
    const pattern = /^(?:[0-9]+[a-z]|[a-z]+[0-9])[a-z0-9]*$/i;
    if(password.val().length<8 || !pattern.test(password.val())){
        password.parent().append('<p class ="error-prompt">Password deve essere lunga almeno 8 caratteri e deve contenere almeno un numero</p>');
        return false;
    }
    return true;
}

function check_conferma_password(conferma, password) {
    if(password.val() !== conferma.val()){
        conferma.parent().append('<p class ="error-prompt">La password di conferma deve coincidere con la password</p>');
        return false;
    }
    return true;
}

function check_cf(cf) {
    const pattern = /^[A-Za-z]{6}[0-9LMNPQRSTUV]{2}[A-Za-z]{1}[0-9LMNPQRSTUV]{2}[A-Za-z]{1}[0-9LMNPQRSTUV]{3}[A-Za-z]{1}$/i;
    if(cf.val()==undefined || !pattern.test(cf.val())){
        cf.parent().append('<p class ="error-prompt">Codice fiscale non valido</p>')
        return false;
    }
    const params = "cf=" + cf.val();
    fun = function(xhttp) {
        if(xhttp === "true"){
            cf.parent().append('<p class ="error-prompt">Un altro utente si e\' gia\' registrato con questo codice fiscale</p>');
            return false;
        } 
        return true;
    }
    return loadDocPostSync("check_existing_user.php", fun, params);
}

function check_idcard(idcard) {
    const pattern = /^C[A-Z]\d{5}[A-Z][A-Z]$/i;
    if(idcard.val()==undefined || !pattern.test(idcard.val())){
        idcard.parent().append('<p class ="error-prompt">Numero carta non valido</p>')
        return false;
    }
    const params = "idcard=" + idcard.val();
    fun = function(xhttp) {
        if(xhttp === "true"){
            idcard.parent().append('<p class ="error-prompt">Un altro utente si e\' gia\' registrato con lo stesso codice di carta</p>');
            return false;
        } 
        return true;
    }
    return loadDocPostSync("check_existing_user.php", fun, params);
}