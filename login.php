<?php
require_once("bootstrap.php");
$templateParams["nbMenu"] = false;
$templateParams["nbAlarm"] = false;
$templateParams["nbCart"] = false;
$templateParams["nbBack"] = true;
$templateParams["nbSearch"] = false;

//Base template
$templateParams["titolo"] = "Pasuta.it - Login";
$templateParams["nome"] = "login-template.php";
$templateParams["js"] = array("js/ajaxRequests.js","js/onLogin.js");

require("template/base.php");
?>