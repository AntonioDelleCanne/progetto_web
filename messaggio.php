<?php
require_once("bootstrap.php");
$templateParams["nbMenu"] = false;
$templateParams["nbAlarm"] = false;
$templateParams["nbCart"] = false;
$templateParams["nbBack"] = true;
$templateParams["nbSearch"] = false;

//Base template
$templateParams["titolo"] = "Pasuta.it - Messaggio";
$templateParams["nome"] = "schermata_invio_messaggio.php";
$templateParams["messaggio"] = null;
$templateParams["mittente"] = null;
$templateParams["destinatari"] = null;
$templateParams["onsubmit"] = "";
$templateParams["risposta"] = isset($_POST["submitbtn"]) && $_POST["submitbtn"] == "rispondi";


if(isset($_GET["risposta"]))
$templateParams["idMessaggio"] = $_GET["risposta"];

if(isset($_GET["id"]))
$templateParams["idMessaggio"] = $_GET["id"];

if(isset($templateParams["idMessaggio"])){
    $templateParams["messaggio"] = $dbh->getMessaggio($templateParams["idMessaggio"]);
    $templateParams["mittente"] = $templateParams["messaggio"]["UsernameMittente"];
    $templateParams["destinatari"] = $dbh->getDestinatari($templateParams["idMessaggio"]);
}

if(isset($_GET["id"]) && in_array($_SESSION["username"], explode(";", multi_implode($templateParams["destinatari"], ";")))){
    if($templateParams["mittente"] != $_SESSION["username"] && !in_array($_SESSION["username"], explode(";", multi_implode($templateParams["destinatari"], ";")))) {
        echo "Errore! Indirizzo non valido!";
        return;
    }
    $templateParams["onsubmit"] = "messaggio.php?risposta=".$templateParams["idMessaggio"];
} else if(isset($_GET["risposta"]) && !in_array($_SESSION["username"], explode(";", multi_implode($templateParams["destinatari"], ";")))){
    echo "Errore! Indirizzo non valido!";
    return;
} else {
    $templateParams["onsubmit"] = "invia_messaggio.php";
}
$templateParams["js"] = array("js/ajaxRequests.js","js/script_invio_messaggio.js");

require("template/base.php");
?>