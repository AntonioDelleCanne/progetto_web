<?php
require_once("bootstrap.php");

$actualImage = $dbh->getEventoById($_POST["id-evento"])["Immagine"];
$filename = $actualImage;
if($_FILES['fileToUpload']['error'] === UPLOAD_ERR_OK) $filename = $_FILES["fileToUpload"]["name"];

$dbh->modificaEvento($_POST["id-evento"] ,$_SESSION["username"], isset($_POST["capienza"]) ? $_POST["capienza"] : null, $_POST["nome-evento"], $filename, $_POST["data-inizio"], $_POST["ora-inizio"].":00", $_POST["data-fine"], $_POST["ora-fine"].":00", $_POST["luogo"], isset($_POST["prezzo"]) ? $_POST["prezzo"] : null, $_POST["descrizione"], $_POST["via"], $_POST["n"], $_POST["cap"], $_POST["provincia"], $_POST["stato"], $_POST["citta"]);

if($_FILES['fileToUpload']['error'] === UPLOAD_ERR_OK) require("upload_file.php");
header("Location: index.php");
?>