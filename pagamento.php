<?php
require_once("bootstrap.php");
ob_start();
require("get_chart_info.php");
ob_end_clean();

if(/*$sum == 0*/false){
    require("index.php");
} else {
    
    
    //active buttons
    $templateParams["nbMenu"] = true;
    $templateParams["nbAlarm"] = true;
    $templateParams["nbCart"] = true;
    $templateParams["nbBack"] = true;
    $templateParams["nbSearch"] = true;
    
    $templateParams["totale"] = number_format((float)$sum, 2, '.', '');
    $templateParams["eventicarrello"] = $dbh->getEventiCarrello($_SESSION["username"]);
    $templateParams["evento"] = "evento_minitura_pagamento.php";
    $templateParams["narticoli"] = $nArticoli;
    
    //Base template
    $templateParams["titolo"] = "Pasuta.it - Pagamento";
    $templateParams["nome"] = "schermata_pagamento.php";
    $templateParams["js"] = array("js/ajaxRequests.js","js/pagamento.js");
    
    require("template/base.php");
}
?>