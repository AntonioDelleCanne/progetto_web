<?php
require_once("bootstrap.php");
$templateParams["nbMenu"] = false;
$templateParams["nbAlarm"] = false;
$templateParams["nbCart"] = false;
$templateParams["nbBack"] = true;
$templateParams["nbSearch"] = false;

$templateParams["titolo"] = "Pasuta.it - Privacy Policy";
$templateParams["nome"] = "privacy_policy.php";

require("template/base.php");
?>