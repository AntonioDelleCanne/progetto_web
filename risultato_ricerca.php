<?php
require_once("bootstrap.php");

//Base template
$templateParams["titolo"] = "Pasuta.it - Risulati Ricerca";
$templateParams["evento"] = "evento_miniatura.php";

$templateParams["nbBack"] = false;
$templateParams["nbCart"] = false;
$templateParams["nbAlarm"] = true;
$templateParams["nbMenu"] = true;
$templateParams["nbSearch"] = true;

$templateParams["nome"] = "schermata_risultato_ricerca.php";

$templateParams["stringa_ricerca"] = $_GET["stringa_ricerca"];

//Particular template
if(isset($_SESSION["logtype"])){
    switch ($_SESSION["logtype"]) {

        case 'utente':
            $templateParams["eventipagina"] = $dbh->getEventiAttiviByKeyword($templateParams["stringa_ricerca"]);
            $templateParams["nbCart"] = true;
        break;

        case 'creatore':
            $templateParams["eventipagina"] = $dbh->getEventiCreatoreByKeyword($_SESSION["username"], $templateParams["stringa_ricerca"]);
        break;
    }
} else { //Utente non loggato
    $templateParams["eventipagina"] = $dbh->getEventiAttiviByKeyword($templateParams["stringa_ricerca"]);
    $templateParams["nbAlarm"] = false;
}

require("template/base.php");
?>