<?php
    require_once("bootstrap.php");
    
    $msgId = $_POST["idMessaggio"];
    $username = $_SESSION["username"];
    $visto = $_POST["visto"];
    $tipo = $_POST["tipo"];
    if($tipo == "ricevuto")
        if($visto) $dbh->setVisto($username, $msgId);
        else $dbh->setNonVisto($username, $msgId);
    else
        if($visto) $dbh->setVistoProm($username, $msgId);
        else $dbh->setNonVistoProm($username, $msgId);
    echo "msgId:$msgId user: $username visto: $visto";
?>
