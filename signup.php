<?php
require_once("bootstrap.php");

$templateParams["nbMenu"] = false;
$templateParams["nbAlarm"] = false;
$templateParams["nbCart"] = false;
$templateParams["nbBack"] = true;
$templateParams["nbSearch"] = false;

$templateParams["js"] = array("js/ajaxRequests.js", "js/onSignup.js");

if (isset($_GET["tipo_signup"])){
    if($_GET["tipo_signup"] == "utente"){
        $templateParams["titolo"] = "Pasuta.it - Signup Utente";
        $templateParams["nome"] = "signup-utente.php";
    } else if ($_GET["tipo_signup"] == "creatore"){
        $templateParams["titolo"] = "Pasuta.it - Signup Creatore";
        $templateParams["nome"] = "signup-creatore.php";
    }else {
        console.log("ERROR: tipo_signup not set");
    }
} else {
    console.log("ERROR: tipo_signup not set");
    
}

require("template/base.php");
?>