<?php
require_once("bootstrap.php");
$templateParams["nbMenu"] = false;
$templateParams["nbAlarm"] = false;
$templateParams["nbCart"] = false;
$templateParams["nbBack"] = true;
$templateParams["nbSearch"] = false;

$templateParams["titolo"] = "Pasuta.it - Signup Successful";
$templateParams["nome"] = "signup_ok.php";

$nome = $_POST["nome"]; 
$cognome = $_POST["cognome"]; 


if (isset($_POST["iban"])){ //signup creatore
    $templateParams["tipo_signup"] = "creatore";
    $iban = $_POST["iban"]; 
    $dbh->aggiungiCreatore($_POST["nome"], $_POST["cognome"], $_POST["nome-utente"], $_POST["password"], $_POST["mail"], $_POST["data-nascita"], $_POST["cf"], $_POST["n-id"], $_POST["iban"]);
} else { //signup utente
    $templateParams["tipo_signup"] = "utente";
    $dbh->aggiungiUtente($_POST["nome"], $_POST["cognome"], $_POST["nome-utente"], $_POST["password"], $_POST["mail"], $_POST["data-nascita"]);
}

require("template/base.php");
?>