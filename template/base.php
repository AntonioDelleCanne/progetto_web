<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html>
<html xml:lang="it" lang="it">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><?php echo $templateParams["titolo"]; ?></title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">

    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/regular.js" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
    
    <!-- jQuery CDN -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

    <!-- Custom includes -->
    <link rel="stylesheet" href="css/style.css" >
    <?php
    if(isset($templateParams["js"])):
        foreach($templateParams["js"] as $script):
    ?>
        <script src="<?php echo $script; ?>"></script>
    <?php
        endforeach;
    endif;
    ?>
    <?php if(isset($_SESSION["username"])):?>
    <script src="js/ajaxRequests.js"></script>
    <script src="js/base.js"></script>
    <?php endif;?>
    <script src="js/sideBarScript.js"></script>
    
</head>
<body>
    <div class="wrapper">
        
        <!-- Sidemenu  -->
        <nav class = "bg-light" id="sidebar">
            <div class="row no-gutters sidebar-header">
                <div class="text-light col-10">
                    <h2 class="h3"><?php if (isset($_SESSION['logtype'])) {
                        echo "Ciao " . $_SESSION['username'] . "!";
                    } else {
                        echo "Benvenuto!";
                    } ?></h2>
                </div>
                <div class="col-2" id="dismiss">
                    <em class="fas fa-arrow-left" aria-hidden="true" title="Esci dal menù laterale"></em>
                </div>
            </div>
                  
            <ul class="list-unstyled CTAs">
                <li>
                    <a href="./index.php">Home</a>
                </li>

                <?php if (!isset($_SESSION['logtype'])) : ?>
                <li>
                    <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">Registrati</a>
                    <ul class="collapse list-unstyled" id="pageSubmenu">
                        <li>
                            <a href="./signup.php?tipo_signup=utente">Come Utente</a>
                        </li>
                        <li>
                            <a href="./signup.php?tipo_signup=creatore">Come Creatore</a> 
                        </li>
                    </ul>
                </li>
                <li class="sideButton mt-3">
                    <a href="./login.php" class="orangeBtn">Accedi</a>
                </li>

                <?php elseif(isset($_SESSION['logtype'])): 
                switch ($_SESSION["logtype"]):
                    case 'utente':?> 
                    <li>
                        <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">Gestisci eventi</a>
                        <ul class="collapse list-unstyled" id="pageSubmenu">
                            <li>
                                <a href="interessati.php?n=2">Eventi osservati</a>
                            </li>
                            <li>
                                <a href="acquisti.php?n=2">Eventi acquistati</a>
                            </li>
                        </ul>
                    </li>
                    <?php break;

                    case 'creatore':?>
                    <li>
                        <a href="creazione.php">Crea evento</a>
                    </li>
                    <li>
                        <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">Gestisci eventi</a>
                        <ul class="collapse list-unstyled" id="pageSubmenu">
                            <li>
                                <a href="./eventi_creatore.php?tipo=tutti&ordinamento=dataAsc">Tutti gli eventi</a>
                            </li>
                            <li>
                                <a href="./eventi_creatore.php?tipo=futuri&ordinamento=dataAsc">Eventi Futuri</a>
                            </li>
                            <li>
                                <a href="./eventi_creatore.php?tipo=passati&ordinamento=dataDesc">Eventi Passati</a>
                            </li>
                            <li>
                                <a href="./eventi_creatore.php?tipo=eliminati&ordinamento=dataAsc#">Eventi Cancellati</a>
                            </li>
                        </ul>
                    </li>
                    <?php break;

                    case 'admin':?>
                    <li>
                        <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">Gestisci</a>
                        <ul class="collapse list-unstyled" id="pageSubmenu">
                            <li>
                                <a href="./gestione_richieste.php?ord=dataAsc">Richieste creatori</a>
                            </li>
                            <li>
                                <a href="./gestione_registrati.php?tipo=tutti&visualizza=tutti&ordinamento=usrAsc">Utenti e creatori</a>
                            </li>
                            <li>
                                <a href="./gestione_eventi.php?tipo=tutti&ordinamento=nomeAsc">Eventi</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#pageSubmenu2" data-toggle="collapse" aria-expanded="false">Invia</a>
                        <ul class="collapse list-unstyled" id="pageSubmenu2">
                            <li>
                                <a href="./messaggio.php?dest=Tutti">Messaggio generale</a>
                            </li>
                            <li>
                                <a href="./messaggio.php?dest=Utenti">Messaggio a utenti</a>
                            </li>
                            <li>
                                <a href="./messaggio.php?dest=Creatori">Messaggio a creatori</a>
                            </li>
                        </ul>
                    </li>
                    <?php break;

                    default:?>

                <?php endswitch ?>

                <li>
                    <a href="gestione_messaggi.php?pagina=ricevuti">Messaggi</a>
                </li>

                <li class="sideButton mt-3">
                    <a href="./logout.php" class="orangeBtn">LogOut</a> 
                </li>
                <?php endif ?>
            </ul>
        </nav>
        
        <!-- Notifiche  -->
        <nav class = "bg-light" id="menuNotifiche">
            <div class="row no-gutters sidebar-header">
                <div class="text-light col-10">
                <h2 class="h3">Notifiche!</h2>
                </div>
                <div class="col-2" id="dismiss-not">
                    <em class="fas fa-arrow-left" aria-hidden="true" title="Esci dal menù notifiche"></em>
                </div>
            </div>
                  
            <ul class="list-unstyled CTAs">
                <li id="nmessaggi">
                </li>
                <li id="npromemoria">
                </li>

                <li class="sideButton mt-3">
                    <a href="gestione_messaggi.php?pagina=ricevuti" class="orangeBtn">Vedi tutte le notifiche</a> 
                </li>
            </ul>
        </nav>

        <!-- Page Content  -->
        <div class="container-fluid" id="content">
            <!-- Upper navigation menu -->
            <nav class="navbar navbar-expand-lg navbar-light justify-content-between w-100" style="z-index:10;">
                <?php if($templateParams["nbMenu"]):?> 
                <div class = "row">
                    <div class="col-3 col-md-1">
                        <button type="button" id="sidebarCollapse" class="btn">
                            <em class="fas fa-align-left" aria-hidden="true" title="Apri il menù laterale"></em>
                        </button>
                    </div>
                    <div class="col col-md-3 col-xl-2 alignLeft">
                        <a href="./index.php"><img src="upload/logo.jpg" id="logo" class="img-fluid" alt="Logo del sito Pasuta.it"></a>
                    </div>

                    <!-- browser -->
                    <?php if ($templateParams["nbSearch"]) :?> 
                    <div class="col-md-6 d-none d-md-block">
                        <form action="risultato_ricerca.php">
                            <div class="row no-gutters align-items-center">
                                <label aria-hidden="false" style="display: none" for="ricerca1">Ricerca eventi</label>
                                <div class="col">
                                    <input class="form-control" id="ricerca1" type="search" placeholder="Cerca..." aria-label="Ricerca eventi" name="stringa_ricerca" <?php if(isset($templateParams["stringa_ricerca"]) && $templateParams["stringa_ricerca"]!= "") echo "value=".$templateParams['stringa_ricerca'];?> />
                                </div>
                                <!--end of col-->
                                <div class="col-auto">
                                    <button class="btn" type="submit">
                                        <em class="fas fa-search" aria-hidden="true" title="Cerca"></em>
                                    </button>
                                </div>
                                <!--end of col-->
                            </div>
                        </form>
                    </div>

                    <?php elseif (isset($_SESSION['logtype'])&&($_SESSION['logtype']) == "admin"):?>
                    <div class="col-md-7 col-xl-6 d-none d-md-block"></div>
                    <div class="col-xl-2 d-none d-xl-block">
                        <div class="d-inline-block">
                            <button class="btn" type="button" id="gestisciButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <em class="fas fa-calendar-alt" aria-hidden="true" title="Gestisci"></em> Gestisci
                            </button>
                            <div class="dropdown-menu" aria-labelledby="gestisciButton">
                                <a class="dropdown-item" href="./gestione_richieste.php?ord=dataAsc">Richieste creatori</a>
                                <a class="dropdown-item" href="./gestione_registrati.php?tipo=tutti&visualizza=tutti&ordinamento=usrAsc">Utenti e creatori</a>
                                <a class="dropdown-item" href="./gestione_eventi.php?tipo=tutti&ordinamento=nomeAsc">Eventi</a>
                            </div>
                        </div>
                        <div class="d-inline-block">
                            <button class="btn" type="button" id="inviaButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <em class="fas fa-comment-dots" aria-hidden="true" title="Invia"></em> Invia
                            </button>
                            <div class="dropdown-menu" aria-labelledby="inviaButton">
                                <a class="dropdown-item" href="./messaggio.php?dest=Tutti">Messaggio generale</a>
                                <a class="dropdown-item" href="./messaggio.php?dest=Utenti">Messagio a utenti</a>
                                <a class="dropdown-item" href="./messaggio.php?dest=Creatori">Messaggio a creatori</a>
                            </div>
                        </div>       
                    </div>
                    <?php endif;?>
                    <?php if (!isset($_SESSION['logtype'])) :?>
                    <div class="col-xl-1 d-none d-xl-block"></div>
                    <div class="col-xl-1 d-none d-xl-block alignCenter">
                        <button class="btn" type="button" id="registratiButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <em class="fas fa-user-edit" aria-hidden="true" title="Registrati"></em> Registrati
                        </button>
                        <div class="dropdown-menu" aria-labelledby="registratiButton">
                            <a class="dropdown-item" href="./signup.php?tipo_signup=utente">Come utente</a>
                            <a class="dropdown-item" href="./signup.php?tipo_signup=creatore">Come creatore</a>
                        </div>
                    </div>
                    <div class="col-xl-1 d-none d-xl-block alignRight">
                        <button onclick="location.href='./login.php'" class="btn">
                            <em class="fas fa-user-circle" aria-hidden="true" title="Accedi"></em> Accedi
                        </button>
                    </div>
                    <?php endif;?> 
                    <!-- end browser -->

                    <?php if((isset($_SESSION['logtype'])) && (($_SESSION['logtype']) == "admin")):?>
                        <div class="col-4 col-md-1 alignRight d-inline-block">
                    <?php else:?>
                        <div class="col-4 col-md-2 col-lg-2 col-xl-3 alignRight d-inline-block">
                    <?php endif;
                    if($templateParams["nbBack"] && !$templateParams["nbSearch"]):?> 
                        <button onclick="history.back();" type="button" class="btn d-md-none">
                            <em class="fas fa-arrow-left" aria-hidden="true" title="Torna alla pagina precedente"></em>
                        </button>
                        <?php endif ?> 
                        <?php if($templateParams["nbAlarm"]):?>
                        <span class="badge badge-danger" id="notificationNumber"></span>
                        <button type="button" id="menuNotificheCollapse" class="btn">
                            <em class="fas fa-bell" aria-hidden="true" title="Apertura menu notifiche"></em>
                        </button>
                        <?php endif ?> 
                        <?php if($templateParams["nbCart"] && (isset($_SESSION['logtype'])&&($_SESSION['logtype']) == "utente")):?> 
                        <button type="button" class="btn" onclick="location.href='carrello.php'" id="btnchart">
                            <em class="fas fa-shopping-cart" aria-hidden="true" title="Apertura pagina del carrello"></em>
                        </button>
                        <span class="badge badge-danger" id="chartNumber"></span>
                        <?php endif ?> 
                    </div>
                </div>
                
                <!-- mobile -->    
                <div class="d-md-none mt-2">
                    <div class = "row no-gutters align-ite ms-center">
                    <?php if($templateParams["nbBack"] && $templateParams["nbSearch"]):?>
                    <div class="col-auto">  
                        <button onclick="history.back();" type="button" class="btn">
                            <em class="fas fa-arrow-left" aria-hidden="true" title="Torna alla pagina precedente    "></em>
                        </button>
                    </div>
                    <?php endif;?>
                    <div class="col">
                    <?php if ($templateParams["nbSearch"]) :?> 
                        <form action="risultato_ricerca.php">
                            <div class="row no-gutters align-items-center">
                                <label aria-hidden="false" style="display: none" for="ricerca2">Ricerca eventi</label>
                                <div class="col">
                                    <input class="form-control" id="ricerca2" type="search" placeholder="Cerca..." aria-label="Ricerca eventi" name="stringa_ricerca" <?php if(isset($templateParams["stringa_ricerca"]) && $templateParams["stringa_ricerca"]!= "") echo "value=".$templateParams['stringa_ricerca'];?> />
                                </div>
                                <!--end of col-->
                                <div class="col-auto">
                                    <button class="btn" type="submit">
                                        <em class="fas fa-search"  aria-hidden="true" title="Cerca"></em>
                                    </button>
                                </div>
                                <!--end of col-->
                            </div>
                        </form>
                    <?php endif;?>
                    </div>                        
                    <!--end of col-->
                </div>

                <?php elseif(!$templateParams["nbMenu"]): ?> 
                <div class = "row">
                    <div class="col-3 col-md-0 d-md-none">
                        <?php if($templateParams["nbBack"]): ?> 
                        <button onclick="history.back();" type="button" class="btn">
                                <em class="fas fa-arrow-left" aria-hidden="true" title="Torna alla pagina precedente"></em>
                        </button>
                        <?php endif ?> 
                    </div>

                    <div class="col-6 col-md-12 alignCenter">
                        <a href="./index.php"><img src="upload/logo.jpg" id="logo" class="img-fluid" alt="Logo del sito Pasuta.it" ></a>
                    </div>

                    <div class="col-3">
                    </div>
                </div>
                <?php endif ?>
            </nav>
            
            <main>
                    <?php
                    if(isset($templateParams["nome"])){
                        require($templateParams["nome"]);
                    }
                    ?>
            </main>

            <footer class= "mt-3 w-100">
                <div>
                    <span>Pasuta.it, l'evento che vuoi eccolo qui!</span>
                </div>
                <div>
                    <a href="./privacy.php">Privacy policy</a>
                </div>
                <div>
                    <span>Ameri Keivan - Antonio delle Canne</span>
                </div>
            </footer>
        </div>
    </div>
    <div class="overlay"></div>

</body>
</html>