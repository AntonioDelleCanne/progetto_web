<?php if(!empty($templateParams["eventicarrello"])): ?>
    <div class="event p-3 mb-3 mt-2 bg-light border border-dark rounded ">
        <div class="row row no-gutters">
            <div class="col-6">
                <p class="thick-text" id="narticoli"></p>
            </div>
            <div class="col-6">
                <p class="thick-text" id="totale"></p>
            </div>
        </div>
        <div class="row row no-gutters">
            <div class="col-12">
                <button class="btn btn-block rounded orange-button" onclick="location.href='pagamento.php'" type="button">Procedi all'ordine</button>
            </div>
        </div>
    </div>
<?php endif?>

<h2 class=" mt-2 row col-12">Articoli nel carrello</h2>
<div class="line row col-12"></div>
<?php if(empty($templateParams["eventicarrello"])){
    echo "Nessun articolo nel carrello";
} else {
    foreach($templateParams["eventicarrello"] as $evento){
        if(isset($templateParams["evento"])){
            require($templateParams["evento"]);
        }
    } 
}?>
