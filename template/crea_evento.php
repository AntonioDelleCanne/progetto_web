<?php if(isset($templateParams["evento"])) $eventoAperto = $templateParams["evento"]?>
<div class="container ">
    <div class="justify-content-center row no-gutters ">
        <form class="form-signin" action="<?php echo isset($eventoAperto) ? "modifica_evento.php" : "conferma_creazione.php"?>" method="POST" enctype="multipart/form-data">
            <div class="">
                <h1 class="col-10 h3 mb-3 font-weight-normal">Crea Evento</h1>
            </div>

            <?php if(isset($eventoAperto)):?>
            <input type="hidden" id="id-evento" name="id-evento" value="<?php echo $eventoAperto["idEvento"]?>"></input>
            <?php endif?>
    
            <div class="form-group row ">
                <div class="col-12 col-md-4">
                    <label for="inputNome">Nome Evento</label>
                </div>
                <div class="col-md-8 col-12">
                        <input id="inputNome" name="nome-evento" class="form-control" placeholder="Nome evento" required="" autofocus="" <?php if(isset($eventoAperto)) echo 'value="'.$eventoAperto["NomeEvento"].'"' ?>></input>
                </div>
            </div>
    
            <div class="form-group row ">
                <div class="col-12 col-md-4">
                    <label for="inputDataInizio">Inizio Evento</label>
                </div>
                <div class="col-md-3 col-auto">
                    <label  aria-hidden="false" style="display: none" for="inputOraInizio">Ora inizio Evento</label>
                    <input type="time" id="inputOraInizio" name="ora-inizio" class="form-control " required="" autofocus="" <?php if(isset($eventoAperto)) echo 'value="'.substr($eventoAperto["OraInizio"], 0, -3).'"' ?>></input>
                </div>
                <div class="col-md-5 col">
                    <input type="date" id="inputDataInizio" name="data-inizio" class="form-control" required="" autofocus="" <?php if(isset($eventoAperto)) echo 'value="'.$eventoAperto["DataInizio"].'"' ?>></input>
                </div>
            </div>
    
            <div class="form-group row ">
                <div class="col-md-4 col-12">
                    <label for="inputDataFine">Fine Evento</label>
                </div>
                <div class="col-md-3 col-auto">
                    <label aria-hidden="false" style="display: none" for="inputOraFine">Ora fine Evento</label>
                    <input type="time" id="inputOraFine" name="ora-fine" class="form-control " required="" autofocus="" <?php if(isset($eventoAperto)) echo 'value="'.substr($eventoAperto["OraFine"], 0, -3).'"' ?>>
                </div>
                <div class="col-md-5 col">
                    <input type="date" id="inputDataFine" name="data-fine" class="form-control" required="" autofocus="" <?php if(isset($eventoAperto)) echo 'value="'.$eventoAperto["DataFine"].'"' ?>>
                </div>
            </div>
    
            <div class="form-group row ">
                <div class="col-md-3 col-12">
                    <label for="inputCosto">Costo</label>
                </div>
                <div class="col-md-1 col-auto ">
                    <label aria-hidden="false" style="display: none" for="checkCosto">Checkbox costo</label>
                    <input type="checkbox" id="checkCosto" data-rif="inputCosto" onchange="deactivateField(this)" <?php if(isset($eventoAperto) && !isset($eventoAperto["Prezzo"])) echo "checked" ?>></input>
                </div>
                <div class="col-md-8 col">
                    <input id="inputCosto" name="prezzo" class="form-control" placeholder="€" required="" autofocus="" <?php if(isset($eventoAperto) && isset($eventoAperto["Capienza"])) echo 'value="'.$eventoAperto["Prezzo"].'"' ?>>
                </div>
            </div>
            
            <div class="form-group row ">
                <div class="col-md-3 col-12">
                    <label for="inputCapienza">Capienza</label>
                </div>
                <div class="col-md-1 col-auto">
                    <label aria-hidden="false" style="display: none" for="checkCapienza">Checkbox capienza</label>
                    <input type="checkbox" id="checkCapienza" data-rif="inputCapienza" onchange="deactivateField(this)" <?php if(isset($eventoAperto) && !isset($eventoAperto["Capienza"])) echo "checked" ?>></input>
                </div>
                <div class="col-md-8 col">
                    <input id="inputCapienza" name="capienza" class="form-control" placeholder="Posti" required="" autofocus="" <?php if(isset($eventoAperto) && isset($eventoAperto["Capienza"])) echo 'value="'.$eventoAperto["Capienza"].'"' ?>>
                </div>
            </div>
    
            <div class="form-group row ">
                <div class="col-md-4 col-12">
                    <label for="inputLuogo">Luogo</label>
                </div>
                <div class="col-md-8 col-12">
                    <input id="inputLuogo" name="luogo" class="form-control" placeholder="Luogo" required="" autofocus="" <?php if(isset($eventoAperto)) echo 'value="'.$eventoAperto["NomeLuogo"].'"' ?>>
                </div>
            </div>
    
            <label>Indirizzo</label>
            <div class="p-3 border rounded row no-gutters">
                <div class="form-group col-6 col-md-2">
                    <label for="inputVia">Via</label>
                </div>
                <div class="form-group col-6 col-md-3">
                    <input id="inputVia" name="via" class="form-control" placeholder="Via" required="" autofocus="" <?php if(isset($eventoAperto)) echo 'value="'.$eventoAperto["Via"].'"' ?>>
                </div>
                <div class="col-md-2"></div>

                <div class="form-group col-6 col-md-2">
                    <label for="inputN">N. civico</label>
                </div>
                <div class="form-group col-6 col-md-3">
                    <input id="inputN" name="n" class="form-control" placeholder="Numero" required="" autofocus="" <?php if(isset($eventoAperto)) echo 'value="'.$eventoAperto["N"].'"' ?>>
                </div>
                <div class="form-group col-6 col-md-2">
                    <label for="inputCitta">Città</label>
                </div>
                <div class="form-group col-6 col-md-3">
                    <input id="inputCitta" name="citta" class="form-control" placeholder="Città" required="" autofocus="" <?php if(isset($eventoAperto)) echo 'value="'.$eventoAperto["Citta"].'"' ?>>
                </div>
                <div class="col-md-2"></div>
                <div class="form-group col-6 col-md-2">
                    <label for="inputCAP">CAP</label>
                </div>
                <div class="form-group col-6 col-md-3">
                    <input id="inputCAP" name="cap" class="form-control" placeholder="CAP" required="" autofocus="" <?php if(isset($eventoAperto)) echo 'value="'.$eventoAperto["CAP"].'"' ?>>
                </div>
                <div class="form-group col-6 col-md-2">
                    <label for="inputProvincia">Provincia</label>
                </div>
                <div class="form-group col-6 col-md-3">
                    <input id="inputProvincia" name="provincia" class="form-control" placeholder="Provincia" required="" autofocus="" <?php if(isset($eventoAperto)) echo 'value="'.$eventoAperto["Provincia"].'"' ?>>
                </div>
                <div class="col-md-2"></div>
                <div class="form-group col-6 col-md-2">
                    <label for="inputStato">Stato</label>
                </div>
                <div class="form-group col-6 col-md-3">
                    <input id="inputStato" name="stato" class="form-control" placeholder="Stato" required="" autofocus="" <?php if(isset($eventoAperto)) echo 'value="'.$eventoAperto["Stato"].'"' ?>>
                </div>
            </div>
    
            <div class="form-group row mt-3 ">
                <label for="descrizione">Descrizione Evento</label>
                <textarea class="form-control rounded-0" id="descrizione" name="descrizione" rows="3"><?php if(isset($eventoAperto)) echo $eventoAperto["Descrizione"] ?></textarea>
            </div>
            <div class="form-group row mt-3 ">
                <label for="immagine"><em class="fas fa-image" aria-hidden="true"></em> Immagine</label>
                <input type="file" id="immagine" name="fileToUpload" class="" autofocus="" <?php if(!isset($eventoAperto)) echo 'required=""'?>>
            </div>
            <div class = "mt-4">
                <button class="btn btn-block rounded orange-button" type="submit"><?php echo isset($eventoAperto) ? "Conferma Modifiche" : "Crea Evento"?></button> 
            </div>
            
        </form>
    </div>
</div>
<?php if(isset($eventoAperto) && !isset($eventoAperto["Prezzo"])) echo '<script type="text/javascript">deactivateField(document.getElementById("checkCosto"))</script>'?>
<?php if(isset($eventoAperto) && !isset($eventoAperto["Capienza"])) echo '<script type="text/javascript">deactivateField(document.getElementById("checkCapienza"))</script>'?>
