<h2>Eventi acquistati <?php echo $_GET["tipo"]?></h2>
<?php if(empty($templateParams["eventi"]) && !isset($_GET["stringa_filtro"])) :
    echo "Non ci sono eventi acquistati ".$_GET["tipo"];
else: ?>
    <form action="./acquisti_particolare.php" method="GET">
    <label aria-hidden="false" style="display: none" for="ricerca">Ricerca eventi acquistati</label>
    <input type="hidden" name="ordinamento" value="<?php echo $templateParams["ordinamento"] ?>"/>
    <input type="hidden" name="tipo" value="<?php echo $templateParams["tipo"] ?>"/>
    <div class="row no-gutters align-items-center">
        <div class="col">
            <input class="form-control" type="search" id="ricerca" placeholder="Cerca..." name="stringa_filtro" <?php if(isset($templateParams["stringa_filtro"]) && $templateParams["stringa_filtro"]!= "") echo "value=".$templateParams['stringa_filtro'];?> />
        </div>
        <!--end of col-->
        <div class="col-auto">
            <button class="btn" type="submit">
                <em class="fas fa-search"></em>
            </button>
        </div>
        <!--end of col-->
    </div>
    </form>
    <div class="d-inline-block">
        <button class="btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Ordina: <?php if($_GET["ordinamento"] == "nomeDesc") {
                echo "nome evento decrescente";
            } elseif ($_GET["ordinamento"] == "nomeAsc"){
                echo "nome evento crescente";
            } elseif (($_GET["ordinamento"] == "dataAsc" && $_GET["tipo"] != "passati") || ($_GET["ordinamento"] == "dataDesc" && $_GET["tipo"] == "passati")){
                echo "Data inizio evento più vicina";
            } elseif (($_GET["ordinamento"] == "dataDesc" && $_GET["tipo"] != "passati") || ($_GET["ordinamento"] == "dataAsc" && $_GET["tipo"] == "passati")){
                echo "Data inizio evento più lontana";
            } elseif (($_GET["ordinamento"] == "acqAsc" && $_GET["tipo"] != "passati") || ($_GET["ordinamento"] == "acqDesc" && $_GET["tipo"] == "passati")){
                echo "Data acquisto più vicina";
            } elseif (($_GET["ordinamento"] == "acqDesc" && $_GET["tipo"] != "passati") || ($_GET["ordinamento"] == "acqAsc" && $_GET["tipo"] == "passati")){
                echo "Data acquisto più lontana";
            }?> <em class="fas fa-caret-down" aria-hidden="true"></em>
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">    
            <a class="dropdown-item" href="<?php echo "./acquisti_particolare.php?tipo=".$_GET["tipo"]."&".($_GET["tipo"]!="passati" ? "ordinamento=dataAsc" : "ordinamento=dataDesc")?>">Prima data più vicina</a>
            <a class="dropdown-item" href="<?php echo "./acquisti_particolare.php?tipo=".$_GET["tipo"]."&".($_GET["tipo"]!="passati" ? "ordinamento=dataDesc" : "ordinamento=dataAsc")?>">Prima data più lontana</a>
            <a class="dropdown-item" href="<?php echo "./acquisti_particolare.php?ordinamento=nomeAsc&tipo=".$_GET["tipo"]?>">Nome Crescente</a>
            <a class="dropdown-item" href="<?php echo "./acquisti_particolare.php?ordinamento=nomeDesc&tipo=".$_GET["tipo"]?>">Nome Decrescente</a>
            <a class="dropdown-item" href="<?php echo "./acquisti_particolare.php?tipo=".$_GET["tipo"]."&".($_GET["tipo"]!="passati" ? "ordinamento=acqAsc" : "ordinamento=acqDesc")?>">Data acquisto più vicina</a>
            <a class="dropdown-item" href="<?php echo "./acquisti_particolare.php?tipo=".$_GET["tipo"]."&".($_GET["tipo"]!="passati" ? "ordinamento=acqDesc" : "ordinamento=acqAsc")?>">Data acquisto più lontana</a>
        </div> 
    </div> 
    <div class="line"></div>
    <?php foreach($templateParams["eventi"] as $evento) {
        if(isset($templateParams["evento"])) {
            require($templateParams["evento"]);
        }
    }
endif ?>