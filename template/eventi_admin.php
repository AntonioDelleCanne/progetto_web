<h2>Gestisci richieste creatori</h2>
<form action="./gestione_eventi.php" method="GET">
    <input type="hidden" name="ordinamento" value="<?php echo $templateParams["ordinamento"] ?>"/>
    <input type="hidden" name="tipo" value="<?php echo $templateParams["tipo"] ?>"/>
    <div class="row no-gutters align-items-center">
        <div class="col">
            <label aria-hidden="false" style="display: none" for="ricerca">Ricerca eventi acquistati</label>
            <input class="form-control" id="ricerca" type="search" placeholder="Cerca..." name="stringa_filtro" <?php if(isset($templateParams["stringa_filtro"]) && $templateParams["stringa_filtro"]!= "") echo "value=".$templateParams['stringa_filtro'];?> />
        </div>
        <!--end of col-->
        <div class="col-auto">
            <button class="btn" type="submit">
                <em class="fas fa-search"  aria-hidden="true" title="Ricerca tra le richieste creatori"></em>
            </button>
        </div>
        <!--end of col-->
    </div>
</form>
<div class="d-inline-block">
    <button class="btn" type="button" id="dropdownMenuButtonOrd" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Ordina: <?php echo ($_GET["ordinamento"]=="nomeAsc" ? "nome crescente" : ($_GET["ordinamento"]=="nomeDesc" ? "nome decrescente" : ($_GET["ordinamento"]=="dataAsc" ? "data inizio minore" : "data più lontana")))?> <em class="fas fa-caret-down" aria-hidden="true"></em>
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButtonOrd">
        <a class="dropdown-item" href="<?php echo "./gestione_eventi.php?tipo=".$_GET["tipo"]."&ordinamento=nomeDesc"?><?php echo ((isset($_GET["type"]) && $_GET["type"] != 0) ? "&type=1&username=".$_GET["username"] : "&type=0")?><?php if(isset($templateParams["stringa_filtro"])) echo "&stringa_filtro=".$templateParams["stringa_filtro"]?>">Nome Decrescente</a>
        <a class="dropdown-item" href="<?php echo "./gestione_eventi.php?tipo=".$_GET["tipo"]."&ordinamento=nomeAsc"?><?php echo ((isset($_GET["type"]) && $_GET["type"] != 0) ? "&type=1&username=".$_GET["username"] : "&type=0")?><?php if(isset($templateParams["stringa_filtro"])) echo "&stringa_filtro=".$templateParams["stringa_filtro"]?>">Nome Crescente</a>
        <a class="dropdown-item" href="<?php echo "./gestione_eventi.php?tipo=".$_GET["tipo"]."&ordinamento=dataAsc"?><?php echo ((isset($_GET["type"]) && $_GET["type"] != 0) ? "&type=1&username=".$_GET["username"] : "&type=0")?><?php if(isset($templateParams["stringa_filtro"])) echo "&stringa_filtro=".$templateParams["stringa_filtro"]?>">Data inizio minore</a>
        <a class="dropdown-item" href="<?php echo "./gestione_eventi.php?tipo=".$_GET["tipo"]."&ordinamento=dataDesc"?><?php echo ((isset($_GET["type"]) && $_GET["type"] != 0) ? "&type=1&username=".$_GET["username"] : "&type=0")?><?php if(isset($templateParams["stringa_filtro"])) echo "&stringa_filtro=".$templateParams["stringa_filtro"]?>">Data inizio più lontana</a>
    </div> 
</div> 
<div class="d-inline-block">
    <button class="btn" type="button" id="dropdownMenuButtonTipo" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Tipologia: <?php echo ($_GET["tipo"]=="tutti" ? "tutti" : ($_GET["tipo"]=="futuri" ? "eventi futuri" : ($_GET["tipo"]=="corso" ? "eventi in corso" : ($_GET["tipo"]=="passati" ? "eventi passati" : "eventi cancellati"))))?> <em class="fas fa-caret-down" aria-hidden="true"></em>
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButtonTipo">
        <a class="dropdown-item" href="<?php echo "./gestione_eventi.php?tipo=tutti&ordinamento=".$_GET["ordinamento"]?><?php echo ((isset($_GET["type"]) && $_GET["type"] != 0) ? "&type=1&username=".$_GET["username"] : "&type=0")?><?php if(isset($templateParams["stringa_filtro"])) echo "&stringa_filtro=".$templateParams["stringa_filtro"]?>">Tutti</a>
        <a class="dropdown-item" href="<?php echo "./gestione_eventi.php?tipo=futuri&ordinamento=".$_GET["ordinamento"]?><?php echo ((isset($_GET["type"]) && $_GET["type"] != 0) ? "&type=1&username=".$_GET["username"] : "&type=0")?><?php if(isset($templateParams["stringa_filtro"])) echo "&stringa_filtro=".$templateParams["stringa_filtro"]?>">Futuri</a>
        <a class="dropdown-item" href="<?php echo "./gestione_eventi.php?tipo=corso&ordinamento=".$_GET["ordinamento"]?><?php echo ((isset($_GET["type"]) && $_GET["type"] != 0) ? "&type=1&username=".$_GET["username"] : "&type=0")?><?php if(isset($templateParams["stringa_filtro"])) echo "&stringa_filtro=".$templateParams["stringa_filtro"]?>">In corso</a>
        <a class="dropdown-item" href="<?php echo "./gestione_eventi.php?tipo=passati&ordinamento=".$_GET["ordinamento"]?><?php echo ((isset($_GET["type"]) && $_GET["type"] != 0) ? "&type=1&username=".$_GET["username"] : "&type=0")?><?php if(isset($templateParams["stringa_filtro"])) echo "&stringa_filtro=".$templateParams["stringa_filtro"]?>">Passati</a>
        <a class="dropdown-item" href="<?php echo "./gestione_eventi.php?tipo=eliminati&ordinamento=".$_GET["ordinamento"]?><?php echo ((isset($_GET["type"]) && $_GET["type"] != 0) ? "&type=1&username=".$_GET["username"] : "&type=0")?><?php if(isset($templateParams["stringa_filtro"])) echo "&stringa_filtro=".$templateParams["stringa_filtro"]?>">Cancellati</a>
    </div> 
</div> 
<div class="line"></div>
<?php if(empty($templateParams["eventi"])) :
    echo "Nessun risultato.";
endif; ?>
<?php foreach($templateParams["eventi"] as $evento) {
    if(isset($templateParams["evento"])) {
        require($templateParams["evento"]);
    }
}