<h2 class="">Eventi osservati in corso e futuri</h2>
<?php if(!empty($templateParams["eventiFuturi"])):?>
<div class="alignRight">
    <a href="./interessati_particolare.php?tipo=futuri&ordinamento=dataAsc">Vedi tutti</a>
</div>
<?php endif ?>
<div class="line"></div>
<?php if(empty($templateParams["eventiFuturi"])){
    echo "Nessun evento futuro interessato";
} else {
    foreach($templateParams["eventiFuturi"] as $evento){
        if(isset($templateParams["evento"])){
            require($templateParams["evento"]);
        }
    } 
}?>

<h2 class=" mt-5">Eventi osservati passati</h2>
<?php if(!empty($templateParams["eventiPassati"])):?>
<div class="alignRight">
    <a href="./interessati_particolare.php?tipo=passati&ordinamento=dataDesc">Vedi tutti</a>
</div>
<?php endif ?>
<div class="line"></div>
<?php if(empty($templateParams["eventiPassati"])){
    echo "Nessun evento passato interessato";
} else {
    foreach($templateParams["eventiPassati"] as $evento){
        if(isset($templateParams["evento"])){
            require($templateParams["evento"]);
        }
    }   
}?>

<?php if(!empty($templateParams["eventiEliminati"])):?>
<h2 class=" mt-5">Eventi osservati cancellati</h2>
<div class="alignRight">
    <a href="./interessati_particolare.php?tipo=eliminati&ordinamento=dataAsc">Vedi tutti</a>
</div>
<div class="line"></div>
    <?php foreach($templateParams["eventiEliminati"] as $evento){
        if(isset($templateParams["evento"])){
            require($templateParams["evento"]);
        }
    }   
endif; ?>
