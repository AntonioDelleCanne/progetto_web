<?php $eventoAperto = $templateParams["evento"]?>
<div class="row no-gutters">
    <div class="col-12 mb-2">
        <div class="row no-gutters">
            <div class="col-12 col-sm-3">
                <img src="<?php echo UPLOAD_DIR.$eventoAperto["Immagine"];?>" class="img-fluid rounded img-evento" alt="Immagine dell'evento">
            </div>
            <div class="col-12 col-sm-9 event p-3 bg-light border border-dark rounded" data-idevento = "<?php echo $eventoAperto["idEvento"];?>">
                <div class="row">
                    <div class="col-3 col-sm-3 alignCenter">
                        <h2 class=""><?php echo date("d", strtotime($eventoAperto["DataInizio"]));?></h2>
                        <span class="weightText"><?php echo getSiglaMeseFromNumero(date("m", strtotime($eventoAperto["DataInizio"])));?></span>
                    </div>
                    <div class="col-9 col-sm-8">
                        <h1 class="h5"> <?php echo $eventoAperto["NomeEvento"];?></h1>
                        <div class="row">
                            <div class="col-7 col-sm-10">
                                <span> <?php echo $eventoAperto["NomeLuogo"];?></span>
                            </div>
                            <div class="col-5 col-sm-2">
                                <h2 class=""><?php echo is_null($eventoAperto["Prezzo"]) ? "Gratuito" : (number_format((float)$eventoAperto["Prezzo"], 2, '.', '')."€"); ?></h2>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- browser -->
                <div class="d-none d-sm-block">
                    <div class="line"></div>
                    <div class="pl-5 pt-1">
                        <p class="caption"><em class="fas fa-map-marker-alt"  aria-hidden="true"></em>: <?php echo $dbh->getIndirizzo($_GET['id']);?></p>
                        <p class="caption"><em class="fas fa-calendar-alt"  aria-hidden="true"></em>: <?php echo $eventoAperto["DataInizio"];?> <?php echo number_format((float)$eventoAperto["OraInizio"], 2, ':', '');?>
                        - <?php echo $eventoAperto["DataFine"];?> <?php echo number_format((float)$eventoAperto["OraFine"], 2, ':', '');?></p>
                    </div>
                    <div>
                        <?php if($eventoAperto["Eliminato"] == 0):
                            if($eventoAperto["DataFine"] >= date("Y-m-d") && $eventoAperto["DataInizio"] < date("Y-m-d")):?>
                                <div class = "w-100 alignCenter">
                                    <h3 class="h5">EVENTO IN CORSO</h3>
                                </div>
                                <?php elseif($eventoAperto["DataFine"] < date("Y-m-d")):?>
                                    <div class = "w-100 alignCenter">
                                        <h3 class="h5">EVENTO PASSATO</h3>
                                    </div>
                                    <?php endif;
                        else:?>
                            <div class="event p-3 text-danger border border-danger w-100 font-weight-bold alignCenter">
                                <h3 class="h5">EVENTO CANCELLATO</h3>
                            </div>
                            <?php endif;?>
                        </div>
                    <div class="line mt-3"></div>
                </div>
                <!-- endbrowser -->

                <div class="row mt-3">
                <?php if($eventoAperto["Eliminato"] == 0):
                    if(isset($_SESSION['logtype'])): 
                        switch ($_SESSION["logtype"]):
                            case 'utente':?> 
                                <div class="<?php echo ($eventoAperto["DataInizio"] >= date("Y-m-d") && $eventoAperto["Prezzo"] != 0  ? "col-3" : "col-6")." alignCenter star"?>">
                                    <em class="fas fa-star" data-stato="<?php echo ($dbh->isOsservato($eventoAperto["idEvento"], $_SESSION['username']) ? "acceso" : "spento")?>" aria-hidden="true" title="Aggiunta dell'evento agli interessati" style="cursor:pointer"></em><p class="caption">Mi interessa</p>
                                </div>
                                <?php if($eventoAperto["DataInizio"] >= date("Y-m-d") && $dbh->getDisponibili($eventoAperto["idEvento"]) > 0) :?>
                                    <?php if($eventoAperto["Prezzo"] != 0) :?>
                                    <div class="<?php echo ($eventoAperto["Prezzo"] != 0 ? "col-3":"col-4")." alignCenter"?>">
                                        <a href="aggiungi_al_carrello.php?idEvento=<?php echo $eventoAperto["idEvento"]?>">
                                            <em class="fas fa-cart-arrow-down" aria-hidden="true" title="Carrello"></em><p class="caption">Carrello</p>
                                        </a>
                                    </div>
                                    <div class="col-3 alignCenter">
                                        <a href="acquista_subito.php?idEvento=<?php echo $eventoAperto["idEvento"]?>">
                                            <em class="fas fa-credit-card" aria-hidden="true" title="Acquisto biglietto"></em><p class="caption">Acquista</p>
                                        </a>
                                    </div>
                                    <?php endif;?>
                                <?php endif;?>
                                <div class="<?php echo ($eventoAperto["DataInizio"] >= date("Y-m-d") && $eventoAperto["Prezzo"] != 0  ? "col-3" : "col-6")." alignCenter"?>">
                                    <a href="./messaggio.php?dest=<?php echo $eventoAperto["Username"] ?>">
                                        <em class="fas fa-comment" aria-hidden="true" title="Invio messaggio all'organizzatore"></em><p class="caption">Messaggio</p>   
                                    </a>
                                </div>
                            <?php break;

                            case 'admin':
                            case 'creatore':?>
                                <div class="<?php echo ($eventoAperto["DataInizio"] >= date("Y-m-d") ? "col-4":"col-6")." alignCenter"?>" data-toggle="modal" data-target="#Modal">
                                    <em class="fas fa-trash-alt" aria-hidden="true" title="Eliminazione evento"  style="cursor:pointer"></em><p class="caption">Cancella evento</p>
                                </div>
                                <div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h2 class=" modal-title" id="ModalLabel">Eliminazione evento</h2>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                L'eliminazione dell'evento comporta il rimborso del prezzo del biglietto a chi lo aveva acquistato.<br>
                                                Un evento eliminato non potrà essere riattivato in futuro.
                                                </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-block rounded orange-button" id="eliminaEvento">Elimina</button>
                                                <button type="button" class="btn rounded" data-dismiss="modal">Annulla</button>
                                            </div>      
                                        </div>
                                    </div>
                                </div>

                                <?php if($eventoAperto["DataInizio"] >= date("Y-m-d")) :?>
                                <div class="col-4 alignCenter">
                                    <a href="creazione.php?id=<?php echo $eventoAperto["idEvento"]?>">
                                        <em class="fas fa-pencil-alt" aria-hidden="true" title="Modifica evento"  style="cursor:pointer"></em><p class="caption">Modifica evento</p>
                                    </a>
                                </div>
                                <?php endif;?>
                                <div class="<?php echo ($eventoAperto["DataInizio"] >= date("Y-m-d") ? "col-4":"col-6")." alignCenter"?>">
                                    <a href="./messaggio.php?evento=<?php echo $eventoAperto["idEvento"] ?>">
                                        <em class="fas fa-comment" aria-hidden="true" title="Invio comunicazione agli utenti che osservano l'evento o ne hanno acquistato un biglietto"></em><p class="caption">Invia comunicazione</p>   
                                    </a>
                                </div>
                            <?php break;

                            default:
                        endswitch;
                    else:?>
                        <div class="<?php echo ($eventoAperto["DataInizio"] >= date("Y-m-d") ? "col-3":"col-6")." alignCenter"?>">
                            <a href="./login.php">
                                <em class="fas fa-star" aria-hidden="true" title="Indirizzamento al login"></em><p class="caption">Mi interessa</p>
                            </a>
                        </div>
                        <?php if($eventoAperto["DataInizio"] >= date("Y-m-d")) :?>
                            <div class="col-3 alignCenter">
                                <a href="./login.php">
                                    <em class="fas fa-cart-arrow-down" aria-hidden="true" title="Indirizzamento al login"></em><p class="caption">Carrello</p>
                                </a>
                            </div>
                            <div class="col-3 alignCenter">
                                <a href="./login.php">
                                    <em class="fas fa-credit-card" aria-hidden="true" title="Indirizzamento al login"></em><p class="caption">Acquista</p>
                                </a>
                            </div>
                        <?php endif;?>
                        <div class="<?php echo ($eventoAperto["DataInizio"] >= date("Y-m-d") ? "col-3":"col-6")." alignCenter"?>">
                            <a href="./login.php">
                                <em class="fas fa-comment" aria-hidden="true" title="Indirizzamento al login"></em><p class="caption">Messaggio</p>   
                            </a>
                        </div>
                    <?php endif;

                    if($eventoAperto["DataFine"] >= date("Y-m-d") && $eventoAperto["DataInizio"] < date("Y-m-d")):?>
                        <div class = "w-100 alignCenter d-sm-none">
                            <h3 class="h5">EVENTO IN CORSO</h3>
                        </div>
                    <?php elseif($eventoAperto["DataFine"] < date("Y-m-d")):?>
                        <div class = "w-100 alignCenter d-sm-none">
                            <h3 class="h5">EVENTO PASSATO</h3>
                        </div>
                    <?php endif;

                else:?>
                    <div class="event p-3 text-danger border border-danger w-100 font-weight-bold alignCenter d-sm-none">
                        <h3 class="h5">EVENTO CANCELLATO</h3>
                    </div>
                <?php endif;?>
                </div>
                
                <!-- mobile-->
                <div class="d-sm-none mt-3">
                    <p class="caption"><em class="fas fa-map-marker-alt"  aria-hidden="true"></em>: <?php echo $dbh->getIndirizzo($_GET['id']);?></p>
                    <p class="caption"><em class="fas fa-calendar-alt"  aria-hidden="true"></em>: <?php echo $eventoAperto["DataInizio"];?> <?php echo number_format((float)$eventoAperto["OraInizio"], 2, ':', '');?>
                    - <?php echo $eventoAperto["DataFine"];?> <?php echo number_format((float)$eventoAperto["OraFine"], 2, ':', '');?></p>
                </div>
                <!-- end mobile-->
            </div>
        </div>
    </div>
    
    
    <div class="col-12 event p-3 mb-2 bg-light border border-dark rounded">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-12 col-md-10">
                <h2 class="">Descrizione</h2>
                <p class="small"><?php echo $eventoAperto["Descrizione"];?></p>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>


    <?php if($eventoAperto["Eliminato"] == 0 && $eventoAperto["Prezzo"] != 0):?>
    <div class="col-12 event p-3 mb-2 bg-light border border-dark rounded">   
        <div class="row alignCenter">
            <div class="<?php echo ($eventoAperto["DataFine"] >= date("Y-m-d") ? "col-6 col-md-4":"col-6")?>">
                <p class="caption"><em class="fas fa-users" aria-hidden="true"></em> Partecipanti: <?php echo $dbh->getPartecipanti($eventoAperto["idEvento"]); ?></p>
            </div>
            <div class="<?php echo ($eventoAperto["DataFine"] >= date("Y-m-d") ? "col-6 col-md-4":"col-6")?>">
                <p class="caption" id="ninteressati" data-ninteressati="<?php echo $dbh->getInteressati($eventoAperto["idEvento"]); ?>"><em class="fas fa-eye" aria-hidden="true"></em> Interessati: <?php echo $dbh->getInteressati($eventoAperto["idEvento"]); ?></p>
            </div>
            <?php if($eventoAperto["DataFine"] >= date("Y-m-d")) :?>
            <div class="col-12 col-md-4">
                <p class="caption"><em class="fas fa-ticket-alt" aria-hidden="true"></em> Disponibili: <?php echo $dbh->getDisponibili($eventoAperto["idEvento"]); ?></p>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <?php endif;?>

    <div class="col-12 caption">
        <em class="fas fa-user-alt" aria-hidden="true"></em>Organizzatore: 
        <span class="font-weight-bold"><?php echo $eventoAperto["Username"];?></span>
    </div>
</div>

