<div class="event p-3 mb-2 bg-light border border-dark rounded" data-idevento="<?php echo $evento["idEvento"]; ?>" data-tempoaggiunta="<?php echo $evento["TempoAggiunta"]; ?>">
    <div class="row no-gutters">
        <div class="col-3 col-md-2">
            <a href="./evento.php?id=<?php echo $evento["idEvento"]; ?>">
                <img src="<?php echo UPLOAD_DIR.$evento["Immagine"]; ?>" class="img-fluid rounded-circle img-miniatura" alt="Immagine dell'evento">
            </a>
        </div>

        <!-- mobile -->
        <div class="col-9 d-md-none">
            <h2> <?php echo $evento["NomeEvento"]; ?> </h2>
            <p class="caption"> <?php echo $evento["DataInizio"]; ?> - <?php echo number_format((float)$evento["OraInizio"], 2, ':', ''); ?>
            , <?php echo $evento["DataFine"]; ?>  - <?php echo number_format((float)$evento["OraFine"], 2, ':', ''); ?><br>
            <?php echo $dbh->getIndirizzo($evento["idEvento"]); ?><br/>
            <?php echo $evento["NomeLuogo"]; ?></p>
        </div>

        <!-- browser -->
        <div class="col-md-9 d-none d-md-block alignCenter mb-3">
            <h2> <?php echo $evento["NomeEvento"]; ?> </h2>
            <div class="line"></div>
            <div class="row"> 
                <div class="col-md-4 alignCenter"> 
                    <span class="caption"><span class="font-weight-bold">Data: </span><?php echo $evento["DataInizio"]; ?> - <?php echo number_format((float)$evento["OraInizio"], 2, ':', ''); ?>
                    , <?php echo $evento["DataFine"]; ?>  - <?php echo number_format((float)$evento["OraFine"], 2, ':', ''); ?></span>
                </div>
                <div class="col-md-4 alignCenter">                 
                    <span class="caption"><span class="font-weight-bold">Indirizzo: </span><?php echo $dbh->getIndirizzo($evento["idEvento"]); ?></span>
                </div>
                <div class="col-md-4 alignCenter"> 
                    <span class="caption"><span class="font-weight-bold">Luogo: </span><?php echo $evento["NomeLuogo"];?></span>
                </div>
            </div>
            <div class="line"></div>

            <div class="row">
                <div class="col-md-4 alignCenter">
                    <button type="button" class="btn chart-btn chart-trash">
                        <em class="fas fa-trash-alt" aria-hidden="true" title="Indirizzamento al login"></em>
                    </button>
                    <p class="caption">Rimuovi</p>
                </div>

                <div class="col-md-4 alignCenter">
                    <button type="button" class="btn chart-btn chart-plus">
                        <em class="fas fa-plus" aria-hidden="true" title="Indirizzamento al login"></em>
                    </button>
                    <button type="button" class="btn chart-btn chart-minus">
                        <em class="fas fa-minus" aria-hidden="true" title="Indirizzamento al login"></em>
                    </button>
                    <p class="caption n-biglietti" data-nbiglietti="<?php echo $dbh->getNumeroBigliettiCarrello($_SESSION["username"],$evento["idEvento"]); ?>">Quantità: <?php echo $dbh->getNumeroBigliettiCarrello($_SESSION["username"],$evento["idEvento"]); ?></p>
                </div>

                <div class="col-md-4 alignCenter">
                    <?php echo is_null($evento["Prezzo"]) ? "Gratis" : number_format((float)$evento["Prezzo"], 2, '.', '');?><em class="<?php echo is_null($evento["Prezzo"]) ? "" : "fas fa-euro-sign"?>" aria-hidden="true"></em>
                    <p class="caption timer"></p>
                </div>
            </div>
        </div>
        <!-- end browser -->

        <div class="col-12 d-md-none">
            <div class="row">
                <div class="col-4 alignCenter">
                    <button type="button" class="btn chart-btn chart-trash">
                        <em class="fas fa-trash-alt" aria-hidden="true" title="Indirizzamento al login"></em>
                    </button>
                    <p class="caption">Rimuovi</p>
                </div>

                <div class="col-4 alignCenter">
                    <button type="button" class="btn chart-btn chart-plus">
                        <em class="fas fa-plus" aria-hidden="true" title="Indirizzamento al login"></em>
                    </button><button type="button" class="btn chart-btn chart-minus">
                        <em class="fas fa-minus" aria-hidden="true" title="Indirizzamento al login"></em>
                    </button>
                    <p class="caption n-biglietti" data-nbiglietti="<?php echo $dbh->getNumeroBigliettiCarrello($_SESSION["username"],$evento["idEvento"]); ?>">Quantità: <?php echo $dbh->getNumeroBigliettiCarrello($_SESSION["username"],$evento["idEvento"]); ?></p>
                </div>

                <div class="col-4 alignCenter">
                    <?php echo is_null($evento["Prezzo"]) ? "Gratis" : number_format((float)$evento["Prezzo"], 2, '.', '');?><em class="<?php echo is_null($evento["Prezzo"]) ? "" : "fas fa-euro-sign"?>" aria-hidden="true"></em>
                    <p class="caption timer"></p>
                </div>
            </div> 
        </div>  
    </div>
</div>