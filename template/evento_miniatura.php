<a href="./evento.php?id=<?php echo $evento["idEvento"]; ?>">
    <div class="event p-3 mb-2 bg-light border border-dark rounded">
        <div class="row">
            <div class="col-3 col-md-2">
                <img src="<?php echo UPLOAD_DIR.$evento["Immagine"]; ?>" class="img-fluid rounded-circle img-miniatura" alt="Immagine dell'evento">
            </div>

            <!-- mobile -->
            <div class="col-9 d-md-none">
                <h2 class=""> <?php echo $evento["NomeEvento"]; ?> </h2>
                <p class="caption"> <?php echo $evento["DataInizio"]; ?> - <?php echo number_format((float)$evento["OraInizio"], 2, ':', ''); ?>
                , <?php echo $evento["DataFine"]; ?>  - <?php echo number_format((float)$evento["OraFine"], 2, ':', ''); ?><br>
                <?php echo $dbh->getIndirizzo($evento["idEvento"]); ?><br/>
                <?php echo $evento["NomeLuogo"]; ?></p>
            </div>

            <!-- browser -->
            <div class="col-md-9 d-none d-md-block alignCenter mb-3">
                <h2 class=""> <?php echo $evento["NomeEvento"]; ?> </h2>
                <div class="line"></div>
                <div class="row"> 
                    <div class="col-md-4 alignCenter"> 
                        <span class="caption"><span class="font-weight-bold">Data: </span><?php echo $evento["DataInizio"]; ?> - <?php echo number_format((float)$evento["OraInizio"], 2, ':', ''); ?>
                        , <?php echo $evento["DataFine"]; ?>  - <?php echo number_format((float)$evento["OraFine"], 2, ':', ''); ?></span>
                    </div>
                    <div class="col-md-4 alignCenter">                 
                        <span class="caption"><span class="font-weight-bold">Indirizzo: </span><?php echo $dbh->getIndirizzo($evento["idEvento"]); ?></span>
                    </div>
                    <div class="col-md-4 alignCenter"> 
                        <span class="caption"><span class="font-weight-bold">Luogo: </span><?php echo $evento["NomeLuogo"];?></span>
                    </div>
                </div>
                <div class="line"></div>

                <?php if(isset($evento["DataVendita"])) :?>
                <div class="row">
                    <div class="col-md-4 alignCenter">
                        <p class="caption">Data Acquisto: <?php echo $evento["DataVendita"]; ?></p>
                    </div>
                    <div class="col-md-4 alignCenter">
                        <p class="caption">Biglietti Acquistati: <?php echo $evento["NumeroBiglietti"]; ?></p>
                    </div>
                    <div class="col-md-4 alignCenter">
                        <p class="caption">Totale: <?php echo is_null($evento["Totale"]) ? " Gratuito" : " ".number_format((float)$evento["Totale"], 2, '.', '')."€"; ?></p>
                    </div>
                </div>

                <?php elseif(!isset($evento["DataVendita"]) && $evento["Eliminato"] == 0):?>  
                <div class="row mt-3 ">
                    <?php if($evento["Prezzo"] != 0) :?>
                    <div class="<?php echo ($evento["Prezzo"] != 0 ? ($evento["DataFine"] >= date("Y-m-d") ? "col-md-3 alignCenter":"col-md-4 alignCenter") : "col-md-6 alignCenter")?>">
                        <p class="caption"><em class="fas fa-users" aria-hidden="true"></em> partecipanti: <?php echo $dbh->getPartecipanti($evento["idEvento"]); ?></p>
                    </div>
                    <?php endif;?>

                    <div class="<?php echo ($evento["DataFine"] >= date("Y-m-d") ? "col-md-3 alignCenter":"col-md-4 alignCenter")?>">
                        <p class="caption"><em class="fas fa-eye" aria-hidden="true"></em> interessati: <?php echo $dbh->getInteressati($evento["idEvento"]); ?></p>
                    </div>
                   

                    <?php if($evento["Prezzo"] != 0) :?>
                        <?php if($evento["DataFine"] >= date("Y-m-d")) :?>
                        <div class="col-md-3 alignCenter">
                            <p class="caption"><em class="fas fa-ticket-alt" aria-hidden="true"></em> disponibili: <?php echo $dbh->getDisponibili($evento["idEvento"]); ?></p>
                        </div>
                        <?php endif;?>
                    <?php endif;?>

                    <div class="<?php echo ($evento["Prezzo"] != 0 ? ($evento["DataFine"] >= date("Y-m-d") ? "col-md-3 alignCenter":"col-md-4 alignCenter") : "col-md-6 alignCenter")?>">
                        <p class="caption"><em class="fas fa-euro-sign" aria-hidden="true"></em><?php echo is_null($evento["Prezzo"]) ? " Gratuito" : " ".number_format((float)$evento["Prezzo"], 2, '.', ''); ?></p>
                    </div>
                </div>
                <?php endif;

                if(($evento["Eliminato"] != 0) || ($evento["DataInizio"] < date("Y-m-d") && $evento["DataFine"] >= date("Y-m-d")) || ($evento["DataFine"] < date("Y-m-d"))):?>
                <div class="<?php echo ($evento["Eliminato"] == 0) ? "event w-100 alignCenter" : "event text-danger border border-danger w-100 font-weight-bold alignCenter"?>">
                    <?php if($evento["Eliminato"] != 0):?>
                        <p>EVENTO CANCELLATO</p>
                    <?php elseif($evento["DataInizio"] < date("Y-m-d") && $evento["DataFine"] >= date("Y-m-d")):?>
                        <p>Evento in corso</p>
                    <?php elseif($evento["DataFine"] < date("Y-m-d")):?>
                        <p>Evento concluso</p>
                    <?php endif;?>    
                </div>
                <?php endif;?>
            </div>
            <!-- end browser -->

            <!-- mobile -->
            <?php if(isset($evento["DataVendita"])) :?>
            <div class="line d-md-none"></div>
            <div class="col-12 d-md-none">
                <div class="row">
                    <div class="col-12">
                        <p class="caption"><span class="font-weight-bold"> Data Acquisto: </span><?php echo $evento["DataVendita"]; ?></p>
                    </div>
                    <div class="col-6">
                        <p class="caption"><span class="font-weight-bold"> Biglietti Acquistati: </span><?php echo $evento["NumeroBiglietti"]; ?></p>
                    </div>
                    <div class="col-6">
                        <p class="caption"><span class="font-weight-bold"> Totale: </span><?php echo is_null($evento["Totale"]) ? " Gratuito" : " ".number_format((float)$evento["Totale"], 2, '.', '')."€"; ?></p>
                    </div>
                </div>
            </div>
            <?php elseif(!isset($evento["DataVendita"]) && $evento["Eliminato"] == 0):?>  
            <div class="col-12 d-md-none">
                <div class="row">
                    <?php if($evento["Prezzo"] !=0) :?>
                    <div class="col-6">
                        <p class="caption"><em class="fas fa-users" aria-hidden="true"></em> partecipanti: <?php echo $dbh->getPartecipanti($evento["idEvento"]); ?></p>
                    </div>
                    <?php endif;?>

                    <div class="col-6">
                        <p class="caption"><em class="fas fa-eye" aria-hidden="true"></em> interessati: <?php echo $dbh->getInteressati($evento["idEvento"]); ?></p>
                    </div>

                    <?php if($evento["DataFine"] >= date("Y-m-d") && $evento["Prezzo"] !=0) :?>
                    <div class="col-6">
                        <p class="caption"><em class="fas fa-ticket-alt" aria-hidden="true"></em> disponibili: <?php echo $dbh->getDisponibili($evento["idEvento"]); ?></p>
                    </div>
                    <?php endif;?>

                    <div class="<?php echo ($evento["DataFine"] >= date("Y-m-d") ? "col-6":"col-12")    ?>">
                        <p class="caption"><em class="fas fa-euro-sign" aria-hidden="true"></em><?php echo is_null($evento["Prezzo"]) ? " Gratuito" : " ".number_format((float)$evento["Prezzo"], 2, '.', ''); ?></p>
                    </div>
                </div>
            </div>
            <?php endif;

            if(($evento["Eliminato"] != 0) || ($evento["DataInizio"] < date("Y-m-d") && $evento["DataFine"] >= date("Y-m-d")) || ($evento["DataFine"] < date("Y-m-d"))):?>
            <div class="<?php echo ($evento["Eliminato"] == 0) ? "event w-100 alignCenter d-md-none" : "event text-danger border border-danger w-100 font-weight-bold alignCenter d-md-none"?>">
                <?php if($evento["Eliminato"] != 0):?>
                    <p>EVENTO CANCELLATO</p>
                <?php elseif($evento["DataInizio"] < date("Y-m-d") && $evento["DataFine"] >= date("Y-m-d")):?>
                    <p>Evento in corso</p>
                <?php elseif($evento["DataFine"] < date("Y-m-d")):?>
                    <p>Evento concluso</p>
                <?php endif;?>    
            </div>
            <?php endif;?>
        </div>
    </div>
</a>