<div class="event pr-3 pl-3 bg-light" data-idevento="<?php echo $evento["idEvento"]; ?>" data-tempoaggiunta="<?php echo $evento["TempoAggiunta"]; ?>">
    <div class="row no-gutters">
        <div class="col-12 col-md-2">
            <span class="caption font-weight-bold"> <?php echo $evento["NomeEvento"]; ?> </span>
        </div>
        <div class="col-12 col-md-10 alignCenter mb-3">
            <div class="line d-md-none"></div>
            <div class="row"> 
                <div class="col-12 col-md-6 alignCenter"> 
                    <span class="caption"><span class="font-weight-bold">Data: </span><?php echo $evento["DataInizio"]; ?> - <?php echo number_format((float)$evento["OraInizio"], 2, ':', ''); ?>
                    , <?php echo $evento["DataFine"]; ?>  - <?php echo number_format((float)$evento["OraFine"], 2, ':', ''); ?></span>
                </div>
                <div class="col-6 col-md-3 alignCenter">
                    <span class="caption n-biglietti" data-nbiglietti="<?php echo $dbh->getNumeroBigliettiCarrello($_SESSION["username"],$evento["idEvento"]); ?>"><span class="font-weight-bold">Quantità biglietti: </span><?php echo $dbh->getNumeroBigliettiCarrello($_SESSION["username"],$evento["idEvento"]); ?></span>
                </div>
                <div class="col-6 col-md-3 alignCenter">
                    <span class="caption"><span class="font-weight-bold">Prezzo a biglietto: </span><?php echo is_null($evento["Prezzo"]) ? "Gratis" : number_format((float)$evento["Prezzo"], 2, '.', '');?><em class="<?php echo is_null($evento["Prezzo"]) ? "" : "fas fa-euro-sign"?>" aria-hidden="true"></em></span>
                </div>
            </div>
        </div>  
    </div>
</div>
<div class="line"></div>
<div class="line  d-md-none"></div>
