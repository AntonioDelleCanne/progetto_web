<div class="event p-3 mb-2 mt-2 bg-light border border-dark rounded">
    <a href="./gestione_richieste.php?ord=dataAsc">
        <div class="row">
            <div class="col-3 alignCenter">
                <em class="fas fa-question-circle fa-2x" aria-hidden="true"></em>
            </div>
            <div class="col-9">
                <h2> Gestisci richieste creatori </h2>
            </div>
        </div>
    </a>
</div>
<div class="event p-3 mb-2 mt-2 bg-light border border-dark rounded">
    <a href="./gestione_registrati.php?tipo=tutti&visualizza=tutti&ordinamento=usrAsc">
        <div class="row">
            <div class="col-3 alignCenter">
                <em class="fas fa-user-circle fa-2x" aria-hidden="true"></em>
            </div>
            <div class="col-9">
                <h2> Gestisci utenti e creatori </h2>
            </div>
        </div>
    </a>
</div>
<div class="event p-3 mb-2 mt-2 bg-light border border-dark rounded">
    <a href="gestione_eventi.php?tipo=tutti&ordinamento=nomeAsc">
        <div class="row">
            <div class="col-3 alignCenter">
                <em class="fas fa-calendar-alt fa-2x" aria-hidden="true"></em>
            </div>
            <div class="col-9">
                <h2> Gestisci eventi </h2>
            </div>
        </div>
    </a>
</div>
<div class="event p-3 mb-2 mt-2 bg-light border border-dark rounded">
    <a href="./messaggio.php?dest=Tutti">
        <div class="row">
            <div class="col-3 alignCenter">
                <em class="fas fa-comments fa-2x" aria-hidden="true"></em>
            </div>
            <div class="col-9">
                <h2> Invia messaggio generale </h2>
            </div>
        </div>
    </a>
</div>
<div class="event p-3 mb-2 mt-2 bg-light border border-dark rounded">
    <a href="./messaggio.php?dest=Utenti">
        <div class="row">
            <div class="col-3 alignCenter">
                <em class="fas fa-comment-dots fa-2x" aria-hidden="true"></em>
            </div>
            <div class="col-9">
                <h2> Invia messaggio utenti </h2>
            </div>
        </div>
    </a>
</div>
<div class="event p-3 mb-2 mt-2 bg-light border border-dark rounded">
    <a href="./messaggio.php?dest=Creatori">
        <div class="row">
            <div class="col-3 alignCenter">
                <em class="fas fa-comment-dots fa-2x" aria-hidden="true"></em>
            </div>
            <div class="col-9">
                <h2> Invia messaggio creatori </h2>
            </div>
        </div>
    </a>
</div>