<div class="event p-3 mb-2 mt-3 bg-light border border-dark rounded">
    <a href="creazione.php">
        <div class="row">
            <div class="col-3 col-md-1 alignCenter">
                <em class="fas fa-plus-circle fa-2x " aria-hidden="true"></em>
            </div>
            <div class="col-9 col-md-11 centrato">
                <h2> Crea Nuovo Evento </h2>
            </div>
        </div>
    </a>
</div>

<h2 class="mt-5">Eventi organizzati in corso e futuri</h2>
<?php if(!empty($templateParams["eventiFuturi"])):?>
    <div class="alignRight">
        <a href="./eventi_creatore.php?tipo=futuri&ordinamento=dataAsc">Vedi tutti</a>
    </div>
<?php endif ?>
<div class="line"></div>
<?php if(empty($templateParams["eventiFuturi"])){
    echo "Nessun evento futuro organizzato";
} else {
    foreach($templateParams["eventiFuturi"] as $evento){
        if(isset($templateParams["evento"])){
            require($templateParams["evento"]);
        }
    } 
}?>

<h2 class="mt-5">Eventi organizzati passati</h2>
<?php if(!empty($templateParams["eventiPassati"])):?>
    <div class="alignRight">
        <a href="./eventi_creatore.php?tipo=passati&ordinamento=dataDesc">Vedi tutti</a>
    </div>
<?php endif ?>
<div class="line"></div>
<?php if(empty($templateParams["eventiPassati"])){
    echo "Nessun evento passato acquistato";
} else {
    foreach($templateParams["eventiPassati"] as $evento){
        if(isset($templateParams["evento"])){
            require($templateParams["evento"]);
        }
    }   
}
?>
<?php if(!empty($templateParams["eventiEliminati"])):?>
    <h2 class="mt-5">Eventi organizzati cancellati</h2>
    <div class="alignRight">
        <a href="./eventi_creatore.php?tipo=eliminati&ordinamento=dataDesc">Vedi tutti</a>
    </div>
    <div class="line"></div>
    <?php foreach($templateParams["eventiEliminati"] as $evento){
        if(isset($templateParams["evento"])){
            require($templateParams["evento"]);
        }
    }   
endif; ?>