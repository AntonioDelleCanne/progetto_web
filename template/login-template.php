<div class="container">
    <div class="justify-content-center row no-gutters">
        <form class="form-signin" method="POST" action="index.php">
            <div class="">
                <h1 class="col-10 h3 mb-3 font-weight-normal">Accedi</h1>
            </div>
            <div class="form-group row">
                <div class="col-12 col-md-6">
                    <label for="username" class="col-form-label text-md-right">Nome Utente</label> 
                </div>
                <div class="col-12 col-md-6">
                    <input type="text" id="username" class="form-control" name="username" required>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-12 col-md-6">
                    <label for="password" class="col-form-label text-md-right">Password</label> 
                </div>
                <div class="col-12 col-md-6">
                    <input type="password" id="password" class="form-control" name="password" required>
                </div>
            </div>
            <button class="btn btn-block rounded orange-button" type="submit">Sign in</button>
            
            <div class="text-center">
                <div class="signup-text-area">
                    <p class="mt-5 mb-1">Non sei un utente registrato? Fallo subito!</p>
                    <p class="mt-1 mb-1"><a href="signup.php?tipo_signup=utente">Registrati come utente</a></p>
                </div>
                <div class="signup-text-area">
                    <p class="mt-5 mb-1">Vuoi contribuire a creare eventi? Unisciti a noi!</p>
                    <p class="mt-1 mb-1"><a href="signup.php?tipo_signup=creatore">Registrati come creatore</a></p>
                </div>
            </div>
        </form>
    </div>
</div>