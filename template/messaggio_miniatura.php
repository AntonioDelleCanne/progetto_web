<div class="msg row col-12 p-3 mb-2 bg-light border border-dark rounded" data-messaggio="<?php echo isset($messaggio["IdMessaggio"]) ? $messaggio["IdMessaggio"] : $messaggio["IdPromemoria"]?>" data-visto="<?php if(isset($messaggio["Visto"])) echo $messaggio["Visto"]?>">
    <div class="row col-12 col-md-6 no-gutters">
        <?php if($templateParams["paginaAttiva"] != 'inviati'): ?>
            <div class="col-3 pallino-visto">
                <?php if(isset($messaggio["Visto"])): ?>
                <?php echo $messaggio["Visto"] ? '<em class="far fa-circle" aria-hidden="true" title="Messaggio visto"></em>' : '<em class="fas fa-circle" aria-hidden="true" title="Messaggio non visto"></em>'; ?>
                <?php endif?>
            </div>
            <div class="col-9">
        <?php else :?>
            <div class="col-12">
        <?php endif;?>
            <div class="row col-12"><span class="caption"><span class="font-weight-bold">Ricevuto: </span><?php echo $messaggio["DataInvio"]; ?> </span></div>
            <?php if($templateParams["paginaAttiva"] == 'ricevuti'): ?>
            <div class="row col-12"><span class="caption"><span class="font-weight-bold">Da: </span><?php echo $dbh->contains('amministratore', 'Username', $messaggio["UsernameMittente"], 's') ? 'Amministratore': $messaggio["UsernameMittente"]; ?></span></div>
            <?php endif?>
            <?php if($templateParams["paginaAttiva"] == 'inviati'): ?>
            <div class="row col-12"><span class="caption"><span class="font-weight-bold">A: </span><?php echo multi_implode($dbh->getDestinatari($messaggio["IdMessaggio"]), ";"); ?></span></div>
            <?php endif?>
            <?php if($templateParams["paginaAttiva"] == 'promemoria'): ?>
                <div class="row col-12"><span class="caption"><span class="font-weight-bold">Evento: </span><?php echo $dbh->getEventoById($messaggio["idEvento"])["NomeEvento"]; ?></span></div>
                <div class="line d-none d-md-block"></div>
                <span class="caption text-center d-none d-md-block"><?php echo $messaggio["Testo"]; ?></span>
            <?php endif?>
            <?php if($templateParams["paginaAttiva"] != "promemoria"): ?>
            <div class="row col-12"><span class="caption"><span class="font-weight-bold">Oggetto: </span><?php echo $messaggio["Oggetto"]; ?> </span></div>
            <?php endif?>
        </div>
    </div>
    <?php if($templateParams["paginaAttiva"] == 'promemoria'): ?>
        <div class="line d-md-none"></div>
        <span class="caption text-center d-md-none"><?php echo $messaggio["Testo"]; ?></span>
    <?php endif?>
    <div class="line d-md-none"></div>
    <div class="row col-12 col-md-6 no-gutters">
        <?php if($templateParams["paginaAttiva"] != 'promemoria'): ?>
        <div class="<?php echo (($templateParams["paginaAttiva"]!='inviati') ? "col-4 alignCenter" : "col-6 alignCenter")?>">
            <button type="button" class="btn chart-btn chart-open" onclick='location.href="<?php echo "./messaggio.php?id=".$messaggio["IdMessaggio"]; ?>"'>
                <em class="fas fa-envelope-open"></em><p class="caption">Apri</p>
            </button>
        </div>
        <?php else :?>
        <div class="<?php echo (($templateParams["paginaAttiva"]!='inviati') ? "col-4 alignCenter" : "col-6 alignCenter")?>">
            <button type="button" class="btn chart-btn chart-open" onclick='location.href="<?php echo "./evento.php?id=".$messaggio["idEvento"]; ?>"'>
                <em class="fas fa-calendar-alt"></em><p class="caption">Apri evento</p>
            </button>
        </div>
        <?php endif?>   
        <div class="<?php echo (($templateParams["paginaAttiva"]!='inviati') ? "col-4 alignCenter" : "col-6 alignCenter")?>">
            <button type="button" class="btn chart-btn chart-trash">
                <em class="fas fa-trash-alt"></em><p class="caption">Elimina</p>
            </button>
        </div>
        <?php if($templateParams["paginaAttiva"] != 'inviati'): ?>
        <div class="col-4 alignCenter">
            <button type="button" class="btn chart-btn chart-visto">
                <?php echo $messaggio["Visto"] ? '<em class="far fa-eye-slash"></em>' : '<em class="far fa-eye"></em>'; ?><p class="caption contrassegna-testo"><?php echo $messaggio["Visto"] ? "Non Letto" : "Letto"; ?></p>
            </button>
        </div>
        <?php endif ?>
    </div>
</div>