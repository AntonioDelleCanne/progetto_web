<div class="container margin_60">
    <div class="main_title">
        <h1>Privacy Policy</h1>
    </div>

    <hr>
        <p>Benvenuto sul sito di eventi &nbsp;<strong>Pasuta.it</strong>.
        Quella che segue è la nostra Policy sulla Privacy per assicurarti trasparenza e chiarezza su quanto accade quando visiti questo sito web.</p>
        <p>Ti preghiamo di leggere attentamente la nostra Informativa, che si applica in ogni caso in cui tu acceda al sito web e decida di navigare al suo interno e di utilizzare i suoi servizi.</p>
        <p>&nbsp;</p>
        <h3>Titolare del Trattamento dei Dati</h3>
        <p>Il titolare dei trattamenti dei dati raccolti tramite questo sito è:<br>
        <strong><br>
        Pasuta.it Srl</strong><br>
        con sede legale in Via dell'Università 50, Cesena (FC), ITALIA<br>
        Partita IVA n° IT55555555555555<br>
        (di seguito abbreviato in “<strong>Pasuta.it</strong>“).</p>
        <p>Indirizzo email del Titolare: info@pasuta.it</p>
        <p>&nbsp;</p>
        <h3>Dati personali raccolti e servizi utilizzati</h3>
        <p>Di seguito le tipologie di dati che raccogliamo per ciascuna finalità.</p>
        Dati Personali: Dati di utilizzo</p>
        <h4>Contattare l’utente</h4>
        <p>Newsletter<br>
        Dati Personali: email e nome</p>
        <p>&nbsp;</p>
        <p>Questi in sintesi i servizi che utilizziamo e che tipo di dati raccogliamo per ognuno di essi. Continua a leggere se vuoi conoscere nel dettaglio la Privacy Policy completa.</p>
        <p>&nbsp;</p>
        <h2 style="font-size: 2rem; text-align: center;"><strong>Privacy Policy Completa</strong></h2 style="font-size: 2rem;">
        <p>&nbsp;</p>
        <h2 style="font-size: 2rem;">1. Tipologie di Dati raccolti</h2 style="font-size: 2rem;">
        <p>Fra i Dati Personali raccolti da questa Applicazione, in modo autonomo o tramite terze parti, ci sono: Dati di utilizzo, Email, Nome.</p>
        <p>Dettagli completi su ciascuna tipologia di dati raccolti sono forniti nelle sezioni dedicate di questa privacy policy o mediante specifici testi informativi visualizzati prima della raccolta dei dati stessi.<br>
        I Dati Personali possono essere inseriti volontariamente dall’Utente, oppure raccolti in modo automatico durante l’uso di questa Applicazione.<br>
        L’eventuale utilizzo di Cookie – o di altri strumenti di tracciamento – da parte di questa Applicazione o dei titolari dei servizi terzi utilizzati da questa Applicazione, ove non diversamente precisato, ha la finalità di identificare l’Utente e registrare le relative preferenze per finalità strettamente legate all’erogazione del servizio richiesto dall’Utente.<br>
        Il mancato conferimento da parte dell’Utente di alcuni Dati Personali potrebbe impedire a questa Applicazione di erogare i propri servizi.<br>
        L’Utente si assume la responsabilità dei Dati Personali di terzi pubblicati o condivisi mediante questa Applicazione e garantisce di avere il diritto di comunicarli o diffonderli, liberando il Titolare da qualsiasi responsabilità verso terzi.</p>
        <p>&nbsp;</p>
        <h2 style="font-size: 2rem;">2. Modalità e luogo del trattamento dei Dati raccolti</h2 style="font-size: 2rem;">
        <h3>2.1 Modalità di trattamento</h3>
        <p>Il Titolare tratta i Dati Personali degli Utenti adottando le opportune misure di sicurezza volte ad impedire l’accesso, la divulgazione, la modifica o la distruzione non autorizzate dei Dati Personali.</p>
        <p>Il trattamento viene effettuato mediante strumenti informatici e/o telematici, con modalità organizzative e con logiche strettamente correlate alle finalità indicate. Oltre al Titolare, in alcuni casi, potrebbero avere accesso ai Dati categorie di incaricati coinvolti nell’organizzazione del sito (personale amministrativo, commerciale, marketing, legali, amministratori di sistema) ovvero soggetti esterni (come fornitori di servizi tecnici terzi, corrieri postali, hosting provider, società informatiche, agenzie di comunicazione) nominati anche, se necessario, Responsabili del Trattamento da parte del Titolare. L’elenco aggiornato dei Responsabili potrà sempre essere richiesto al Titolare del Trattamento.</p>
        <h3>2.2 Luogo</h3>
        <p>I Dati sono trattati presso le sedi operative del Titolare ed in ogni altro luogo in cui le parti coinvolte nel trattamento siano localizzate. Per ulteriori informazioni, contatta il Titolare.</p>
        <p>I Dati Personali dell’Utente potrebbero essere trasferiti in un paese diverso da quello in cui l’Utente si trova. Per ottenere ulteriori informazioni sul luogo del trattamento l’Utente può fare riferimento alla sezione relativa ai dettagli sul trattamento dei Dati Personali.</p>
        <p>L’Utente ha diritto a ottenere informazioni in merito alla base giuridica del trasferimento di Dati al di fuori dell’Unione Europea o ad un’organizzazione internazionale di diritto internazionale pubblico o costituita da due o più paesi, nonché in merito alle misure di sicurezza adottate dal Titolare per proteggere i Dati.</p>
        <h3>2.3 Tempi</h3>
        <p>I Dati sono trattati per il tempo necessario allo svolgimento del servizio richiesto dall’Utente, o richiesto dalle finalità descritte in questo documento, e l’Utente può sempre chiedere l’interruzione del Trattamento o la cancellazione dei Dati.</p>
        <p>Quando il trattamento è basato sul consenso dell’Utente, il Titolare può conservare i Dati Personali più a lungo sino a quando detto consenso non venga revocato. Inoltre il Titolare potrebbe essere obbligato a conservare i Dati Personali per un periodo più lungo in ottemperanza ad un obbligo di legge o per ordine di un’autorità.</p>
        <p>Al termine del periodo di conservazioni i Dati Personali saranno cancellati. Pertanto, allo spirare di tale termine il diritto di accesso, cancellazione, rettificazione ed il diritto alla portabilità dei Dati non potranno più essere esercitati.</p>
        <p>&nbsp;</p>
        <h2 style="font-size: 2rem;">3. Finalità del Trattamento dei Dati raccolti</h2 style="font-size: 2rem;">
        <p>I Dati dell’Utente sono raccolti per consentire al Titolare di fornire i propri servizi, così come per le seguenti finalità: Statistica, Pubblicità,&nbsp;Affiliazione commerciale, Contattare l’Utente.</p>
        <p>I Dati Personali sono raccolti per le seguenti finalità ed utilizzando i seguenti servizi:</p>
        <p>&nbsp;</p>
        <h3>3.1 Contattare l’utente</h3>
        <h4>Moduli di contatto</h4>
        <p>L’Utente, compilando con i propri Dati il modulo di contatto, acconsente al loro utilizzo per rispondere alle richieste di informazioni o di qualunque altra natura indicata dall’intestazione del modulo.&nbsp;Dati personali raccolti: Email e Nome.</p>
        <h4>Newsletter</h4>
        <p><em>Dati personali raccolti: Email e Nome.</em></p>
        <p>Con la registrazione alla mailing list o alla newsletter, l’indirizzo email dell’Utente viene automaticamente inserito in una lista di contatti a cui potranno essere trasmessi messaggi email contenenti informazioni, anche di natura commerciale e promozionale, relative a questo sito. L’indirizzo email dell’Utente potrebbe anche essere aggiunto a questa lista come risultato della registrazione a questo sito o dopo aver effettuato un acquisto.</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <h2 style="font-size: 2rem;">4. Diritti dell’Utente</h2 style="font-size: 2rem;">
        <p>Gli Utenti possono esercitare determinati diritti con riferimento ai Dati trattati dal Titolare. In particolare, l’Utente ha il diritto di:</p>
        <ul>
        <li><b>revocare il consenso in ogni momento.</b>&nbsp;L’Utente può revocare il consenso al trattamento dei propri Dati Personali precedentemente espresso.</li>
        <li><b>opporsi al trattamento dei propri Dati.</b>&nbsp;L’Utente può opporsi al trattamento dei propri Dati quando esso avviene su una base giuridica diversa dal consenso. Ulteriori dettagli sul diritto di opposizione sono indicati nella sezione sottostante.</li>
        <li><b>accedere ai propri Dati.</b>&nbsp;L’Utente ha diritto ad ottenere informazioni sui Dati trattati dal Titolare, su determinati aspetti del trattamento ed a ricevere una copia dei Dati trattati.</li>
        <li><b>verificare e chiedere la rettificazione.</b>&nbsp;L’Utente può verificare la correttezza dei propri Dati e richiederne l’aggiornamento o la correzione.</li>
        <li><b>ottenere la limitazione del trattamento.</b>&nbsp;Quando ricorrono determinate condizioni, l’Utente può richiedere la limitazione del trattamento dei propri Dati. In tal caso il Titolare non tratterà i Dati per alcun altro scopo se non la loro conservazione.</li>
        <li><b>ottenere la cancellazione o rimozione dei propri Dati Personali.</b>&nbsp;Quando ricorrono determinate condizioni, l’Utente può richiedere la cancellazione dei propri Dati da parte del Titolare.</li>
        <li><b>ricevere i propri Dati o farli trasferire ad altro titolare.</b>&nbsp;L’Utente ha diritto di ricevere i propri Dati in formato strutturato, di uso comune e leggibile da dispositivo automatico e, ove tecnicamente fattibile, di ottenerne il trasferimento senza ostacoli ad un altro titolare. Questa disposizione è applicabile quando i Dati sono trattati con strumenti automatizzati ed il trattamento è basato sul consenso dell’Utente, su un contratto di cui l’Utente è parte o su misure contrattuali ad esso connesse.</li>
        <li><b>proporre reclamo.</b>&nbsp;L’Utente può proporre un reclamo all’autorità di controllo della protezione dei dati personali competente o agire in sede giudiziale.</li>
        </ul>
        <h3>Dettagli sul diritto di opposizione</h3>
        <p>Quando i Dati Personali sono trattati nell’interesse pubblico, nell’esercizio di pubblici poteri di cui è investito il Titolare oppure per perseguire un interesse legittimo del Titolare, gli Utenti hanno diritto ad opporsi al trattamento per motivi connessi alla loro situazione particolare.</p>
        <p>Si fa presente agli Utenti che, ove i loro Dati fossero trattati con finalità di marketing diretto, possono opporsi al trattamento senza fornire alcuna motivazione. Per scoprire se il Titolare tratti dati con finalità di marketing diretto gli Utenti possono fare riferimento alle rispettive sezioni di questo documento.</p>
        <h3>Come esercitare i diritti</h3>
        <p>Per esercitare i diritti dell’Utente, gli Utenti possono indirizzare una richiesta agli estremi di contatto del Titolare indicati in questo documento. Le richieste sono depositate a titolo gratuito e evase dal Titolare nel più breve tempo possibile, in ogni caso entro un mese.</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <h2 style="font-size: 2rem;">5. Ulteriori informazioni sul trattamento</h2 style="font-size: 2rem;">
        <h3>Log di sistema e manutenzione</h3>
        <p>Per necessità legate al funzionamento ed alla manutenzione, questa Applicazione e gli eventuali servizi terzi da essa utilizzati potrebbero raccogliere Log di sistema, ossia file che registrano le interazioni e che possono contenere anche Dati Personali, quali l’indirizzo IP Utente.</p>
        <h3>Informazioni non contenute in questa policy</h3>
        <p>Maggiori informazioni in relazione al trattamento dei Dati Personali potranno essere richieste in qualsiasi momento al Titolare del Trattamento utilizzando le informazioni di contatto.</p>
        <h3>Modifiche a questa privacy policy</h3>
        <p>Il Titolare del Trattamento si riserva il diritto di apportare modifiche alla presente privacy policy in qualunque momento dandone pubblicità agli Utenti su questa pagina. Si prega dunque di consultare spesso questa pagina. Nel caso di mancata accettazione delle modifiche apportate alla presente privacy policy, l’Utente è tenuto a cessare l’utilizzo di questa Applicazione e può richiedere al Titolare del Trattamento di rimuovere i propri Dati Personali. Salvo quanto diversamente specificato, la precedente privacy policy continuerà ad applicarsi ai Dati Personali sino a quel momento raccolti.</p>
        <p>&nbsp;</p>
        <h2 style="font-size: 2rem;">6. Riferimenti legali</h2 style="font-size: 2rem;">
        <p>Avviso agli Utenti europei: la presente informativa privacy è redatta in adempimento degli obblighi previsti da:</p>
        <ul>
        <li>l’Art. 10 della Direttiva n. 95/46/CE</li>
        <li>la Direttiva 2002/58/CE, come aggiornata dalla Direttiva 2009/136/CE.</li>
        <li>gli artt. 13 e 14 del Regolamento (UE) 2016/679 (GDPR).</li>
        </ul>
</div>