<h2>Gestisci Utenti e Creatori</h2>
<form action="./gestione_registrati.php" method="GET">
    <input type="hidden" name="ordinamento" value="<?php echo $templateParams["ordinamento"] ?>"/>
    <input type="hidden" name="tipo" value="<?php echo $templateParams["tipo"] ?>"/>
    <input type="hidden" name="visualizza" value="<?php echo $_GET["visualizza"] ?>"/>

    <div class="row no-gutters align-items-center">
        <div class="col">
            <label aria-hidden="false" style="display: none" for="ricercaP">Ricerca tra i registrati</label>
            <input class="form-control" type="search" id="ricercaP" placeholder="Cerca..." name="stringa_filtro" <?php if(isset($templateParams["stringa_filtro"]) && $templateParams["stringa_filtro"]!= "") echo "value=".$templateParams['stringa_filtro'];?> />
        </div>
        <!--end of col-->
        <div class="col-auto">
            <button class="btn" type="submit">
                <em class="fas fa-search" aria-hidden="true" title="Ricerca tra i registrati"></em>
            </button>
        </div>
        <!--end of col-->
    </div>
</form>
<div class="d-inline-block">
    <button class="btn" type="button" id="dropdownMenuButtonTipo" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Ordina: <?php echo ($_GET["ordinamento"] == "usrDesc" ? "nome utente decrescente" : "nome utente crescente")?> <em class="fas fa-caret-down" aria-hidden="true"></em>
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButtonTipo">
        <a class="dropdown-item" href="<?php echo "./gestione_registrati.php?tipo=".$_GET["tipo"]."&visualizza=".$_GET["visualizza"]."&ordinamento=usrDesc"?><?php if(isset($templateParams["stringa_filtro"])) echo "&stringa_filtro=".$templateParams["stringa_filtro"]?>">Nome Decrescente</a>
        <a class="dropdown-item" href="<?php echo "./gestione_registrati.php?tipo=".$_GET["tipo"]."&visualizza=".$_GET["visualizza"]."&ordinamento=usrAsc"?><?php if(isset($templateParams["stringa_filtro"])) echo "&stringa_filtro=".$templateParams["stringa_filtro"]?>">Nome Crescente</a>
    </div> 
</div>
<div class="d-inline-block">
    <button class="btn" type="button" id="dropdownMenuButtonVisualizza" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Visualizza: <?php echo ($_GET["visualizza"]);?> <em class="fas fa-caret-down" aria-hidden="true"></em>
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButtonVisualizza">
        <a class="dropdown-item" href="<?php echo "./gestione_registrati.php?tipo=".$_GET["tipo"]."&visualizza=tutti&ordinamento=".$_GET["ordinamento"]?><?php if(isset($templateParams["stringa_filtro"])) echo "&stringa_filtro=".$templateParams["stringa_filtro"]?>">Tutti</a>
        <a class="dropdown-item" href="<?php echo "./gestione_registrati.php?tipo=".$_GET["tipo"]."&visualizza=bannati&ordinamento=".$_GET["ordinamento"]?><?php if(isset($templateParams["stringa_filtro"])) echo "&stringa_filtro=".$templateParams["stringa_filtro"]?>">Bannati</a>
        <a class="dropdown-item" href="<?php echo "./gestione_registrati.php?tipo=".$_GET["tipo"]."&visualizza=unbannati&ordinamento=".$_GET["ordinamento"]?><?php if(isset($templateParams["stringa_filtro"])) echo "&stringa_filtro=".$templateParams["stringa_filtro"]?>">Non bannati</a>
    </div> 
</div>
<div class="d-inline-block">
    <button class="btn" type="button" id="dropdownMenuButtonOrdina" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Tipologia: <?php echo ($_GET["tipo"]);?> <em class="fas fa-caret-down" aria-hidden="true"></em>
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButtonOrdina">
        <a class="dropdown-item" href="<?php echo "./gestione_registrati.php?tipo=tutti"."&visualizza=".$_GET["visualizza"]."&ordinamento=".$_GET["ordinamento"]?><?php if(isset($templateParams["stringa_filtro"])) echo "&stringa_filtro=".$templateParams["stringa_filtro"]?>">Tutti</a>
        <a class="dropdown-item" href="<?php echo "./gestione_registrati.php?tipo=utenti"."&visualizza=".$_GET["visualizza"]."&ordinamento=".$_GET["ordinamento"]?><?php if(isset($templateParams["stringa_filtro"])) echo "&stringa_filtro=".$templateParams["stringa_filtro"]?>">Utenti</a>
        <a class="dropdown-item" href="<?php echo "./gestione_registrati.php?tipo=creatori"."&visualizza=".$_GET["visualizza"]."&ordinamento=".$_GET["ordinamento"]?><?php if(isset($templateParams["stringa_filtro"])) echo "&stringa_filtro=".$templateParams["stringa_filtro"]?>">Creatori</a>
    </div> 
</div>
<div class="line"></div>
<?php if(empty($templateParams["registrati"])) :
    echo "Nessun risultato.";
endif; ?>
<?php foreach($templateParams["registrati"] as $registrato) {
    if(isset($templateParams["registrato"])) {
        require($templateParams["registrato"]);
    }
}