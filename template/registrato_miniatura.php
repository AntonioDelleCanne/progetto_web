<div class="event p-3 mb-2 bg-light border border-dark rounded row no-gutters col-12 " data-username="<?php echo $registrato["Username"]?>">
    <div class="row col-12 col-md-6 no-gutters">
        <div class="col-3 col-md-2">
            <rm class="fas fa-user-circle fa-3x"></rm> 
        </div>
        <div class="col col-md-8">
            <h2> <?php echo $registrato["Username"];?> </h2>
            <p class="caption"> <?php echo $registrato["Nome"]." ".$registrato["Cognome"]?></p>
        </div>
        <div class="col-2 d-md-none">
            <button class="btn toggle-btn faq-links info" aria-expanded="false" data-toggle="collapse" data-btn="plus" aria-controls="<?php echo $registrato["Username"]?>" data-target="<?php echo "#".$registrato["Username"]?>">
                <rm class="fas fa-plus-square" aria-hidden="true" title="Mostra più informazioni"></rm>
            </button>
        </div>
    </div>
    
    <?php  if(!empty($dbh->getUtenteFromPersona($registrato["Username"]))): 
    $utente = $dbh->getUtenteFromPersona($registrato["Username"]);?>
    <div class="row col-12 col-md-6 no-gutters">
        <div class="col-6 col-md-3 alignCenter" data-toggle="modal" data-target="<?php echo "#Modal".$registrato["Username"]?>">
                <em class="<?php echo ($utente["Ban"] == 0 ? "fas fa-ban" : "fas fa-circle")?>" aria-hidden="true" title="Ban/Unban" style="cursor:pointer"></em><p class="caption"><?php echo ($utente["Ban"] == 0 ? "Ban" : "Unban")?></p>
        </div>
        <div class="col-6 col-md-3 alignCenter">
            <a href="./messaggio.php?dest=<?php echo $registrato["Username"]?>">
                <em class="fas fa-comment" aria-hidden="true" title="Invia messaggio all'utente"></em><p class="caption">Messaggia</p>
            </a>
        </div>
        <div class="col-md-3 "></div>
        <div class="col-md-3 d-none d-md-block alignRight">
            <button class="btn toggle-btn faq-links info" aria-expanded="false" data-toggle="collapse" data-btn="plus" aria-controls="<?php echo $registrato["Username"]?>" data-target="<?php echo "#".$registrato["Username"]?>">
                <rm class="fas fa-plus-square" aria-hidden="true" title="Mostra più informazioni"></rm>
            </button>
        </div>
    </div>
    <div class="collapse hiding-zone row col-12 no-gutters" id="<?php echo $registrato["Username"]?>">
        <div class="line"></div>
        <div class="row col-12 ml-1"><span class="caption"><span class="font-weight-bold">E-mail: </span><?php echo $utente["Mail"];?></span></div>
        <div class="row col-12 ml-1"><span class="caption"><span class="font-weight-bold">Data di nascita: </span><?php echo $utente["DataNascita"];?></span></div>  
    </div>

    <?php else:
    $creatore = $dbh->getCreatoreFromPersona($registrato["Username"]);?>
        <div class="row col-12 col-md-6 no-gutters">
        <div class="col-4 col-md-3 alignCenter" data-toggle="modal" data-target="<?php echo "#Modal".$registrato["Username"]?>">
            <em class="<?php echo ($creatore["Ban"] == 0 ? "fas fa-ban" : "fas fa-circle")?>" aria-hidden="true" title="Ban/Unban" style="cursor:pointer"></em><p class="caption"><?php echo (($creatore["Ban"] == 0) ? "Ban" : "Unban")?></p>
        </div>
        <div class="col-4 col-md-3 alignCenter">
            <a href="<?php echo "./gestione_eventi.php?tipo=tutti&ordinamento=dataAsc&type=1&username=".$registrato["Username"]?>">
                <em class="fas fa-calendar-alt" aria-hidden="true" title="Eventi organizzati dal creatore"></em><p class="caption">Eventi Organizzati</p>
            </a>
        </div>
        <div class="col-4 col-md-3 alignCenter">
            <a href="./messaggio.php?dest=<?php echo $registrato["Username"]?>">
                <em class="fas fa-comment" aria-hidden="true" title="Invia messagio al creatore"></em><p class="caption">Messaggia</p>
            </a>
        </div>
        <div class="col-md-3 d-none d-md-block alignRight">
            <button class="btn toggle-btn faq-links info" aria-expanded="false" data-toggle="collapse" data-btn="plus" aria-controls="<?php echo $registrato["Username"]?>" data-target="<?php echo "#".$registrato["Username"]?>">
                <rm class="fas fa-plus-square" aria-hidden="true" title="Mostra più informazioni"></rm>
            </button>
        </div>
    </div>
    <div class="collapse hiding-zone row col-12 no-gutters" id="<?php echo $registrato["Username"]?>">
        <div class="line"></div>
        <div class="row col-12 ml-1"><span class="caption"><span class="font-weight-bold">E-mail: </span><?php echo $creatore["Mail"];?></span></div>
        <div class="row col-12 ml-1"><span class="caption"><span class="font-weight-bold">Data di nascita: </span><?php echo $creatore["DataNascita"];?></span></div>
        <div class="row col-12 ml-1"><span class="caption"><span class="font-weight-bold">Codice Fiscale: </span><?php echo $creatore["CF"];?></span></div>
        <div class="row col-12 ml-1"><span class="caption"><span class="font-weight-bold">Codice carta d'Identità: </span><?php echo $creatore["CartaIdentita"];?></span></div>
        <div class="row col-12 ml-1"><span class="caption"><span class="font-weight-bold">IBAN: </span><?php echo $creatore["IBAN"];?></span></div>  
    </div>
    <?php endif;?>    

    
    <div class="modal fade" id="<?php echo "Modal".$registrato["Username"]?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo "ModalLabel".$registrato["Username"]?>" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title" id="<?php echo "ModalLabel".$registrato["Username"]?>"><?php echo ($dbh->isBannato($registrato["Username"]) ? "Unban" : "Ban")?> utente</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                    Sei sicuro di volere <?php echo ($dbh->isBannato($registrato["Username"]) ? "unbannare" : "bannare")?> <?php echo $registrato["Username"]?>?
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-block rounded orange-button <?php echo ($dbh->isBannato($registrato["Username"]) ? "unban" : "ban")?>">Procedi</button>
                    <button type="button" class="btn rounded" data-dismiss="modal">Annulla</button>
                </div>      
            </div>
        </div>
    </div>

    
</div>
