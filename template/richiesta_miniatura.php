
<div class="event p-3 mb-2 bg-light border border-dark rounded row no-gutters col-12 " data-username="<?php echo $creatore["Username"]?>">
    <div class="row col-12 col-md-6 no-gutters">
        <div class="col-3 col-md-2">
            <rm class="fas fa-user-circle fa-3x"></rm> 
        </div>
        <div class="col col-md-8">
            <h2> <?php echo $creatore["Username"];?> </h2>
            <p class="caption"> Data richiesta: <?php echo $creatore["DataRegistrazione"];?></p>
        </div>
        <div class="col-2 d-md-none">
            <button class="btn toggle-btn faq-links info" aria-expanded="false" data-toggle="collapse" data-btn="plus" aria-controls="<?php echo $creatore["Username"]?>" data-target="<?php echo "#".$creatore["Username"]?>">
                <rm class="fas fa-plus-square" aria-hidden="true" title="Mostra più informazioni"></rm>
            </button>
        </div>
    </div>

    <div class="row col-12 col-md-6 no-gutters">
        <div class="col-6 col-md-4 alignCenter" data-toggle="modal" data-target="<?php echo "#Modal1".$creatore["Username"]?>">
           <em class="fas fa-check" aria-hidden="true" title="Rifiuta richiesta registrazione" style="cursor:pointer"></em><p class="caption">Accetta</p>
        </div>
        <div class="modal fade" id="<?php echo "Modal1".$creatore["Username"]?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo "ModalLabel1".$creatore["Username"]?>" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="<?php echo "ModalLabel1".$creatore["Username"]?>">Accettazione richiesta</h2>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>
                        Sei sicuro di volere accettare la richiesta di registrazione come creatore di <?php echo $creatore["Username"]?>?
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-block rounded orange-button accettazione-richiesta">Procedi</button>
                        <button type="button" class="btn rounded" data-dismiss="modal">Annulla</button>
                    </div>      
                </div>
            </div>
        </div>
        <div class="col-6 col-md-4 alignCenter" data-toggle="modal" data-target="<?php echo "#Modal2".$creatore["Username"]?>">
           <em class="fas fa-times" aria-hidden="true" title="Rifiuta richiesta registrazione" style="cursor:pointer"></em><p class="caption">Rifiuta</p>
        </div>
        <div class="modal fade" id="<?php echo "Modal2".$creatore["Username"]?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo "ModalLabel2".$creatore    ["Username"]?>" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="<?php echo "ModalLabel2".$creatore["Username"]?>">Rifiuto richiesta</h2>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>
                        Sei sicuro di volere rifiutare la richiesta di registrazione come creatore di <?php echo $creatore["Username"]?>?
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-block rounded orange-button rifiuto-richiesta">Procedi</button>
                        <button type="button" class="btn rounded" data-dismiss="modal">Annulla</button>
                    </div>      
                </div>
            </div>
        </div>
        <div class="col-md-4 d-none d-md-block alignRight">
            <button class="btn toggle-btn faq-links info" aria-expanded="false" data-toggle="collapse" data-btn="plus" aria-controls="<?php echo $creatore["Username"]?>" data-target="<?php echo "#".$creatore["Username"]?>">
                <rm class="fas fa-plus-square" aria-hidden="true" title="Mostra più informazioni"></rm>
            </button>
        </div>
    </div>

    <div class="collapse hiding-zone row col-12 no-gutters" id="<?php echo $creatore["Username"]?>">
        <div class="line"></div>
        <p>
        <div class="row col-12 ml-1"><span class="caption"><span class="font-weight-bold">E-mail: </span><?php echo $creatore["Mail"];?></span></div>
        <div class="row col-12 ml-1"><span class="caption"><span class="font-weight-bold">Data di nascita: </span><?php echo $creatore["DataNascita"];?></span></div>
        <div class="row col-12 ml-1"><span class="caption"><span class="font-weight-bold">Codice Fiscale: </span><?php echo $creatore["CF"];?></span></div>
        <div class="row col-12 ml-1"><span class="caption"><span class="font-weight-bold">Codice carta d'Identità: </span><?php echo $creatore["CartaIdentita"];?></span></div>
        <div class="row col-12 ml-1"><span class="caption"><span class="font-weight-bold">IBAN: </span><?php echo $creatore["IBAN"];?></span></div>
        </p>    
    </div>
</div>