<h2>Gestisci richieste creatori</h2>
<?php if(empty($templateParams["richieste"]) && !isset($templateParams["stringa_filtro"])) :
    echo "Non ci sono richieste in corso.";
else: ?>
    <form action="./gestione_richieste.php" method="GET">
        <input type="hidden" name="ord" value="<?php echo $templateParams["ordinamento"] ?>"/>
        <div class="row no-gutters align-items-center">
            <div class="col">
                <label aria-hidden="false" style="display: none" for="ricercaParticolare">Ricerca tra le richieste</label>
                <input class="form-control" id="ricercaParticolare" type="search" placeholder="Cerca..." name="stringa_filtro" <?php if(isset($templateParams["stringa_filtro"]) && $templateParams["stringa_filtro"]!= "") echo "value=".$templateParams['stringa_filtro'];?> />
            </div>
            <!--end of col-->
            <div class="col-auto">
                <button class="btn" type="submit">
                    <em class="fas fa-search" aria-hidden="true" title="Ricerca tra le richieste"></em>
                </button>
            </div>
            <!--end of col-->
        </div>
    </form>
    <div class="d-inline-block">
        <button class="btn w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Ordina: <?php echo ($_GET["ord"]=="nomeAsc" ? "nome crescente" : ($_GET["ord"]=="nomeDesc" ? "nome decrescente" : ($_GET["ord"]=="dataAsc" ? "data inizio minore" : "data più lontana")))?> <em class="fas fa-caret-down" aria-hidden="true"></em>
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="./gestione_richieste.php?ord=nomeDesc<?php if(isset($templateParams["stringa_filtro"])) echo "&stringa_filtro=".$templateParams["stringa_filtro"]?>">Nome Decrescente</a>
            <a class="dropdown-item" href="./gestione_richieste.php?ord=nomeAsc<?php if(isset($templateParams["stringa_filtro"])) echo "&stringa_filtro=".$templateParams["stringa_filtro"]?>">Nome Crescente</a>
            <a class="dropdown-item" href="./gestione_richieste.php?ord=dataAsc<?php if(isset($templateParams["stringa_filtro"])) echo "&stringa_filtro=".$templateParams["stringa_filtro"]?>">Prima più recenti</a>
            <a class="dropdown-item" href="./gestione_richieste.php?ord=dataDesc<?php if(isset($templateParams["stringa_filtro"])) echo "&stringa_filtro=".$templateParams["stringa_filtro"]?>">Prima più vecchi</a>
        </div> 
    </div>
    <div class="line"></div>
    <?php foreach($templateParams["richieste"] as $creatore) {
        if(isset($templateParams["evento"])) {
            require($templateParams["evento"]);
        }
    }
endif ?>