<div class="container">
    <div class="justify-content-center row" id="messageNavDiv">
        <nav class="navbar no-padding navbar-expand border border-dark col-12" id="messageNav">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item col-4 alignCenter <?php if($templateParams['paginaAttiva'] == 'ricevuti') echo 'active'?>">
                    <a class="nav-link" href="gestione_messaggi.php?pagina=ricevuti"><em class="fas fa-inbox d-md-none" aria-hidden="true" title="Messaggi ricevuti"></em><span class="d-none d-md-block"> Ricevuti </span> <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item col-4 alignCenter <?php if($templateParams['paginaAttiva'] == 'inviati') echo 'active'?>">
                    <a class="nav-link" href="gestione_messaggi.php?pagina=inviati"><em class="fas fa-paper-plane d-md-none" aria-hidden="true" title="Messaggi inviati"></em><span class="d-none d-md-block"> Inviati </span> </a>
                </li>
                <li class="nav-item col-4 alignCenter <?php if($templateParams['paginaAttiva'] == 'promemoria') echo 'active'?>">
                    <a class="nav-link" href="gestione_messaggi.php?pagina=promemoria"><em class="fas fa-exclamation d-md-none" aria-hidden="true" title="Promemoria"></em><span class="d-none d-md-block"> Promemoria </span> </a>
                </li>
            </ul>
        </nav>
    </div>
    <div class="d-md-none">
        <?php if($templateParams['paginaAttiva'] == 'ricevuti'):?>
            <h2 class="mt-2">Messaggi ricevuti</h2>
        <?php elseif($templateParams['paginaAttiva'] == 'inviati'):?>
            <h2 class="mt-2">Messaggi inviati</h2>
        <?php else:?>
            <h2 class="mt-2">Promemoria</h2>
        <?php endif;?>
        <div class="line"></div>
    </div>
    
    <?php if($templateParams['paginaAttiva'] == 'inviati'):?>
    <a href="messaggio.php">
        <div class="event p-3 mb-2 mt-2 bg-light border border-dark rounded row">
            <div class="col-4">
                <em class="fas fa-plus-circle fa-2x" aria-hidden="true"></em>
            </div>
            <div class="col-8">
                <h2>Nuovo messaggio</h2>
            </div>
        </div>
    </a>
    <div class="row"><div class="line"></div></div>

    <?php endif;?>
    <div class="justify-content-center row">
        <?php if(isset($templateParams["messaggi"])):?>
        <?php foreach($templateParams["messaggi"] as $messaggio): ?>
        <?php
        if(isset($templateParams["miniatura"])){
            require($templateParams["miniatura"]);
        }
        ?>
        <?php endforeach; ?>
        <?php else:?>
        <p>Non ci sono messaggi disponibili<p>
            <?php endif;?>
        </div>
    </div>