<div class="p-3">
    <form class="form-signin justify-content-center" method="post" action=<?php echo $templateParams["onsubmit"]?>>
        <div class="form-group row">
            <div class="col-12 col-md-3">
            <?php if(isset($templateParams["idMessaggio"]) && !isset($_GET["risposta"]) && $templateParams["mittente"] != $_SESSION["username"]): ?> 
                <label for="inputNome">Da:</label>
            <?php else: ?>
                <label for="inputNome">A:</label>
            <?php endif ?>
            </div>
            <div class="col-12 col-md-9">
                    <input id="inputNome" name="inputNome" <?php if(isset($_GET["evento"])) echo "readonly value=Partecipanti"; else if(isset($templateParams["idMessaggio"])) echo $templateParams["mittente"] == $_SESSION["username"] ? "readonly value=".multi_implode($templateParams["destinatari"], ";") : (isset($_GET["risposta"]) ? "value=".($dbh->contains('amministratore', 'Username', $templateParams["mittente"], 's') ? 'Amministratore': $templateParams["mittente"]) : "readonly value=".($dbh->contains('amministratore', 'Username', $templateParams["mittente"], 's') ? 'Amministratore': $templateParams["mittente"]))?> <?php if(isset($_GET["dest"]) && !isset($templateParams["idMessaggio"])) echo "readonly value=".$_GET["dest"]?> name="destinatari" class="form-control" placeholder="Destinatario..." required="" autofocus=""/>
            </div>
        </div>
        <?php if(isset($_GET["evento"])):?>
        <input type="hidden" name="evento" value="<?php echo $_GET["evento"] ?>"/>
        <?php endif?>
        <div class="form-group row">
            <div class="col-12 col-md-3">
                <label for="inputOggetto">Oggetto:</label>
            </div>
            <div class="col-12 col-md-9">
                    <input id="inputOggetto" <?php if(isset($templateParams["idMessaggio"]))  echo isset($_GET["risposta"]) ? "value='Re: ".$templateParams["messaggio"]["Oggetto"]."'" : "readonly value=".$templateParams["messaggio"]["Oggetto"]?> name="oggetto" class="form-control" placeholder="Oggetto messaggio..." required="" autofocus="">
            </div>
        </div>

        <div class="form-group">
            <label for="textArea">Scrivi testo...</label>
            <textarea class="form-control rounded-0" id="textArea" name="testo" rows="10" <?php  if(isset($templateParams["idMessaggio"]) && !isset($_GET["risposta"])) echo "readonly"?> > <?php  if(isset($templateParams["idMessaggio"])) echo isset($_GET["risposta"]) ?  $templateParams["messaggio"]["Testo"]."&#10;---------------------------------------&#10;Risposta di ".$_SESSION["username"].":&#10;&#10;" : $templateParams["messaggio"]["Testo"];?></textarea>
        </div>
        <?php if($templateParams["mittente"] != $_SESSION["username"]): ?> 
        <button class="btn btn-block rounded orange-button" type="submit"><?php echo isset($templateParams["idMessaggio"]) && !isset($_GET["risposta"]) ? "Rispondi" : "Invia messaggio"?></button>
        <?php endif ?>
    </form>
</div>
