<form class="form-signin justify-content-center" action="esito_pagamento.php">
    <div class="event p-3 mb-2 mt-2 bg-light border border-dark rounded">
        <div class="row justify-content-center">
            <div class="col-6">
                <p class="thick-text" id="narticoli"><?php echo 'Articoli: '.$templateParams["narticoli"];?></p>
            </div>
            <div class="col-6">
                <p class="thick-text" id="totale"><?php echo "Totale:    ".$templateParams['totale'];?><em class='fas fa-euro-sign' aria-hidden="true"></em></p>
            </div>
        </div>
        <div class="row justify-content-center mb-2">
            <div class="col-12">
                <button class="btn btn-block rounded orange-button" type="submit">Procedi all'ordine</button>
            </div>
        </div>
        <?php foreach($templateParams["eventicarrello"] as $evento){
            if(isset($templateParams["evento"])){
                require($templateParams["evento"]);
            }
        }?>
    </div>
    
    <!--<h2 class="mt-2">Dati Pagamento</h2>
    <div class="p-3 mb-2 bg-light border border-dark rounded">
    </div>
    
    <h2 class="mt-2">Consegna</h2>
    <div class="p-3 mb-2 bg-light border border-dark rounded">
    </div>-->
</form>