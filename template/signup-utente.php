<div class="container">
    <div class="row no-gutters justify-content-center pl-3 pr-3">
        <form class="form-signin justify-content-center" action= "signup_successful.php" method="post">
        <div class="row no-gutters">
            <h1 class="col-10 h3 mb-3 font-weight-normal">Registrati Come Utente</h1>
        </div>
        <div class="form-group row">
            <div class="col-12 col-md-6">
                <label for="nome" class="col-form-label text-md-right">Nome</label>
            </div>
            <div class="col-md-6 col-12">
                <input type="text" id="nome" class="form-control" name="nome" required autofocus>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-12 col-md-6">
                <label for="cognome" class="col-form-label text-md-right">Cognome</label> 
            </div>
            <div class="col-12 col-md-6">
                <input type="text" id="cognome" class="form-control" name="cognome" required>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-12 col-md-6">
                <label for="nome-utente" class="col-form-label text-md-right">Nome utente</label> 
            </div>
            <div class="col-12 col-md-6">
                <input type="text" id="nome-utente" class="form-control" name="nome-utente" required>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-12 col-md-6">
                <label for="mail" class="col-form-label text-md-right">E-mail</label> 
            </div>
            <div class="col-12 col-md-6">
                <input type="text" id="mail" class="form-control" name="mail" required>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-12 col-md-6">
                <label for="password" class="col-form-label text-md-right">Password</label> 
            </div>
            <div class="col-12 col-md-6">
                <input type="password" id="password" class="form-control" name="password" required>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-12 col-md-6">
                <label for="conferma-password" class="col-form-label text-md-right">Conferma Password</label> 
            </div>
            <div class="col-12 col-md-6">
                <input type="password" id="conferma-password" class="form-control" name="conferma-password" required>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-12 col-md-6">
                <label for="data-nascita" class="col-form-label text-md-right">Data di Nascita</label> 
            </div>
            <div class="col-12 col-md-6">
                <input type="date" id="data-nascita" class="form-control" name="data-nascita" required>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-12">
                <button class="btn btn-block rounded orange-button" type="submit">Registrati</button>
            </div>
        </div>
        
    </form>
    </div>
</div>