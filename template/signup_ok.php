<div class="justify-content-center pl-3 pr-3 alignCenter">
    <h1 class="h3 mb-3 font-weight-normal">Account creato con successo</h1>
    
    <?php
        if ($templateParams["tipo_signup"] === "utente") :
            echo "<p>Ora puoi eseguire il login come utente.</p>";?>
            <button onclick="location.href='./login.php'" class="btn">
                <em class="fas fa-user-circle" aria-hidden="true" title="Accedi"></em> Accedi
            </button>
    <?php else : 
            echo "<p>Per poter eseguire il login come creatore dovrai aspettare che il tuo account venga approvato.</p>";
    endif?>
</div>