<?php
require_once("bootstrap.php");

$username = $_SESSION["username"];

$dbh->updatePromemoriaUtente($username);
$nMessaggiRicevuti = $dbh->getNMessaggiRicevutiNonLetti($username);
$nPromemoria = $dbh->getNPromemoriaNonLetti($username);
$nArticoli = -1;
$now = new DateTime();
if($_SESSION["logtype"] == 'utente'){
    $nArticoli = 0;
    foreach ($dbh->getEventiCarrello($username) as $key => $value) {
        $event_date = new DateTime($value["TempoAggiunta"]);
        $tempo = $now->getTimestamp() - $event_date->getTimestamp();
        if($tempo > 900) $dbh->eliminaEventoCarrello($_SESSION["username"], $value["idEvento"]);
        else $nArticoli += $value["Quantita"];
    }
}
echo json_encode(array("npromemoria" => $nPromemoria, "nmessaggi" => $nMessaggiRicevuti, "narticoli" => $nArticoli));
?>